package jpm.utils;

import java.lang.reflect.Field;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.junit.Assert;

import jpm.git.bo.TaskInfo;
import jpm.git.tasks.Task;
import jpm.git.tasks.TaskManager;
import jpm.git.tasks.TaskStatus;
import jpm.git.util.GitUtil;
import jpm.git.wrapper.AbstractCommand;

/**
 * Clase con utilidades utilizadas en los tests
 * 
 * @author j.penalba
 *
 */
public class TestUtils {

	final static Logger LOG = LoggerFactory.getLogger(TestUtils.class);

	public static final String repository1 = "https://github.com/josepm9/ProgrammingAssignment2.git";
	public static final String norepository = "https://github.com/josepm9/ProgrammingAssignment2_no.git";
	public static final String repository1_key = "ProgrammingAssignment2.git";
	public static final String norepository_key = "ProgrammingAssignment2_no.git";
	public static final String repository2 = "https://bitbucket.org/jpenalba/rebase_maligno.git";
	public static final String repository2_key = "rebase_maligno.git";
	public static final String repository2_u = "jpenalba";
	public static final String repository2_p = "";
	public static String workingFolder = "target/tests";

	public static final String repository3 = "https://github.com/josepm9/jgp_testing_repo.git";
	public static final String repository3_key = "jgp_testing_repo.git";
	public static final String repository3_issue1 = "JGP-1";
	public static final String repository3_issue2 = "JGP-2";

	public static String executable = null;

	public static void findGitExecutable() {
		if (TestUtils.executable == null) {
			TestUtils.executable = GitUtil.findGitExecutable(null);
		}
		Assert.assertNotNull(TestUtils.executable);
		LOG.info("Ejecutable encontrado: " + executable);
	}

	public static TaskStatus waitForEnd(final String taskKey, final TaskManager taskManager, long timeout)
			throws InterruptedException {
		final long ts = System.currentTimeMillis();
		TaskStatus st = null;
		while (true) {
			Thread.sleep(50l);
			st = taskManager.getTaskStatus(taskKey);
			System.out.print(st.getNewOutput());
			if (st.getState() == Task.ST_ENDED_OK || st.getState() == Task.ST_ENDED_KO) {
				break;
			}
			// Asegurar que la prueba no se eterniza
			Assert.assertTrue((System.currentTimeMillis() - ts) < timeout);
		}
		return st;
	}

	/**
	 * Esperar a que un conjunto de tareas finalice
	 * 
	 * @throws InterruptedException
	 */
	public static void waitForTasksToEnd(TaskInfo[] tasks, final long timeoutMillis, final TaskManager taskManager)
			throws InterruptedException {
		final ThreadPoolExecutor exeSvc = new ThreadPoolExecutor(tasks.length, tasks.length, 0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());
		for (int i = 0; i < tasks.length; ++i) {
			final int j = i;
			exeSvc.execute(new Runnable() {
				@Override
				public void run() {
					try {
						tasks[j].setAsyncTaskStatus(
								TestUtils.waitForEnd(tasks[j].getKey(), taskManager, timeoutMillis));
					} catch (InterruptedException e) {
						LOG.error(e.getMessage(), e);
					}
				}
			});
		}
		exeSvc.shutdown();
		Assert.assertTrue(exeSvc.awaitTermination(timeoutMillis, TimeUnit.MILLISECONDS));
	}

	/**
	 * Obtener el GitWrapper de un Task
	 */
	public static AbstractCommand<?> getCommand(Task<?> task) {
		try {
			final Field f = Task.class.getDeclaredField("gitWrapper");
			f.setAccessible(true);
			return (AbstractCommand<?>) f.get(task);
		} catch (Exception e) {
			throw new RuntimeException(e.getClass().getName() + ": " + e.getMessage(), e);
		}
	}
}
