package jpm.utils;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import jpm.git.util.IoUtil;
import jpm.git.wrapper.AbstractCommand;
import jpm.git.wrapper.GitWrapperException;

public class DeleteDirCommand extends AbstractCommand<Object> {

	final static Logger LOG = LoggerFactory.getLogger(DeleteDirCommand.class);
	final File dir;
	
	public DeleteDirCommand(final File dir) {
		super(null, null, null, null, null, "DeleteDirCommand");
		this.dir = dir;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.git.wrapper.GitWrapper#execute()
	 */
	@Override
	public void execute() throws GitWrapperException {
		int exitValue = 0;
		try {
			if (dir.exists()) {
				try {
					IoUtil.tryDelete(dir);
				} catch (IOException e) {
					LOG.error(e.getMessage());
					exitValue = 1;
				}
			}
			if (!dir.mkdirs()) {
				exitValue = 2;
			}
		} finally {
			// Done!
			LOG.info("DeleteDirCommand ended with status " + exitValue);
		}
		end(exitValue);
	}
	
}
