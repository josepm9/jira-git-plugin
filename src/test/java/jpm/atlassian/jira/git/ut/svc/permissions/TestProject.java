package jpm.atlassian.jira.git.ut.svc.permissions;

class TestProject extends ATestProject {
	
	final String key;
	
	public TestProject(final String key) {
		this.key = key;
	}

	/* (non-Javadoc)
	 * @see jpm.atlassian.jira.git.ut.svc.permissions.ATestProject#getKey()
	 */
	@Override
	public String getKey() {
		return key;
	}
	
	

}
