package jpm.atlassian.jira.git.ut.svc.permissions;

import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;

class TestApplicationUser implements ApplicationUser {

	final boolean admin;
	final List<String> projsAsDev;
	final List<String> projsAsAdmin;

	public TestApplicationUser(final boolean admin, final String[] projsAsDev, final String[] projsAsAdmin) {
		this.admin = admin;
		this.projsAsDev = projsAsDev==null?null:Arrays.asList(projsAsDev);
		this.projsAsAdmin = projsAsAdmin==null?null:Arrays.asList(projsAsAdmin);
	}

	@Override
	public Long getId() {
		return 1l;
	}

	@Override
	public long getDirectoryId() {
		return 0;
	}

	@Override
	public User getDirectoryUser() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return "1";
	}

	@Override
	public String getEmailAddress() {
		return "1";
	}

	@Override
	public String getKey() {
		return "1";
	}

	@Override
	public String getName() {
		return "1";
	}

	@Override
	public String getUsername() {
		return "1";
	}

	@Override
	public boolean isActive() {
		return true;
	}

}
