package jpm.atlassian.jira.git.ut.svc.permissions;

import java.util.Collection;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.GlobalPermissionEntry;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraPermission;
import com.atlassian.jira.user.ApplicationUser;

abstract class ATestGlobalPermissionManager implements GlobalPermissionManager {
	
	@Override
	public boolean addPermission(int arg0, String arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addPermission(GlobalPermissionType arg0, String arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clearCache() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<GlobalPermissionType> getAllGlobalPermissions() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Option<GlobalPermissionType> getGlobalPermission(int arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Option<GlobalPermissionType> getGlobalPermission(GlobalPermissionKey arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Option<GlobalPermissionType> getGlobalPermission(String arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<String> getGroupNames(int arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<String> getGroupNames(GlobalPermissionType arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<String> getGroupNamesWithPermission(GlobalPermissionKey arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Group> getGroupsWithPermission(int arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Group> getGroupsWithPermission(GlobalPermissionType arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Group> getGroupsWithPermission(GlobalPermissionKey arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<JiraPermission> getPermissions(int arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<GlobalPermissionEntry> getPermissions(GlobalPermissionType arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<GlobalPermissionEntry> getPermissions(GlobalPermissionKey arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPermission(int arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPermission(GlobalPermissionType arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPermission(int arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPermission(GlobalPermissionType arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPermission(GlobalPermissionKey arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isGlobalPermission(int arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isPermissionManagedByJira(GlobalPermissionKey arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removePermission(int arg0, String arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removePermission(GlobalPermissionType arg0, String arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removePermissions(String arg0) {
		throw new UnsupportedOperationException();
	}

}
