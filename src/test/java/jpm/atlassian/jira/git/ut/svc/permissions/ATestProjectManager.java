package jpm.atlassian.jira.git.ut.svc.permissions;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.project.DefaultAssigneeException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.user.ApplicationUser;

abstract class ATestProjectManager implements ProjectManager {

	@Override
	public List<Project> convertToProjectObjects(Collection<Long> arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project createProject(ApplicationUser arg0, ProjectCreationData arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory createProjectCategory(String arg0, String arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<ProjectCategory> getAllProjectCategories() throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Set<String> getAllProjectKeys(Long arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public long getCurrentCounterForProject(Long arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ApplicationUser getDefaultAssignee(Project arg0, ProjectComponent arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ApplicationUser getDefaultAssignee(Project arg0, Collection<ProjectComponent> arg1)
			throws DefaultAssigneeException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public long getNextId(Project arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Project getProjectByCurrentKey(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project getProjectByCurrentKeyIgnoreCase(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategory(Long arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategoryForProject(Project arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategoryObject(Long arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategoryObjectByName(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategoryObjectByNameIgnoreCase(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public long getProjectCount() throws DataAccessException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Project getProjectObj(Long arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project getProjectObjByKey(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project getProjectObjByKeyIgnoreCase(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project getProjectObjByName(String arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public List<Project> getProjectObjects() throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjectObjectsFromProjectCategory(Long arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjectObjectsWithNoCategory() throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public List<Project> getProjects() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjectsFromProjectCategory(ProjectCategory arg0) throws DataAccessException {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public List<Project> getProjectsLeadBy(ApplicationUser arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean isProjectCategoryUnique(String arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void refresh() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void removeProject(Project arg0) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void removeProjectCategory(Long arg0) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void removeProjectIssues(Project arg0) throws RemoveException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setCurrentCounterForProject(Project arg0, long arg1) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setProjectCategory(Project arg0, ProjectCategory arg1) throws DataAccessException {
		throw new UnsupportedOperationException();

	}

	@Override
	public Project updateProject(Project arg0, String arg1, String arg2, String arg3, String arg4, Long arg5) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project updateProject(Project arg0, String arg1, String arg2, String arg3, String arg4, Long arg5,
			Long arg6) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Project updateProject(Project arg0, String arg1, String arg2, String arg3, String arg4, Long arg5, Long arg6,
			String arg7) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void updateProjectCategory(ProjectCategory arg0) throws DataAccessException {
		throw new UnsupportedOperationException();

	}

	@Override
	public Project updateProjectType(ApplicationUser arg0, Project arg1, ProjectTypeKey arg2) {
		throw new UnsupportedOperationException();
		
	}

}
