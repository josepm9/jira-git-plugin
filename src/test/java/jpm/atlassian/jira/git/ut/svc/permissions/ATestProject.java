package jpm.atlassian.jira.git.ut.svc.permissions;

import java.util.Collection;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;

abstract class ATestProject implements Project {

	@Override
	public Long getAssigneeType() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Avatar getAvatar() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<ProjectComponent> getComponents() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getDescription() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getEmail() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public GenericValue getGenericValue() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Long getId() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<IssueType> getIssueTypes() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getKey() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ApplicationUser getLead() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getLeadUserKey() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getLeadUserName() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getName() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getOriginalKey() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategory() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectCategory getProjectCategoryObject() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<ProjectComponent> getProjectComponents() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ApplicationUser getProjectLead() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectTypeKey getProjectTypeKey() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public String getUrl() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Version> getVersions() {
		throw new UnsupportedOperationException();
		
	}

}
