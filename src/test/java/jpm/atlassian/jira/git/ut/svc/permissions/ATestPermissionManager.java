package jpm.atlassian.jira.git.ut.svc.permissions;

import java.util.Collection;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.ProjectWidePermission;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.workflow.loader.ActionDescriptor;

abstract class ATestPermissionManager implements PermissionManager {

	@Override
	public void flushCache() {
		throw new UnsupportedOperationException();

	}

	@Override
	public Collection<Group> getAllGroups(int arg0, Project arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<ProjectPermission> getAllProjectPermissions() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Option<ProjectPermission> getProjectPermission(ProjectPermissionKey arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<ProjectPermission> getProjectPermissions(ProjectPermissionCategory arg0) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjects(int arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjects(ProjectPermissionKey arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjects(int arg0, ApplicationUser arg1, ProjectCategory arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Collection<Project> getProjects(ProjectPermissionKey arg0, ApplicationUser arg1, ProjectCategory arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(int arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(int arg0, Issue arg1, ApplicationUser arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(ProjectPermissionKey arg0, Issue arg1, ApplicationUser arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(int arg0, Project arg1, ApplicationUser arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(ProjectPermissionKey arg0, Project arg1, ApplicationUser arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(ProjectPermissionKey arg0, Issue arg1, ApplicationUser arg2, ActionDescriptor arg3) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(ProjectPermissionKey arg0, Issue arg1, ApplicationUser arg2, Status arg3) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(int arg0, Project arg1, ApplicationUser arg2, boolean arg3) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasPermission(ProjectPermissionKey arg0, Project arg1, ApplicationUser arg2, boolean arg3) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public ProjectWidePermission hasProjectWidePermission(ProjectPermissionKey arg0, Project arg1,
			ApplicationUser arg2) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasProjects(int arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasProjects(ProjectPermissionKey arg0, ApplicationUser arg1) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void removeGroupPermissions(String arg0) throws RemoveException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void removeUserPermissions(ApplicationUser arg0) throws RemoveException {
		throw new UnsupportedOperationException();

	}

}
