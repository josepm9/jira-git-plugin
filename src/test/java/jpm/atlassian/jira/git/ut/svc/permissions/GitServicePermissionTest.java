package jpm.atlassian.jira.git.ut.svc.permissions;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import jpm.atlassian.jira.git.PluginProperties;
import jpm.atlassian.jira.git.svc.GitApiMethodFilterImpl;
import jpm.git.bo.GitInfo;
import jpm.git.persistence.properties.PropGitRepositoryDAOImpl;
import jpm.git.svc.GitServiceImpl;
import jpm.git.svc.rest.AbstractTest;
import jpm.git.svc.rest.GitRestApi;
import jpm.utils.TestUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GitServicePermissionTest extends AbstractTest {

	private static final String TEST_PROJECT = "GitServicePermissionTest";

	static GitRestApi component;
	static GitApiMethodFilterImpl filter;
	static TestUserUtil userUtil = new TestUserUtil();

	@Test
	public void t0000_before() throws IOException {
		initializeComponentManager();
		filter = new GitApiMethodFilterImpl(new TestGlobalPermissionManager(), new TestPermissionManager(), userUtil,
				new TestProjectManager(), cfg);
		component = new GitRestApi(
				new GitServiceImpl(cfg, taskManager, filter, new PropGitRepositoryDAOImpl(propertyDao), propertyDao));
	}

	// setGitExecutable

	@Test
	public void t0010_test_setGitExecutable_ok() {
		userUtil.setUser(new TestApplicationUser(true, null, null));
		final Response r = component.setGitExecutable("got.exe");
		Assert.assertNotNull(r);
		Assert.assertTrue(r.getEntity() instanceof GitInfo);
		final GitInfo info = (GitInfo) r.getEntity();
		Assert.assertNotNull(info);
		Assert.assertNotNull(info.getError());
	}

	@Test
	public void t0020_test_setGitExecutable_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.setGitExecutable("got.exe").getStatus());
	}

	// getGitExecutable(true)

	@Test
	public void t0030_test_getGitExecutable_ok() {
		userUtil.setUser(new TestApplicationUser(true, null, null));
		Assert.assertEquals(200, component.getGitExecutable(true).getStatus());
	}

	@Test
	public void t0035_test_getGitExecutable_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.getGitExecutable(true).getStatus());
	}

	// getGitExecutable(null)

	@Test
	public void t0040_test_getGitExecutable_ok() {
		userUtil.setUser(new TestApplicationUser(true, null, null));
		Assert.assertEquals(200, component.getGitExecutable(null).getStatus());
	}

	@Test
	public void t0045_test_getGitExecutable_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.getGitExecutable(null).getStatus());
	}

	// getRepositories

	@Test
	public void t0050_test_getRepositories_ok() {
		userUtil.setUser(new TestApplicationUser(true, null, null));
		Assert.assertEquals(200, component.getRepositories().getStatus());
	}

	@Test
	public void t0060_test_getRepositories_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.getRepositories().getStatus());
	}

	// createNewRepository

	@Test
	public void t0070_test_createNewRepository_ok() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { TEST_PROJECT }));
		final Response r = component.createNewRepository(TEST_PROJECT, TestUtils.norepository, null, null, "gitlab",
				false, false);
		Assert.assertEquals(500, r.getStatus());
	}

	@Test
	public void t0080_test_createNewRepository_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { TEST_PROJECT + "_NO" }));
		Assert.assertEquals(403,
				component.createNewRepository(TEST_PROJECT, TestUtils.repository1, null, null, "gitlab", false, false)
						.getStatus());
	}

	// getRepositories_prj

	@Test
	public void t0090_test_getRepositories_prj_admin_ok() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { "1" }));
		final Response r = component.getRepositories("1");
		Assert.assertEquals(200, r.getStatus());
	}

	@Test
	public void t0100_test_getRepositories_prj_dev_ko() {
		userUtil.setUser(new TestApplicationUser(false, new String[] { "1" }, null));
		Assert.assertEquals(403, component.getRepositories("1").getStatus());
	}

	@Test
	public void t0110_test_getRepositories_prj_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.getRepositories("2").getStatus());
	}

	// getRepository

	@Test
	public void t0120_test_getRepository_admin_ok() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { "1" }));
		final Response r = component.getRepository("1", "r");
		Assert.assertEquals(404, r.getStatus());
	}

	@Test
	public void t0130_test_getRepository_dev_ko() {
		userUtil.setUser(new TestApplicationUser(false, new String[] { "1" }, null));
		Assert.assertEquals(403, component.getRepository("1", "r").getStatus());
	}

	@Test
	public void t0140_test_getRepository_ko() {
		userUtil.setUser(new TestApplicationUser(false, new String[] { "2" }, new String[] { "2" }));
		Assert.assertEquals(403, component.getRepository("1", "r").getStatus());
	}

	// updateRepository

	@Test
	public void t0150_test_updateRepository_ok() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { "1" }));
		final Response r = component.updateRepository("1", "r", TestUtils.repository1, null, null, "gitlab", false);
		Assert.assertEquals(404, r.getStatus());
	}

	@Test
	public void t0160_test_updateRepository_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403,
				component.updateRepository("1", "r", TestUtils.repository1, null, null, "gitlab", false).getStatus());
	}

	// deleteRepository ==> Desplazado al final

	// getAsyncTaskStatus

	@Test
	public void t0170_test_getAsyncTaskStatus_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.getAsyncTaskStatus("1", "t").getStatus());
	}

	@Test
	public void t0180_test_getAsyncTaskStatus_ok() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { "1" }));
		Assert.assertEquals(404, component.getAsyncTaskStatus("1", "t").getStatus());
	}

	// getIssueCommits

	@Test
	public void t0190_test_getIssueCommits_ok_nodev() {
		userUtil.setUser(new TestApplicationUser(true, null, new String[] { "1" }));
		propertyDao.deleteProperty(PluginProperties.P_ISSUEPANEL_REQUIREROLE_DEV); // default
																					// ==>
																					// false
		Assert.assertEquals(200, component.getIssueCommits(TEST_PROJECT, TEST_PROJECT + "-1").getStatus());
	}

	@Test
	public void t0195_test_getIssueCommits_ko() {
		userUtil.setUser(new TestApplicationUser(true, null, new String[] { "1" }));
		propertyDao.createProperty(PluginProperties.P_ISSUEPANEL_REQUIREROLE_DEV, "true");
		Assert.assertEquals(403, component.getIssueCommits(TEST_PROJECT, TEST_PROJECT + "-1").getStatus());
	}

	@Test
	public void t0200_test_getIssueCommits_ok() {
		userUtil.setUser(new TestApplicationUser(false, new String[] { TEST_PROJECT }, null));
		Assert.assertEquals(200, component.getIssueCommits(TEST_PROJECT, TEST_PROJECT + "-1").getStatus());
	}

	// deleteRepository

	@Test
	public void t0210_test_deleteRepository_ko() {
		userUtil.setUser(new TestApplicationUser(false, null, null));
		Assert.assertEquals(403, component.deleteRepository(TEST_PROJECT, TestUtils.repository1_key).getStatus());
	}

	@Test
	public void t0220_test_deleteRepository_ok() {
		userUtil.setUser(new TestApplicationUser(false, null, new String[] { TEST_PROJECT }));
		Assert.assertEquals(404, component.deleteRepository(TEST_PROJECT, TestUtils.repository1_key).getStatus());
	}

}
