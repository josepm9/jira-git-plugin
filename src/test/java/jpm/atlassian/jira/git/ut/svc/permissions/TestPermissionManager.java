package jpm.atlassian.jira.git.ut.svc.permissions;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

public class TestPermissionManager extends ATestPermissionManager {

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.atlassian.jira.git.ut.svc.permissions.ATestPermissionManager#
	 * hasPermission(com.atlassian.jira.security.plugin.ProjectPermissionKey,
	 * com.atlassian.jira.project.Project,
	 * com.atlassian.jira.user.ApplicationUser)
	 */
	@Override
	public boolean hasPermission(ProjectPermissionKey permission, Project project, ApplicationUser user) {
		final TestApplicationUser auser = (TestApplicationUser)user;
		if (permission == ProjectPermissions.ADMINISTER_PROJECTS) {
			return auser.projsAsAdmin==null?false:auser.projsAsAdmin.contains(project.getKey());
		}
		else if (permission == ProjectPermissions.VIEW_DEV_TOOLS) {
			return auser.projsAsDev==null?false:auser.projsAsDev.contains(project.getKey());
		}
		return false;
	}

}
