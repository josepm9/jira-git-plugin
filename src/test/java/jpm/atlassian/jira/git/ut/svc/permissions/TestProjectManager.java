package jpm.atlassian.jira.git.ut.svc.permissions;

import com.atlassian.jira.project.Project;

class TestProjectManager extends ATestProjectManager {

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.atlassian.jira.git.ut.svc.permissions.ATestProjectManager#
	 * getProjectObjByKey(java.lang.String)
	 */
	@Override
	public Project getProjectObjByKey(String arg0) {
		return new TestProject(arg0);
	}

}
