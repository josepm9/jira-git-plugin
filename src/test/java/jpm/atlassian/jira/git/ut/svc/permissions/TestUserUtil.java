package jpm.atlassian.jira.git.ut.svc.permissions;

import com.atlassian.jira.user.ApplicationUser;

import jpm.atlassian.jira.git.svc.UserUtil;

class TestUserUtil extends UserUtil {

	ApplicationUser user;

	public TestUserUtil() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.atlassian.jira.git.svc.UserUtil#getLoggedInUser()
	 */
	@Override
	public ApplicationUser getLoggedInUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(ApplicationUser user) {
		this.user = user;
	}
	
	

}
