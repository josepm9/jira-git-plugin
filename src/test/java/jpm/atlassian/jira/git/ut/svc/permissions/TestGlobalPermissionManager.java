package jpm.atlassian.jira.git.ut.svc.permissions;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

class TestGlobalPermissionManager extends ATestGlobalPermissionManager {
	
	@Override
	public boolean hasPermission(GlobalPermissionKey arg0, ApplicationUser arg1) {
		final TestApplicationUser user = (TestApplicationUser)arg1;
		if (arg0 == GlobalPermissionKey.ADMINISTER) {
			return user.admin;
		}
		return false;
	}

}
