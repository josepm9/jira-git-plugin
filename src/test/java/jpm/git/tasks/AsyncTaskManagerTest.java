package jpm.git.tasks;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.svc.rest.AbstractTest;
import jpm.utils.DeleteDirCommand;
import jpm.utils.TestUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AsyncTaskManagerTest extends AbstractTest {

	private final String TEST_PROJECT = "AsyncTaskManagerTest";

	final static Logger LOG = LoggerFactory.getLogger(AsyncTaskManagerTest.class);

	@Test
	public void t0010_before() throws IOException {
		LOG.info("t0010_before: START");
		initializeComponentManager();
	}

	@Test
	public void t0020_testPool() throws InterruptedException {
		LOG.info("t0020_testPool: START");
		Assert.assertNotNull(taskManager);
		for (int i = 0; i < 10; ++i) {
			taskManager.start(new VoidTask(taskManager, 500l, false, "VoidTask" + i));
		}
		// 5 tareas en ejecución, 5 en cola
		Assert.assertEquals("El número de tareas en ejecución no coincide", 5, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 5, taskManager.getQueuedTasksCount());
		// Esperamos 750 milisegundos, sólo debería haber 5 tareas en ejecución
		// y 0 en cola
		Thread.sleep(750l);
		Assert.assertEquals("El número de tareas en ejecución no coincide", 5, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());
		// Esperamos 50 milisegundos, todo debería haber terminado
		Thread.sleep(500l);
		Assert.assertEquals("El número de tareas en ejecución no coincide", 0, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());
	}

	/**
	 * Comprobar la correcta parada del gestor de tareas
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void t0030_testStopPool() throws InterruptedException {
		LOG.info("t0030_testStopPool: START");
		Assert.assertNotNull(taskManager);
		final VoidTask[] voidTasks = new VoidTask[10];
		for (int i = 0; i < 10; ++i) {
			voidTasks[i] = new VoidTask(taskManager, 500l, false, "VoidTask" + i);
			taskManager.start(voidTasks[i]);
		}
		taskManager.shutdown(true, 100l);
		Assert.assertEquals("El número de tareas en ejecución no coincide", 0, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());
		for (VoidTask voidTask : voidTasks) {
			Assert.assertNull(voidTask.executingThread);
		}
	}

	/**
	 * Comprobar que nada arranca con el gestor de tareas parado
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void t0040_testNothingStarts() throws InterruptedException {
		LOG.info("t0040_testNothingStarts: START");

		Assert.assertNotNull(taskManager);
		for (int i = 0; i < 10; ++i) {
			RuntimeException e1 = null;
			try {
				taskManager.start(new VoidTask(taskManager, 500l, false, "VoidTask" + i));
			} catch (RuntimeException e) {
				e1 = e;
			}
			Assert.assertNotNull(e1);
			Assert.assertEquals("TaskManagerImpl on shutdown", e1.getMessage());
		}
		Assert.assertEquals("El número de tareas en ejecución no coincide", 0, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());

		LOG.info("t0040_testNothingStarts: ENDED");
	}

	/**
	 * Comprobar que nada arranca con el gestor de tareas parado
	 * 
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	@Test
	public void t0050_testGetStatus() throws InterruptedException, IOException {
		LOG.info("t0050_testGetStatus: START");
		initializeComponentManager();
		final String key = taskManager.start(new VoidTask(taskManager, 500l, true, "VoidTask"));
		// 1
		Thread.sleep(125l);
		TaskStatus st = taskManager.getTaskStatus(key);
		Assert.assertNotNull(st);
		Assert.assertEquals(Task.ST_RUNNING, st.state);
		Assert.assertEquals("1", st.newOutput);
		// 2
		Thread.sleep(250l);
		st = taskManager.getTaskStatus(key);
		Assert.assertNotNull(st);
		Assert.assertEquals(Task.ST_RUNNING, st.state);
		Assert.assertEquals("2", st.newOutput);
		// 3
		Thread.sleep(750l);
		st = taskManager.getTaskStatus(key);
		Assert.assertNotNull(st);
		Assert.assertEquals(Task.ST_ENDED_OK, st.state);
		Assert.assertEquals("3", st.newOutput);
		// Ended
		Assert.assertEquals("El número de tareas en ejecución no coincide", 0, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());
		LOG.info("t0050_testGetStatus: ENDED");
	}

	/**
	 * Ejecutar un comando casi real... limpiar el repositorio para preparar la
	 * siguiente prueba
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void t0060_testEnsureDeleteRepo() throws InterruptedException {
		LOG.info("t0060_testEnsureDeleteRepo: START");
		final Task<Object> task = new Task<Object>(new DeleteDirCommand(new File(TestUtils.workingFolder)), taskManager, taskManager) {
			@Override
			public AsyncTaskType getTaskType() {
				return AsyncTaskType.Generic;
			}
		};
		final String key = taskManager.start(task);
		while (true) {
			Thread.sleep(50l);
			final TaskStatus st = taskManager.getTaskStatus(key);
			if (st.state == Task.ST_ENDED_OK || st.state == Task.ST_ENDED_KO) {
				break;
			}
		}
		Assert.assertEquals(Task.ST_ENDED_OK, task.state);
		// Ended
		Assert.assertEquals("El número de tareas en ejecución no coincide", 0, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());
		LOG.info("t0060_testEnsureDeleteRepo: ENDED");
	}

	/**
	 * Ejecutar un comando real
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@Test
	public void t0070_testGitClone() throws InterruptedException, IOException {
		LOG.info("t0070_testGitClone: START");
		// Encontrar ejecutable
		TestUtils.findGitExecutable();
		cfg.setGitExecutable(TestUtils.executable);
		// Ejecutar comando (asegurarse de que la carpeta exista!)
		final File d = new File(TestUtils.workingFolder + File.separator + TEST_PROJECT);
		Assert.assertTrue(d.exists() || d.mkdirs());
		final Task<Object> gitClonetask = taskManager.createGitCloneTask(TEST_PROJECT, TestUtils.repository1, null, null);
		final String key = taskManager.start(gitClonetask);
		final long ts = System.currentTimeMillis();
		while (true) {
			Thread.sleep(50l);
			final TaskStatus st = taskManager.getTaskStatus(key);
			System.out.print(st.newOutput);
			if (st.state == Task.ST_ENDED_OK || st.state == Task.ST_ENDED_KO) {
				break;
			}
			// Asegurar que la prueba no se eterniza
			Assert.assertTrue((System.currentTimeMillis() - ts) < 30000l);
		}
		Assert.assertEquals(Task.ST_ENDED_OK, gitClonetask.state);
		// Ended
		Assert.assertEquals("El número de tareas en ejecución no coincide", 0, taskManager.getRunningTasksCount());
		Assert.assertEquals("El número de tareas en cola no coincide", 0, taskManager.getQueuedTasksCount());
		LOG.info("t0070_testGitClone: ENDED");
	}
}
