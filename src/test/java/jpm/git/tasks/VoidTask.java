package jpm.git.tasks;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

public class VoidTask extends Task<Object> {

	final static Logger LOG = LoggerFactory.getLogger(VoidTask.class);

	public VoidTask(final TaskManagerImpl asyncTaskManager, long runMillis, boolean generateOutput,
			final String taskName) {
		super(new VoidGitWrapper(generateOutput, runMillis, taskName), asyncTaskManager, asyncTaskManager);
	}

	@Override
	public AsyncTaskType getTaskType() {
		return AsyncTaskType.Generic;
	}

}
