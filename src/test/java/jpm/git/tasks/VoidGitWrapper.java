package jpm.git.tasks;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import jpm.git.wrapper.AbstractCommand;
import jpm.git.wrapper.GitWrapperException;

public class VoidGitWrapper extends AbstractCommand<Object> {

	final static Logger LOG = LoggerFactory.getLogger(VoidGitWrapper.class);

	protected final boolean generateOutput;
	protected final long runMillis;

	public VoidGitWrapper(boolean generateOutput, long runMillis, final String description) {
		super(null, null, null, null, null, description);
		this.generateOutput = generateOutput;
		this.runMillis = runMillis;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.git.wrapper.GitWrapper#execute()
	 */
	@Override
	public void execute() throws GitWrapperException {
		try {
			if (generateOutput) {
				// 1 antes de empezar
				output("1");
				// 2 a mitad de ejecición
				Thread.sleep(runMillis / 2l);
				output("2");
				// 3 al final
				Thread.sleep(runMillis / 2l);
				output("3");
			} else {
				Thread.sleep(runMillis);
			}
			// Done!
			LOG.info(description + " ended with status 0");
			end(0);
		} catch (InterruptedException e) {
			LOG.info("Thread Interrupted!");
			end(-1);
		}
	}
	
}
