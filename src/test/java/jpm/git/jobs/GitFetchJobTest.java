package jpm.git.jobs;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.TaskInfo;
import jpm.git.persistence.GitRepositoryDAO;
import jpm.git.persistence.properties.PropGitRepositoryDAOImpl;
import jpm.git.svc.GitService;
import jpm.git.svc.GitServiceImpl;
import jpm.git.svc.ServiceException;
import jpm.git.svc.rest.AbstractTest;
import jpm.git.tasks.Task;
import jpm.git.tasks.Task.AsyncTaskType;
import jpm.git.tasks.TaskManagerImpl;
import jpm.git.tasks.impl.GitFetchTask;
import jpm.git.wrapper.AbstractCommand;
import jpm.utils.TestUtils;

public class GitFetchJobTest extends AbstractTest {

	final static Logger LOG = LoggerFactory.getLogger(GitFetchJobTest.class);

	final static int MAX_TASKS = TaskManagerImpl.MAX_TASKS;
	final static int MY_TASKS_COUNT = MAX_TASKS + 1;
	static GitFetchJob gitFetchJob;
	static GitService gitService;

	@BeforeClass
	public static void beforeClass() throws IOException {
		initializeComponentManager();
		final GitRepositoryDAO repoDAO = new PropGitRepositoryDAOImpl(propertyDao);
		gitFetchJob = new GitFetchJobImpl(repoDAO, taskManager);
		gitService = new GitServiceImpl(cfg, taskManager, null, repoDAO, propertyDao);
	}

	@Test
	public void t0010_create_repos_and_sincronize() throws ServiceException, InterruptedException, JobException {
		final String[] prjs = new String[MY_TASKS_COUNT];
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			prjs[i] = "GitFetchJobTest" + i;
		}
		;
		// CLONE REPOSITORIES
		final TaskInfo[] tasksInfo = new TaskInfo[MY_TASKS_COUNT];
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			tasksInfo[i] = gitService.async_createNewRepository(prjs[i], TestUtils.repository3, null, null, "Github",
					false);
		}
		// Now, this fast... 5 tasks running, 1 task queued
		Assert.assertEquals(MAX_TASKS, taskManager.getRunningTasksCount());
		Assert.assertEquals(MY_TASKS_COUNT - MAX_TASKS, taskManager.getQueuedTasksCount());
		// Wait for all repoitories to sincronize...
		TestUtils.waitForTasksToEnd(tasksInfo, 60000l, taskManager);
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			Assert.assertEquals(Task.ST_ENDED_OK, tasksInfo[i].getAsyncTaskStatus().getState());
		}

		// // TODO: move this test from here!
		// // LAUNCH SYNCRONIZATIONS
		// for (int i = 0; i < MY_TASKS_COUNT; ++i) {
		// tasks[i] = gitService.async_synchronizeRepository(prjs[i],
		// TestUtils.repository3_key);
		// }
		// // Now, this fast... 5 tasks running, 1 task queued
		// Assert.assertEquals(MAX_TASKS,
		// man.getTaskManager().getRunningTasksCount());
		// Assert.assertEquals(MY_TASKS_COUNT - MAX_TASKS,
		// man.getTaskManager().getQueuedTasksCount());
		// Esperar a que terminen (con éxito) las tareas de sincronización

		// LAUNCH SYNCRONIZATIONS
		// Modificar los IDs de tarea, conocemos la secuencia...
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			final String newKey = Long.toString(MY_TASKS_COUNT + Long.parseLong(tasksInfo[i].getKey(), 10), 10);
			tasksInfo[i].setKey(newKey);
		}
		gitFetchJob.doJob();
		// Comprobar que todos los comandos actuales son fetch y que se ha
		// lanzado uno sobre cada carpeta de proyecto
		final Set<String> dirSet = new HashSet<>();
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			dirSet.add(
					new File(cfg.getWorkDir() + File.separator + prjs[i] + File.separator + TestUtils.repository3_key)
							.getAbsolutePath());
		}
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			final Task<?> task = taskManager.getTasks().get(tasksInfo[i].getKey());
			Assert.assertTrue(task instanceof GitFetchTask && task.getTaskType() == AsyncTaskType.GitFetch);
			AbstractCommand<?> cmd = TestUtils.getCommand(task);
			dirSet.remove(cmd.getWorkingFolder().getAbsolutePath());
		}
		Assert.assertEquals(0, dirSet.size());
		// // Now, this fast... 5 tasks running, 1 task queued
		Assert.assertEquals(MAX_TASKS, taskManager.getRunningTasksCount());
		Assert.assertEquals(MY_TASKS_COUNT - MAX_TASKS, taskManager.getQueuedTasksCount());
		// Esperar a que terminen (con éxito) las tareas de sincronización
		TestUtils.waitForTasksToEnd(tasksInfo, 60000l, taskManager);
		for (int i = 0; i < MY_TASKS_COUNT; ++i) {
			Assert.assertEquals(Task.ST_ENDED_OK, tasksInfo[i].getAsyncTaskStatus().getState());
		}
		// Hecho
		Assert.assertEquals(0, taskManager.getRunningTasksCount());
		Assert.assertEquals(0, taskManager.getQueuedTasksCount());

	}

}
