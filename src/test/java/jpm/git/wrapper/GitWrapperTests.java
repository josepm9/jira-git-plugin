package jpm.git.wrapper;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.GitCommit;
import jpm.git.util.GitUtil;
import jpm.git.util.IoUtil;
import jpm.git.wrapper.DoWithGitAndJGitRule.DoWithGitAndJGit;
import jpm.utils.TestUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GitWrapperTests {

	final static Logger LOG = LoggerFactory.getLogger(GitWrapperTests.class);
	
	@Rule
	public final DoWithGitAndJGitRule rule = new DoWithGitAndJGitRule();

	/**
	 * Test 1: encontrar el ejecutable 'git'
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@BeforeClass
	public static void t0000_findExecutable() throws InterruptedException, IOException, GitWrapperException {
		TestUtils.findGitExecutable();
	}
	
	/**
	 * Clonar un repositorio privado
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@Ignore
	@DoWithGitAndJGit
	public void t0010_clonePrivRepository() throws InterruptedException, IOException, GitWrapperException {
		Assert.assertNotNull(TestUtils.executable);
		final File dir = new File(TestUtils.workingFolder);
		if (dir.exists()) {
			IoUtil.tryDelete(dir);
		}
		dir.mkdirs();
		final Command<Object> g = CommandFactory.getInstance().createGitCloneCommand(TestUtils.executable, dir,
				TestUtils.repository2, TestUtils.repository2_u, TestUtils.repository2_p);
		g.execute();
		final int result = GitUtil.logExecution(g, null);
		Assert.assertEquals("El comando ha fallado", 0, result);
	}
	
	/**
	 * Clonar un repositorio privado
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@Ignore
	@DoWithGitAndJGit
	public void t0015_synchronizePrivRepository() throws InterruptedException, IOException, GitWrapperException {
		Assert.assertNotNull(TestUtils.executable);
		final File dir = new File(TestUtils.workingFolder + File.separator + TestUtils.repository2_key);
		final Command<Object> g = CommandFactory.getInstance().createGitFetchCommand(TestUtils.executable, dir,
				TestUtils.repository2, TestUtils.repository2_u, TestUtils.repository2_p);
		g.execute();
		final int result = GitUtil.logExecution(g, null);
		Assert.assertEquals("El comando ha fallado", 0, result);
	}
	
	/**
	 * Clonar un repositorio público
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0020_cloneRepository2() throws InterruptedException, IOException, GitWrapperException {
		Assert.assertNotNull(TestUtils.executable);
		final File dir = new File(TestUtils.workingFolder);
		if (dir.exists()) {
			IoUtil.tryDelete(dir);
		}
		dir.mkdirs();
		final Command<Object> g = CommandFactory.getInstance().createGitCloneCommand(TestUtils.executable, dir,
				TestUtils.repository1, null, null);
		g.execute();
		final int result = GitUtil.logExecution(g, null);
		Assert.assertEquals("El comando ha fallado", 0, result);
	}

	/**
	 * sincronizar un repositorio
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0030_synchronizeRepository() throws InterruptedException, IOException, GitWrapperException {
		Assert.assertNotNull(TestUtils.executable);
		final File dir = new File(TestUtils.workingFolder + File.separator + TestUtils.repository1_key);
		final Command<Object> g = CommandFactory.getInstance().createGitFetchCommand(TestUtils.executable, dir,
				TestUtils.repository1, null, null);
		g.execute();
		final int result = GitUtil.logExecution(g, null);
		Assert.assertEquals("El comando ha fallado", 0, result);
	}

	/**
	 * Comprobar el correcto funcionamiento de la búsqueda en el repositorio
	 * (todos los commits)
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0040_searchCommits1() throws InterruptedException, IOException, GitWrapperException {
		searchCommits("", 10, "c5bb0da37dca8913c87ee0405a52a2379325e872");
	}

	/**
	 * Comprobar los valores devueltos por la b�squeda en el repositorio
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0050_searchCommits2() throws InterruptedException, IOException, GitWrapperException {
		final GitCommit[] commits = searchCommits("typos\\.", 2, null);
		// Commit 1
		Assert.assertEquals("873d883cbbc6de667b66349c96148203cdbbb8b1", commits[0].getHash());
		Assert.assertEquals("Roger D. Peng", commits[0].getAuthor());
		Assert.assertEquals("rdpeng@gmail.com", commits[0].getAuthorEmail());
		Assert.assertEquals(new Date(1398175590000l), commits[0].getAuthorDate());
		Assert.assertEquals("Roger D. Peng", commits[0].getCommitter());
		Assert.assertEquals("rdpeng@gmail.com", commits[0].getCommitterEmail());
		Assert.assertEquals(new Date(1398175590000l), commits[0].getCommitterDate());
		Assert.assertEquals("", commits[0].getRefNames());
		Assert.assertEquals("Merge pull request #1 from gustavdelius/master", commits[0].getSubject());
		Assert.assertEquals("Fixed some minor typos.", commits[0].getBody());
		Assert.assertEquals("", commits[0].getNotes());
		// TODO: avoid this error:
		// git command does not detect the modified file from the git pull
		// request
		// jgit api does: we examine both parents and, obviously there is a file
		// modificacion
		if (CommandFactory.getInstance() instanceof JGitCommandFactory) {
			Assert.assertEquals(1, commits[0].getChangedFiles().length);
			Assert.assertEquals("M", commits[0].getChangedFiles()[0].getType());
			Assert.assertEquals("README.md", commits[0].getChangedFiles()[0].getFilePath());
		} else {
			Assert.assertEquals(0, commits[0].getChangedFiles().length);
		}
		// Commit 2
		Assert.assertEquals("4f84c6fc9c58cfcfeb788e74fbde63549ff20100", commits[1].getHash());
		Assert.assertEquals("Gustav Delius", commits[1].getAuthor());
		Assert.assertEquals("gustav.delius@gmail.com", commits[1].getAuthorEmail());
		Assert.assertEquals(new Date(1396991474000l), commits[1].getAuthorDate());
		Assert.assertEquals("Gustav Delius", commits[1].getCommitter());
		Assert.assertEquals("gustav.delius@gmail.com", commits[1].getCommitterEmail());
		Assert.assertEquals(new Date(1396991474000l), commits[1].getCommitterDate());
		Assert.assertEquals("", commits[1].getRefNames());
		Assert.assertEquals("Fixed some minor typos.", commits[1].getSubject());
		Assert.assertEquals("", commits[1].getBody());
		Assert.assertEquals("", commits[1].getNotes());
		Assert.assertEquals(1, commits[1].getChangedFiles().length);
		Assert.assertEquals("M", commits[1].getChangedFiles()[0].getType());
		Assert.assertEquals("README.md", commits[1].getChangedFiles()[0].getFilePath());
	}

	/**
	 * Comprobar encontrar un solo resultado
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0060_searchCommits3() throws InterruptedException, IOException, GitWrapperException {
		final GitCommit[] commits = searchCommits("Initial commit", 1, null);
		// Commit 1
		Assert.assertEquals("27987823fcf81d46a5e2186391addd87c079b879", commits[0].getHash());
		Assert.assertEquals("Roger D. Peng [amelia]", commits[0].getAuthor());
		Assert.assertEquals("rdpeng@gmail.com", commits[0].getAuthorEmail());
		Assert.assertEquals(new Date(1389737332000l), commits[0].getAuthorDate());
		Assert.assertEquals("Roger D. Peng [amelia]", commits[0].getCommitter());
		Assert.assertEquals("rdpeng@gmail.com", commits[0].getCommitterEmail());
		Assert.assertEquals(new Date(1389737332000l), commits[0].getCommitterDate());
		Assert.assertEquals("", commits[0].getRefNames());
		Assert.assertEquals("Initial commit for Programming Assignment 2", commits[0].getSubject());
		Assert.assertEquals("", commits[0].getBody());
		Assert.assertEquals("", commits[0].getNotes());
		Assert.assertEquals(2, commits[0].getChangedFiles().length);
		Assert.assertEquals("A", commits[0].getChangedFiles()[0].getType());
		Assert.assertEquals("README.md", commits[0].getChangedFiles()[0].getFilePath());
		Assert.assertEquals("A", commits[0].getChangedFiles()[1].getType());
		Assert.assertEquals("cachematrix.R", commits[0].getChangedFiles()[1].getFilePath());
	}

	/**
	 * Comprobar no encontrar resultados
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0070_searchCommits4() throws InterruptedException, IOException, GitWrapperException {
		searchCommits("Tarod", 0, null);
	}

	/**
	 * Comprobar buscar por la primera palabra
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0080_searchCommits5() throws InterruptedException, IOException, GitWrapperException {
		final GitCommit[] commits = searchCommits("A few more", 1, "e4eed4153bd237a2dd4532fc3f40ce0e22fd0cb7");
		Assert.assertTrue(commits[0].getSubject().startsWith("A few more"));
	}

	/**
	 * Comprobar buscar por la �ltima palabra
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0090_searchCommits6() throws InterruptedException, IOException, GitWrapperException {
		final GitCommit[] commits = searchCommits("(missing resources)", 1, "5d54a2e5406dedf9d4437a988bf5da1ed7fbc8de");
		Assert.assertTrue(commits[0].getSubject().endsWith("(missing resources)"));
		Assert.assertEquals("", commits[0].getBody());
	}

	/**
	 * Comprobar buscar por TODO el comentario
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0100_searchCommits7() throws InterruptedException, IOException, GitWrapperException {
		final GitCommit[] commits = searchCommits("A few more comments on what to do", 1,
				"e4eed4153bd237a2dd4532fc3f40ce0e22fd0cb7");
		Assert.assertEquals("A few more comments on what to do", commits[0].getSubject());
		Assert.assertEquals("", commits[0].getBody());
	}

	/**
	 * Comprobar buscar token intermedio
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	@Test
	@DoWithGitAndJGit
	public void t0110_searchCommits8() throws InterruptedException, IOException, GitWrapperException {
		searchCommits("#1", 1, "873d883cbbc6de667b66349c96148203cdbbb8b1");
	}

	/**
	 * Utilidad para lanzar una búsqueda y hacer una comprobación sencilla OJO:
	 * tener en cuenta que se realiza sobre el repositorio 1
	 * 
	 * @param token
	 * @param expectedResults
	 * @param firstHash
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws GitWrapperException
	 */
	protected GitCommit[] searchCommits(String token, int expectedResults, String firstHash)
			throws InterruptedException, IOException, GitWrapperException {
		Assert.assertNotNull(TestUtils.executable);
		final File dir = new File(TestUtils.workingFolder + File.separator + TestUtils.repository1_key);
		final Command<GitCommit[]> g = CommandFactory.getInstance().createGitLogIssueCommand(TestUtils.executable, dir,
				token);
		g.execute();
		final StringBuilder commitsString = new StringBuilder();
		final int result = GitUtil.logExecution(g, commitsString);
		Assert.assertEquals("El comando ha fallado", 0, result);
		final GitCommit[] commits = g.getResult();
		Assert.assertNotNull("Ha fallado la interpretaci�n el resultado", commits);
		Assert.assertEquals("Número de commits interpretados incorrecto", expectedResults, commits.length);
		if (firstHash != null) {
			Assert.assertEquals(firstHash, commits[0].getHash());
		}
		return commits;
	}

}
