package jpm.git.wrapper;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoWithGitAndJGitRule implements TestRule {
	
	final static Logger LOG = LoggerFactory.getLogger(DoWithGitAndJGitRule.class);

	protected final static GitCommandFactory GIT = new GitCommandFactory();
	protected final static JGitCommandFactory JGIT = new JGitCommandFactory();

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ java.lang.annotation.ElementType.METHOD })
	public @interface DoWithGitAndJGit {
	}

	private static class DoWithGitAndJGitStatement extends Statement {
		private final Statement statement;

		private DoWithGitAndJGitStatement(Statement statement) {
			this.statement = statement;
		}

		@Override
		public void evaluate() throws Throwable {
			LOG.info("With GIT");
			CommandFactory.setInstance(GIT);
			statement.evaluate();
			LOG.info("With JGIT");
			CommandFactory.setInstance(JGIT);
			statement.evaluate();
		}
	}

	@Override
	public Statement apply(Statement statement, Description description) {
		Statement result = statement;
		DoWithGitAndJGit repeat = description.getAnnotation(DoWithGitAndJGit.class);
		if (repeat != null) {
			result = new DoWithGitAndJGitStatement(statement);
		}
		return result;
	}
}