package jpm.git.statistics;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.GadgetDataRow;
import jpm.git.bo.GadgetDataTable;
import jpm.git.persistence.GitRepositoryDAO;
import jpm.git.persistence.JsonMapper;
import jpm.git.persistence.properties.PropGitRepositoryDAOImpl;
import jpm.git.svc.GadgetsService;
import jpm.git.svc.GadgetsServiceImpl;
import jpm.git.svc.GitService;
import jpm.git.svc.GitServiceImpl;
import jpm.git.svc.ServiceException;
import jpm.git.svc.rest.AbstractTest;
import jpm.utils.TestUtils;

public class CommitsStatisticTest extends AbstractTest {

	final static Logger LOG = LoggerFactory.getLogger(CommitsStatisticTest.class);

	public static final String PRJ = "CommitsStatisticTest";

	static GadgetsService gadgetsService;
	static GitService gitService;

	@BeforeClass
	public static void beforeClass() throws IOException, ServiceException {
		initializeComponentManager();
		final GitRepositoryDAO repoDAO = new PropGitRepositoryDAOImpl(propertyDao);
		gadgetsService = new GadgetsServiceImpl(cfg, taskManager, repoDAO, propertyDao);
		gitService = new GitServiceImpl(cfg, taskManager, null, repoDAO, propertyDao);
		// Clone repos for tests
		gitService.createNewRepository(PRJ, TestUtils.repository1, null, null, "Github", true);
	}

	@Test
	public void t0010_commitsByAuthor_reposGrouped()
			throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
		final GadgetDataTable dataTable = gadgetsService.commitsStatistics(PRJ,
				/* group repositories */ 1, /* count commits */ 1,
				/* group by author */1, /* table */1);
		Assert.assertNotNull(dataTable);
		LOG.info("t0010_commitsByAuthor_reposGrouped:\n" + JsonMapper.getInstance().writeValueAsString(dataTable));
		
		Assert.assertEquals(2, dataTable.getColumns().length);
		Assert.assertEquals("author-email", dataTable.getColumns()[0]);
		Assert.assertEquals("total-commits", dataTable.getColumns()[1]);

		Assert.assertEquals(3, dataTable.getRows().length);
		
		GadgetDataRow dataRow = dataTable.getRows()[0];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("rdpeng@gmail.com", dataRow.getCells()[0]);
		Assert.assertEquals(6, dataRow.getCells()[1]);
		
		dataRow = dataTable.getRows()[1];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("gustav.delius@gmail.com", dataRow.getCells()[0]);
		Assert.assertEquals(1, dataRow.getCells()[1]);
		
		dataRow = dataTable.getRows()[2];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("josepm9@gmail.com", dataRow.getCells()[0]);
		Assert.assertEquals(3, dataRow.getCells()[1]);
	}

	@Test
	public void t0020_filedModifiedByAuthor_reposGrouped()
			throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
		final GadgetDataTable dataTable = gadgetsService.commitsStatistics(PRJ,
				/* group repositories */ 1, /* count files modified */ 2,
				/* group by author */1, /* table */1);
		LOG.info("t0020_filedModifiedByAuthor_reposGrouped:\n" + JsonMapper.getInstance().writeValueAsString(dataTable));
		
		Assert.assertEquals(2, dataTable.getColumns().length);
		Assert.assertEquals("author-email", dataTable.getColumns()[0]);
		Assert.assertEquals("total-files-modified", dataTable.getColumns()[1]);

		Assert.assertEquals(3, dataTable.getRows().length);
		
		GadgetDataRow dataRow = dataTable.getRows()[0];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("rdpeng@gmail.com", dataRow.getCells()[0]);
		Assert.assertEquals(7, dataRow.getCells()[1]);
		
		dataRow = dataTable.getRows()[1];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("gustav.delius@gmail.com", dataRow.getCells()[0]);
		Assert.assertEquals(1, dataRow.getCells()[1]);
		
		dataRow = dataTable.getRows()[2];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("josepm9@gmail.com", dataRow.getCells()[0]);
		Assert.assertEquals(4, dataRow.getCells()[1]);
	}

	@Test
	public void t0030_commitsByRepo()
			throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
		final GadgetDataTable dataTable = gadgetsService.commitsStatistics(PRJ,
				/* do not group repositories */ 0, /* count commits */ 1,
				/* group by repository */2, /* table */1);
		LOG.info("t0030_commitsByRepo:\n" + JsonMapper.getInstance().writeValueAsString(dataTable));
		
		Assert.assertEquals(2, dataTable.getColumns().length);
		Assert.assertEquals("repository", dataTable.getColumns()[0]);
		Assert.assertEquals("total-commits", dataTable.getColumns()[1]);

		Assert.assertEquals(1, dataTable.getRows().length);
		
		GadgetDataRow dataRow = dataTable.getRows()[0];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals(10, dataRow.getCells()[1]);
	}

	@Test
	public void t0040_filedModifiedByRepo()
			throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
		final GadgetDataTable dataTable = gadgetsService.commitsStatistics(PRJ,
				/* do not group repositories */ 0, /* count files modified */ 2,
				/* group by repository */2, /* table */1);
		LOG.info("t0040_filedModifiedByRepo:\n" + JsonMapper.getInstance().writeValueAsString(dataTable));
		
		Assert.assertEquals(2, dataTable.getColumns().length);
		Assert.assertEquals("repository", dataTable.getColumns()[0]);
		Assert.assertEquals("total-files-modified", dataTable.getColumns()[1]);

		Assert.assertEquals(1, dataTable.getRows().length);
		
		GadgetDataRow dataRow = dataTable.getRows()[0];
		Assert.assertEquals(2, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals(12, dataRow.getCells()[1]);
	}
	
	@Test
	public void t0050_commitsByAuthor()
			throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
		final GadgetDataTable dataTable = gadgetsService.commitsStatistics(PRJ,
				/* group repositories */ 0, /* count commits */ 1,
				/* group by author */1, /* table */1);
		Assert.assertNotNull(dataTable);
		LOG.info("t0050_commitsByAuthor:\n" + JsonMapper.getInstance().writeValueAsString(dataTable));
		
		Assert.assertEquals(3, dataTable.getColumns().length);
		Assert.assertEquals("repository", dataTable.getColumns()[0]);
		Assert.assertEquals("author-email", dataTable.getColumns()[1]);
		Assert.assertEquals("total-commits", dataTable.getColumns()[2]);

		Assert.assertEquals(3, dataTable.getRows().length);
		
		GadgetDataRow dataRow = dataTable.getRows()[0];
		Assert.assertEquals(3, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals("rdpeng@gmail.com", dataRow.getCells()[1]);
		Assert.assertEquals(6, dataRow.getCells()[2]);
		
		dataRow = dataTable.getRows()[1];
		Assert.assertEquals(3, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals("gustav.delius@gmail.com", dataRow.getCells()[1]);
		Assert.assertEquals(1, dataRow.getCells()[2]);
		
		dataRow = dataTable.getRows()[2];
		Assert.assertEquals(3, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals("josepm9@gmail.com", dataRow.getCells()[1]);
		Assert.assertEquals(3, dataRow.getCells()[2]);
	}

	@Test
	public void t0060_filedModifiedByAuthor()
			throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
		final GadgetDataTable dataTable = gadgetsService.commitsStatistics(PRJ,
				/* group repositories */ 0, /* count files modified */ 2,
				/* group by author */1, /* table */1);
		LOG.info("t0060_filedModifiedByAuthor:\n" + JsonMapper.getInstance().writeValueAsString(dataTable));
		
		Assert.assertEquals(3, dataTable.getColumns().length);
		Assert.assertEquals("repository", dataTable.getColumns()[0]);
		Assert.assertEquals("author-email", dataTable.getColumns()[1]);
		Assert.assertEquals("total-files-modified", dataTable.getColumns()[2]);

		Assert.assertEquals(3, dataTable.getRows().length);
		
		GadgetDataRow dataRow = dataTable.getRows()[0];
		Assert.assertEquals(3, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals("rdpeng@gmail.com", dataRow.getCells()[1]);
		Assert.assertEquals(7, dataRow.getCells()[2]);
		
		dataRow = dataTable.getRows()[1];
		Assert.assertEquals(3, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals("gustav.delius@gmail.com", dataRow.getCells()[1]);
		Assert.assertEquals(1, dataRow.getCells()[2]);
		
		dataRow = dataTable.getRows()[2];
		Assert.assertEquals(3, dataRow.getCells().length);
		Assert.assertEquals("ProgrammingAssignment2.git", dataRow.getCells()[0]);
		Assert.assertEquals("josepm9@gmail.com", dataRow.getCells()[1]);
		Assert.assertEquals(4, dataRow.getCells()[2]);
	}
}
