package jpm.git.svc.rest;

import java.io.File;
import java.io.IOException;

import jpm.git.config.Configuration;
import jpm.git.config.Property;
import jpm.git.persistence.PropertyDao;
import jpm.git.persistence.file.FilePropertyDaoImpl;
import jpm.git.tasks.TaskManagerImpl;
import jpm.git.util.IoUtil;
import jpm.utils.TestUtils;

public class AbstractTest {

	protected static Configuration cfg;
	protected static PropertyDao<? extends Property> propertyDao;
	protected static TaskManagerImpl taskManager;

	protected static void initializeComponentManager() throws IOException {
		TestUtils.findGitExecutable();
		propertyDao = new FilePropertyDaoImpl(new File(TestUtils.workingFolder + File.separator + "git.properties"));
		final File workDir = new File(TestUtils.workingFolder);
		IoUtil.tryDelete(workDir);
		workDir.mkdirs();
		// FInally, create service
		cfg = new Configuration(propertyDao);
		cfg.setWorkDir(workDir);
		cfg.setGitExecutable(TestUtils.executable);
		taskManager = new TaskManagerImpl(cfg);
	}

}
