package jpm.git.svc.rest;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import jpm.git.bo.GitInfo;
import jpm.git.persistence.properties.PropGitRepositoryDAOImpl;
import jpm.git.svc.GitServiceImpl;
import jpm.git.svc.ServiceException;
import jpm.git.svc.rest.GitRestApi;
import jpm.git.util.GitUtil;

/**
 * Probar API Rest (y servicios). Esta prueba se centra en los servicios
 * administrativos globales.
 * 
 * @author José
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GitAdminServiceTest extends AbstractTest {

	final static Logger LOG = LoggerFactory.getLogger(GitAdminServiceTest.class);

	static GitRestApi component;

	static String gitLocation;

	@Test
	public void t0000_before() throws IOException {
		initializeComponentManager();
		component = new GitRestApi(
				new GitServiceImpl(cfg, taskManager, null, new PropGitRepositoryDAOImpl(propertyDao), propertyDao));
	}

	@Test
	public void t0010_getGit_NotFound() throws InterruptedException {
		cfg.setGitExecutable("got.exe");
		final Response r = component.getGitExecutable(null);
		Assert.assertNotNull(r);
		Assert.assertEquals(200, r.getStatus());
		Assert.assertTrue(r.getEntity() instanceof GitInfo);
		final GitInfo gitInfo = (GitInfo) r.getEntity();
		Assert.assertNotNull(gitInfo);
		Assert.assertEquals("got.exe", gitInfo.getGitLocation());
		Assert.assertNotNull(gitInfo.getError());
		Assert.assertEquals("", gitInfo.getVersionOutput());
	}

	@Test
	public void t0020_findGit() throws InterruptedException {
		final Response r = component.getGitExecutable(Boolean.TRUE);
		Assert.assertNotNull(r);
		Assert.assertEquals(200, r.getStatus());
		Assert.assertTrue(r.getEntity() instanceof GitInfo);
		final GitInfo gitInfo = (GitInfo) r.getEntity();
		Assert.assertNotNull(gitInfo.getGitLocation());
		Assert.assertNull(gitInfo.getError());
		Assert.assertNotNull(gitInfo.getVersionOutput());
		LOG.info(gitInfo.getVersionOutput());
		// Se usa mas adelante
		gitLocation = gitInfo.getGitLocation();
	}

	@Test
	public void t0030_putGit_error() throws InterruptedException {
		final Response r = component.setGitExecutable("got.exe");
		Assert.assertEquals(200, r.getStatus());
		Assert.assertNotNull(r);
		Assert.assertTrue(r.getEntity() instanceof GitInfo);
		final GitInfo gitInfo = (GitInfo) r.getEntity();
		Assert.assertEquals("got.exe", gitInfo.getGitLocation());
		Assert.assertNotNull(gitInfo.getError());
		Assert.assertEquals("", gitInfo.getVersionOutput());
	}

	@Test
	public void t0040_putGit_ok() throws InterruptedException {
		final Response r = component.setGitExecutable(gitLocation);
		Assert.assertNotNull(r);
		Assert.assertEquals(200, r.getStatus());
		Assert.assertTrue(r.getEntity() instanceof GitInfo);
		final GitInfo gitInfo = (GitInfo) r.getEntity();
		Assert.assertNotNull(gitInfo.getGitLocation());
		Assert.assertNull(gitInfo.getError());
		Assert.assertNotNull(gitInfo.getVersionOutput());
		LOG.info(gitInfo.getVersionOutput());
	}

	@Test
	public void t0050_findGit_error() throws InterruptedException, IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException {
		final Field f = GitUtil.class.getDeclaredField("GIT_COMMON_PATHS");
		f.setAccessible(true);
		f.set(null, new String[] { "got.exe" });

		final Response r = component.getGitExecutable(Boolean.TRUE);
		Assert.assertNotNull(r);
		Assert.assertEquals(404, r.getStatus());
		Assert.assertTrue(r.getEntity() instanceof ServiceException);
		final ServiceException response = (ServiceException) r.getEntity();
		Assert.assertNotNull(response.getMessage());
		Assert.assertEquals(ServiceException.ErrorCode.E_NOT_FOUND, response.errorCode);
		LOG.info(response.getMessage());
	}

}
