package jpm.git.svc.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import jpm.git.bo.GitRepository;
import jpm.git.bo.TaskInfo;
import jpm.git.persistence.properties.PropGitRepositoryDAOImpl;
import jpm.git.svc.GitServiceImpl;
import jpm.git.tasks.Task;
import jpm.git.tasks.TaskStatus;
import jpm.git.wrapper.CommandFactory;
import jpm.git.wrapper.GitCommandFactory;
import jpm.utils.TestUtils;

/**
 * Probar API Rest (y servicios). Esta prueba se centra en los servicios de
 * administración de proyectos.
 * 
 * @author José
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectAdminServiceTest extends AbstractTest {

	final static Logger LOG = LoggerFactory.getLogger(ProjectAdminServiceTest.class);

	private static final String TEST_PROJECT = "ProjectAdminServiceTest";
	static GitRestApi component;

	@Test
	public void t0000_before() throws IOException {
		initializeComponentManager();
		component = new GitRestApi(
				new GitServiceImpl(cfg, taskManager, null, new PropGitRepositoryDAOImpl(propertyDao), propertyDao));
	}

	@Test
	public void t0010_cloneRepoOk() throws InterruptedException {
		Response response = component.createNewRepository(TEST_PROJECT, TestUtils.repository1, null, null, "gitlab",
				false, true);
		Assert.assertEquals(202, response.getStatus());
		Assert.assertTrue(response.getEntity() instanceof TaskInfo);
		final TaskInfo taskInfo = (TaskInfo) response.getEntity();
		TaskStatus st = taskInfo.getAsyncTaskStatus();
		Assert.assertNotNull(st);
		final GitRepository repo = taskInfo.getGitRepository();
		Assert.assertNotNull(repo);
		Assert.assertEquals(TestUtils.repository1_key, repo.getRepoKey());
		Assert.assertEquals(TEST_PROJECT, repo.getProjectKey());
		Assert.assertEquals(TestUtils.repository1, repo.getUrl());
		Assert.assertEquals(false, repo.isCreateURLs());
		Assert.assertEquals("gitlab", repo.getType());
		final String taskKey = taskInfo.getKey();
		while (true) {
			LOG.debug(st.getNewOutput());
			Assert.assertNotEquals(Task.ST_STARTUP_FAILED, st.getState());
			Assert.assertNotEquals(Task.ST_ENDED_KO, st.getState());
			if (Task.ST_ENDED_OK == st.getState()) {
				break;
			}
			// Seguir esperando
			Thread.sleep(500l);
			response = component.getAsyncTaskStatus(TEST_PROJECT, taskKey);
			Assert.assertEquals(200, response.getStatus());
			Assert.assertTrue(response.getEntity() instanceof TaskStatus);
			st = (TaskStatus) response.getEntity();
		}
	}

	@Test
	public void t0015_cloneRepoSyncOk() throws InterruptedException {
		Response response = component.createNewRepository(TEST_PROJECT, TestUtils.repository3, null, null, "Github",
				false, false);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertTrue(response.getEntity() instanceof GitRepository);
		final GitRepository repo = (GitRepository) response.getEntity();
		Assert.assertEquals(TestUtils.repository3_key, repo.getRepoKey());
		Assert.assertEquals(TEST_PROJECT, repo.getProjectKey());
		Assert.assertEquals(TestUtils.repository3, repo.getUrl());
		Assert.assertEquals(false, repo.isCreateURLs());
		Assert.assertEquals("Github", repo.getType());
	}

	@Test
	public void t0020_cloneInvalidRepo() throws InterruptedException {
		Response response = component.createNewRepository(TEST_PROJECT, TestUtils.norepository, null, null, "gitlab",
				false, true);
		Assert.assertEquals(202, response.getStatus());
		Assert.assertTrue(response.getEntity() instanceof TaskInfo);
		final TaskInfo taskInfo = (TaskInfo) response.getEntity();
		final GitRepository repo = taskInfo.getGitRepository();
		Assert.assertNotNull(repo);
		Assert.assertEquals(TestUtils.norepository_key, repo.getRepoKey());
		Assert.assertEquals(TEST_PROJECT, repo.getProjectKey());
		Assert.assertEquals(TestUtils.norepository, repo.getUrl());
		Assert.assertEquals(false, repo.isCreateURLs());
		Assert.assertEquals("gitlab", repo.getType());
		TaskStatus st = taskInfo.getAsyncTaskStatus();
		Assert.assertNotNull(st);
		final String taskKey = taskInfo.getKey();
		while (true) {
			LOG.debug(st.getNewOutput());
			Assert.assertNotEquals(Task.ST_STARTUP_FAILED, st.getState());
			Assert.assertNotEquals(Task.ST_ENDED_OK, st.getState());
			if (Task.ST_ENDED_KO == st.getState()) {
				break;
			}
			// Seguir esperando
			Thread.sleep(500l);
			response = component.getAsyncTaskStatus(TEST_PROJECT, taskKey);
			Assert.assertEquals(200, response.getStatus());
			Assert.assertTrue(response.getEntity() instanceof TaskStatus);
			st = (TaskStatus) response.getEntity();
		}
	}

	@Test
	public void t0030_cloneWithStartupError() throws InterruptedException {
		final String gitExecutable = cfg.getGitExecutable();
		try {
			cfg.setGitExecutable("void_git_exe");
			Response response = component.createNewRepository(TEST_PROJECT, TestUtils.norepository, null, null,
					"gitlab", false, true);
			Assert.assertEquals(202, response.getStatus());
			Assert.assertTrue(response.getEntity() instanceof TaskInfo);
			final TaskInfo taskInfo = (TaskInfo) response.getEntity();
			final GitRepository repo = taskInfo.getGitRepository();
			Assert.assertNotNull(repo);
			Assert.assertEquals(TestUtils.norepository_key, repo.getRepoKey());
			Assert.assertEquals(TEST_PROJECT, repo.getProjectKey());
			Assert.assertEquals(TestUtils.norepository, repo.getUrl());
			Assert.assertEquals(false, repo.isCreateURLs());
			Assert.assertEquals("gitlab", repo.getType());
			TaskStatus st = taskInfo.getAsyncTaskStatus();
			Assert.assertNotNull(st);
			final String taskKey = taskInfo.getKey();
			while (true) {
				LOG.debug(st.getNewOutput());
				if (Task.ST_RUNNING != st.getState() && Task.ST_QUEUED != st.getState()) {
					if (CommandFactory.getInstance() instanceof GitCommandFactory) {
						Assert.assertEquals(Task.ST_STARTUP_FAILED, st.getState());
						break;
					} else {
						Assert.assertEquals(Task.ST_ENDED_KO, st.getState());
						break;
					}
				}
				// Seguir esperando
				Thread.sleep(500l);
				response = component.getAsyncTaskStatus(TEST_PROJECT, taskKey);
				Assert.assertEquals(200, response.getStatus());
				Assert.assertTrue(response.getEntity() instanceof TaskStatus);
				st = (TaskStatus) response.getEntity();
			}
		} finally {
			cfg.setGitExecutable(gitExecutable);
		}
	}

	// TODO: otros métodos administrativos
	// async_synchronizeRepository
	// synchronizeRepository
	// public GitRepository[] getRepositories() throws ServiceException;
	// public GitRepository[] getRepositories(String projectKey) throws
	// ServiceException;
	// public GitRepository getRepository(String projectKey, String repoKey)
	// throws ServiceException;
	// public GitRepository updateRepository(String projectKey, String repoKey,
	// String repoURL, String gitRepoManType,
	// Boolean createURLs) throws ServiceException;
	// public GitRepository deleteRepository(String projectKey, String repoKey)
	// throws ServiceException;
	// public TaskStatus getAsyncTaskStatus(String projectKey, String key)
	// throws ServiceException;

}
