package jpm.git.svc.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import jpm.git.bo.GitLogIssue;
import jpm.git.bo.TaskInfo;
import jpm.git.persistence.properties.PropGitRepositoryDAOImpl;
import jpm.git.svc.GitServiceImpl;
import jpm.git.tasks.Task;
import jpm.git.tasks.TaskStatus;
import jpm.utils.TestUtils;

/**
 * Probar API Rest (y servicios). Esta prueba se centra en los servicios de
 * desarrollador (commits).
 * 
 * @author José
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeveloperServicesTest extends AbstractTest {

	final static Logger LOG = LoggerFactory.getLogger(DeveloperServicesTest.class);

	private static final String TEST_PROJECT = "DeveloperServicesTest";
	static GitRestApi component;

	@BeforeClass
	public static void t0000_before() throws IOException, InterruptedException {
		initializeComponentManager();
		component = new GitRestApi(
				new GitServiceImpl(cfg, taskManager, null, new PropGitRepositoryDAOImpl(propertyDao), propertyDao));
		// Clone repository for testing
		Response response = component.createNewRepository(TEST_PROJECT, TestUtils.repository1, null, null, "Github", false, true);
		Assert.assertEquals(202, response.getStatus());
		// Wait until finished
		final String taskKey = ((TaskInfo) response.getEntity()).getKey();
		TaskStatus st = ((TaskInfo) response.getEntity()).getAsyncTaskStatus();
		while (st.getState() != Task.ST_ENDED_KO && st.getState() != Task.ST_ENDED_OK) {
			Thread.sleep(1000l);
			response = component.getAsyncTaskStatus(TEST_PROJECT, taskKey);
			Assert.assertEquals(200, response.getStatus());
			st = (TaskStatus) response.getEntity();
		}
		LOG.info("DeveloperServicesTest.t0000_before");
		Assert.assertEquals(Task.ST_ENDED_OK, st.getState());
	}

	@Test
	public void t0010_Commits() throws InterruptedException {
		final Response r = component.getIssueCommits(TEST_PROJECT, "Assignment 2");
		Assert.assertEquals(200, r.getStatus());
		Assert.assertNotNull(r.getEntity());
		Assert.assertTrue(r.getEntity() instanceof GitLogIssue[]);
		final GitLogIssue[] logs = (GitLogIssue[]) r.getEntity();
		Assert.assertEquals(1, logs.length);
		Assert.assertNotNull(logs[0].getCommits());
		Assert.assertEquals(3, logs[0].getCommits().length);
		Assert.assertEquals("5d54a2e5406dedf9d4437a988bf5da1ed7fbc8de", logs[0].getCommits()[0].getHash());
		Assert.assertEquals("b30be7cb56f2e3605113a706340acd3273ab1097", logs[0].getCommits()[1].getHash());
		Assert.assertEquals("27987823fcf81d46a5e2186391addd87c079b879", logs[0].getCommits()[2].getHash());
	}

	@Test
	public void t0020_Commits_noproject() throws InterruptedException {
		final Response r = component.getIssueCommits(TEST_PROJECT+"no", "Assignment 2");
		Assert.assertEquals(200, r.getStatus());
		Assert.assertNotNull(r.getEntity());
		Assert.assertTrue(r.getEntity() instanceof GitLogIssue[]);
		final GitLogIssue[] logs = (GitLogIssue[]) r.getEntity();
		Assert.assertEquals(0, logs.length);
	}

	@Test
	public void t0030_Commits_nocommits() throws InterruptedException {
		final Response r = component.getIssueCommits(TEST_PROJECT, "Elric");
		Assert.assertEquals(200, r.getStatus());
		Assert.assertNotNull(r.getEntity());
		Assert.assertTrue(r.getEntity() instanceof GitLogIssue[]);
		final GitLogIssue[] logs = (GitLogIssue[]) r.getEntity();
		Assert.assertEquals(0, logs.length);
	}
}
