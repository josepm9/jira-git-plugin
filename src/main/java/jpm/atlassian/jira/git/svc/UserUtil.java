package jpm.atlassian.jira.git.svc;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;

public class UserUtil {
	
	public ApplicationUser getLoggedInUser() {
		return ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
	}

}
