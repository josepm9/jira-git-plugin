package jpm.atlassian.jira.git.svc;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;

import jpm.atlassian.jira.git.PluginProperties;
import jpm.git.bo.PropertyBO;
import jpm.git.config.Configuration;
import jpm.git.svc.GitApiMethodFilter;
import jpm.git.svc.ServiceException;
import jpm.git.svc.ServiceException.ErrorCode;

public class GitApiMethodFilterImpl implements GitApiMethodFilter {

	final GlobalPermissionManager globalPermissionManager;
	final PermissionManager permissionManager;
	final ProjectManager projectManager;
	final UserUtil userUtil;
	final Configuration cfg;

	public GitApiMethodFilterImpl(final GlobalPermissionManager globalPermissionManager,
			final PermissionManager permissionManager, final UserUtil userUtil, final ProjectManager projectManager,
			final Configuration cfg) {
		this.globalPermissionManager = globalPermissionManager;
		this.permissionManager = permissionManager;
		this.userUtil = userUtil;
		this.projectManager = projectManager;
		this.cfg = cfg;
	}

	@Override
	public void findGitExecutable() throws ServiceException {
		assertAdmin();
	}

	@Override
	public void getGitExecutable() throws ServiceException {
		assertAdmin();
	}

	@Override
	public void setGitExecutable(String git) throws ServiceException {
		assertAdmin();
	}

	@Override
	public void createNewRepository(String projectKey, String repoURL, String authUser, String authPassword,
			String gitRepoManType, Boolean createURLs) throws ServiceException {
		assertProjectAdmin(projectKey);
	}

	@Override
	public void synchronizeRepository(String projectKey, String repoKey) throws ServiceException {
		assertProjectAdmin(projectKey);
	}
	
	@Override
	public void getRepositories() throws ServiceException {
		assertAdmin();
	}

	@Override
	public void getRepositories(String projectKey) throws ServiceException {
		assertProjectAdmin(projectKey);
	}

	@Override
	public void getRepository(String projectKey, String repoKey) throws ServiceException {
		assertProjectAdmin(projectKey);
	}

	@Override
	public void updateRepository(String projectKey, String repoKey, String repoURL, String authUser,
			String authPassword, String gitRepoManType, Boolean createURLs) throws ServiceException {
		assertProjectAdmin(projectKey);
	}

	@Override
	public void deleteRepository(String projectKey, String repoKey) throws ServiceException {
		assertProjectAdmin(projectKey);
	}

	@Override
	public void getAsyncTaskStatus(String projectKey, String key) throws ServiceException {
		assertProjectAdmin(projectKey);
	}

	@Override
	public void getIssueCommits(String projectKey, String issueKey) throws ServiceException {
		assertProjectDeveloper(projectKey);
	}

	public void assertAdmin() throws ServiceException {
		if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, userUtil.getLoggedInUser())) {
			throw new ServiceException(ErrorCode.E_FORBIDDEN, "Forbidden operation", null);
		}
	}

	public void assertProjectAdmin(String projectKey) throws ServiceException {
		if (!permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS,
				projectManager.getProjectObjByKey(projectKey), userUtil.getLoggedInUser())) {
			throw new ServiceException(ErrorCode.E_FORBIDDEN, "Forbidden operation", null);
		}
	}

	public void assertProjectDeveloper(String projectKey) throws ServiceException {
		if (PluginProperties.getIssuePanelRequireRoleDev(cfg)
				&& !permissionManager.hasPermission(ProjectPermissions.VIEW_DEV_TOOLS,
						projectManager.getProjectObjByKey(projectKey), userUtil.getLoggedInUser())) {
			throw new ServiceException(ErrorCode.E_FORBIDDEN, "Forbidden operation", null);
		}
	}

	@Override
	public void getProperties() throws ServiceException {
		assertAdmin();
	}

	@Override
	public void getProperty(String key) throws ServiceException {
		assertAdmin();
	}

	@Override
	public void updateProperty(String key, PropertyBO p) throws ServiceException {
		assertAdmin();
	}

	@Override
	public void createProperty(PropertyBO bean) throws ServiceException {
		assertAdmin();
	}

	@Override
	public void deleteProperty(String key) throws ServiceException {
		assertAdmin();
	}

}
