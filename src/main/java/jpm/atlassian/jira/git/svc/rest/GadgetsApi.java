package jpm.atlassian.jira.git.svc.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import jpm.atlassian.jira.git.bo.gadgets.ErrorCollection;
import jpm.atlassian.jira.git.bo.gadgets.ValidationError;
import jpm.atlassian.jira.git.svc.UserUtil;
import jpm.git.svc.GadgetsService;
import jpm.git.svc.ServiceException;

@Path("/gadgets")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GadgetsApi {

	final static Logger LOG = LoggerFactory.getLogger(GadgetsApi.class);

	public static final String VAL_ERR_NOUSER = "gadget.common.val.nouser";
	public static final String VAL_ERR_NOPRJ = "gadget.common.val.noprj";
	public static final String VAL_ERR_NOPRJBROWSE = "gadget.common.val.noprjbrowse";
	public static final String VAL_ERR_NOREPOGRP = "gadget.common.val.norepogrp";
	public static final String VAL_ERR_NOENTITY = "gadget.common.val.noentity";
	public static final String VAL_ERR_NOGROUP = "gadget.common.val.nogroup";
	public static final String VAL_ERR_NOTYPE = "gadget.common.val.notype";
	public static final String VAL_ERR_REPOGRP_GRPBY = "gadget.common.val.repogrpgrpby";

	protected final GadgetsService gadgetSvc;
	protected final GlobalPermissionManager globalPermissionManager;
	protected final PermissionManager permissionManager;
	protected final ProjectManager projectManager;
	protected final UserUtil userUtil;

	protected static class GadgetValidation {
		final ErrorCollection errors = new ErrorCollection();
		ApplicationUser user = null;
		Project project = null;
	}

	public GadgetsApi(final GadgetsService gadgetSvc, final GlobalPermissionManager globalPermissionManager,
			final PermissionManager permissionManager, final UserUtil userUtil, final ProjectManager projectManager) {
		this.gadgetSvc = gadgetSvc;
		this.globalPermissionManager = globalPermissionManager;
		this.permissionManager = permissionManager;
		this.userUtil = userUtil;
		this.projectManager = projectManager;
	}

	/**
	 * 
	 * @param projectId
	 * @param repository
	 * @param entity
	 * @param group
	 * @param type
	 * @return
	 */
	@Path("/commits-statistics/val")
	@GET
	public Response validateCommitsStatistics(@QueryParam(value = "project") String projectId,
			@QueryParam(value = "repository") Integer repository, @QueryParam(value = "entity") Integer entity,
			@QueryParam(value = "group") Integer group, @QueryParam(value = "type") Integer type) {
		return Response.ok(doValidateCommitsStatistics(projectId, repository, entity, group, type).errors).build();
	}

	/**
	 * 
	 * @param projectId
	 * @param repository
	 * @param entity
	 * @param group
	 * @param type
	 * @return
	 */
	protected GadgetValidation doValidateCommitsStatistics(String projectId, Integer repository, Integer entity,
			Integer group, Integer type) {
		final GadgetValidation gadgetValidation = doValidateProject(projectId);
		final ErrorCollection errors = gadgetValidation.errors;
		if (repository == null || repository < 0 || repository > 1) {
			errors.getErrors().add(new ValidationError("repository", VAL_ERR_NOREPOGRP, null));
		}
		if (entity == null || entity < 1 || entity > 2) {
			errors.getErrors().add(new ValidationError("entity", VAL_ERR_NOENTITY, null));
		}
		if (group == null || group < 1 || group > 2) {
			errors.getErrors().add(new ValidationError("group", VAL_ERR_NOGROUP, null));
		}
		if (type == null || type < 1 || type > 7) {
			errors.getErrors().add(new ValidationError("type", VAL_ERR_NOTYPE, null));
		}
		if (repository!=null && group!=null && repository==1 && group==2) {
			errors.getErrors().add(new ValidationError("type", VAL_ERR_REPOGRP_GRPBY, null));
		}
		return gadgetValidation;
	}

	/**
	 * 
	 * @param projectId
	 * @return
	 */
	protected GadgetValidation doValidateProject(String projectId) {
		final GadgetValidation val = new GadgetValidation();
		val.user = userUtil.getLoggedInUser();
		Long projectLongId = null;
		if (projectId == null) {
			val.errors.getErrors().add(new ValidationError("project", VAL_ERR_NOPRJ, null));
		} else {
			projectId = projectId.startsWith("project-") ? projectId.substring(8) : projectId;
			try {
				projectLongId = Long.parseLong(projectId, 10);
			} catch (NumberFormatException e) {
				LOG.error("NumberFormatException(projectId): " + projectId);
				val.errors.getErrors().add(new ValidationError("project", VAL_ERR_NOPRJ, null));
			}
		}
		if (val.user == null) {
			val.errors.getErrorMessages().add(VAL_ERR_NOUSER);
		}
		if (val.user != null && projectLongId != null) {
			val.project = projectManager.getProjectObj(projectLongId);
			if (val.project == null) {
				val.errors.getErrors().add(new ValidationError("project", VAL_ERR_NOPRJ, null));
			} else if (!permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, val.project,
					userUtil.getLoggedInUser())) {
				val.errors.getErrors().add(new ValidationError("project", VAL_ERR_NOPRJBROWSE, null));
			}
		}
		return val;
	}

	/**
	 * 
	 * @param projectId
	 * @param repository
	 * @param entity
	 * @param group
	 * @param type
	 * @return
	 */
	@Path("/commits-statistics")
	@GET
	public Response commitsStatistics(@QueryParam(value = "project") String projectId,
			@QueryParam(value = "repository") Integer repository, @QueryParam(value = "entity") Integer entity,
			@QueryParam(value = "group") Integer group, @QueryParam(value = "type") Integer type) {
		final GadgetValidation val = doValidateCommitsStatistics(projectId, repository, entity, group, type);
		if (val.errors.getErrors().size() > 0 || val.errors.getErrorMessages().size() > 0) {
			return Response.status(400).entity(val.errors)
					.build();
		}
		;
		try {
			return Response.ok(gadgetSvc.commitsStatistics(val.project.getKey(), repository, entity, group, type))
					.build();

		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}
}
