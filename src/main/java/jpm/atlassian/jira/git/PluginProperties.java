package jpm.atlassian.jira.git;

import jpm.git.config.Configuration;

public class PluginProperties {

	public static final String P_ISSUEPANEL_REQUIREROLE_DEV = "_/issuepanel.requirerole.developer";

	/**
	 * Flag to force developer role to be able to see git issue panel.
	 * @param cfg
	 * @return
	 */
	public static final boolean getIssuePanelRequireRoleDev(final Configuration cfg) {
		return "true".equals(cfg.getPropertyValue(P_ISSUEPANEL_REQUIREROLE_DEV, "false"));
	}
}
