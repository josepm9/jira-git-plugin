package jpm.atlassian.jira.git;

import java.io.File;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.Schedule;

import jpm.atlassian.jira.git.jobs.GitFetchJobRunner;
import jpm.git.config.Configuration;
import jpm.git.jobs.GitFetchJob;
import jpm.git.persistence.PropertyDao;
import jpm.git.tasks.TaskManager;

/**
 * Clase utilizada para crear los componentes de git
 * 
 * @author José
 *
 */
public class PluginConfiguration implements InitializingBean, DisposableBean {

	final static Logger LOG = LoggerFactory.getLogger(PluginConfiguration.class);

	/**
	 * Gestor de tareas programadas de atlassian
	 */
	protected final SchedulerService schedulerService;

	/**
	 * Gestor de tareas
	 */
	protected final TaskManager taskManager;

	/**
	 * Trabajo de sincronización de repositorios git
	 */
	protected final GitFetchJob gitFetchJob;

	/**
	 * Configuración del plugin
	 */
	protected final Configuration cfg;

	public PluginConfiguration(PropertyDao<?> dao, final SchedulerService schedulerService,
			final GitFetchJob gitFetchJob, final TaskManager taskManager, final Configuration cfg) {
		this.cfg = cfg;
		this.taskManager = taskManager;
		this.schedulerService = schedulerService;
		this.gitFetchJob = gitFetchJob;
		cfg.setWorkDir(getPluginHome());
	}

	/**
	 * Obtener la carpeta para almacenar los datos del plugin
	 * 
	 * @return
	 */
	protected File getPluginHome() {
		// JiraHome para obtener workDir
		JiraHome jiraHome;
		try {
			jiraHome = ComponentAccessor.getComponentOfType(JiraHome.class);
		} catch (java.lang.IllegalStateException e) {
			LOG.error("ComponentAccessor en estado inválido, ¿estamos en una prueba unitaria?");
			return null;
		}
		// getHome devuelve "shared home", "home" replicada en el cluster
		final File f = new File(jiraHome.getHome() + File.separator + "gitplugin");
		if (!f.exists()) {
			if (!f.mkdir()) {
				LOG.error("Sin permisos de escritura para crear: " + f.getAbsolutePath());
			}
		}
		return f;
	}

	/**
	 * Registrar las tareas progradas del plugin: git fetch
	 * 
	 * @throws Exception
	 */
	protected void registerJobs() throws Exception {
		if (schedulerService == null) {
			LOG.warn("Won't register any job as schedulerService is null (is this a test?)");
			return;
		}
		schedulerService.registerJobRunner(GitFetchJobRunner.KEY, new GitFetchJobRunner(gitFetchJob));
		final Schedule schedule = Schedule.forInterval(GitFetchJobRunner.DEFAULT_JOB_INTERVAL_MILLIS,
				new Date(System.currentTimeMillis() + GitFetchJobRunner.DEFAULT_JOB_INTERVAL_MILLIS / 2l));
		final JobConfig jobConfig = JobConfig.forJobRunnerKey(GitFetchJobRunner.KEY).withSchedule(schedule);
		schedulerService.scheduleJob(GitFetchJobRunner.JOBID, jobConfig);
	}

	/**
	 * Eliminar el registro de tareas progradas del plugin: git fetch
	 * 
	 * @throws Exception
	 */
	protected void unregisterJobs() throws Exception {
		if (schedulerService == null) {
			return;
		}
		schedulerService.unscheduleJob(GitFetchJobRunner.JOBID);
		schedulerService.unregisterJobRunner(GitFetchJobRunner.KEY);
	}

	// /////////////////////////////////////////
	// InitializingBean && DisposableBean

	@Override
	public void afterPropertiesSet() throws Exception {
		registerJobs();
	}

	@Override
	public void destroy() throws Exception {
		try {
			taskManager.shutdown(true, 10000l);
		} catch (Exception e) {
			LOG.error("Exception (" + e.getClass().getName() + ") while shuting down task manager: " + e.getMessage(),
					e);
		}
		try {
			unregisterJobs();
		} catch (Exception e) {
			LOG.error("Exception (" + e.getClass().getName() + ") while unregistering jobs: " + e.getMessage(), e);
		}
	}
}
