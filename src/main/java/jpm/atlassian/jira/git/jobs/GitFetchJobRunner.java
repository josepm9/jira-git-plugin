package jpm.atlassian.jira.git.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.ComponentManager;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;

import jpm.git.jobs.GitFetchJob;
import jpm.git.jobs.JobException;

public class GitFetchJobRunner implements JobRunner {

	final static Logger LOG = LoggerFactory.getLogger(GitFetchJobRunner.class);

	public static final JobRunnerKey KEY = JobRunnerKey.of(GitFetchJobRunner.class.getName());
	public static final JobId JOBID = JobId.of(GitFetchJobRunner.class.getName());
	public static final long DEFAULT_JOB_INTERVAL_MILLIS = 60000l * 30l; // Cada
																			// 30
																			// minutos

	protected final GitFetchJob gitFetchJob;

	public GitFetchJobRunner(final GitFetchJob gitFetchJob) {
		this.gitFetchJob = gitFetchJob;
	}

	@Override
	public JobRunnerResponse runJob(JobRunnerRequest arg0) {
		LOG.info("GitFetchJobRunner-[START]");
		try {
			if (this.gitFetchJob == null) {
				ComponentManager.getComponentInstanceOfType(GitFetchJob.class).doJob();
			} else {
				gitFetchJob.doJob();
			}
		} catch (JobException e) {
			return JobRunnerResponse.failed(e.getMessage());
		} catch (Exception e) {
			LOG.error("GitFetchJobRunner-[ERROR]: " + e.getMessage(), e);
			return JobRunnerResponse.failed(e);
		} finally {
			LOG.info("GitFetchJobRunner-[END]");
		}
		return JobRunnerResponse.success();
	}

}
