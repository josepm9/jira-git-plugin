package jpm.atlassian.jira.git.ao;

import com.atlassian.activeobjects.external.ActiveObjects;

import jpm.git.persistence.PropertyDao;
import net.java.ao.DBParam;
import net.java.ao.Query;

/**
 * Gestor de acceso a las propiedades en BBDD
 * 
 * @author José
 *
 */
public class AoPropertyDaoImpl implements PropertyDao<AoProperty> {

	final ActiveObjects ao;

	public AoPropertyDaoImpl(final ActiveObjects ao) {
		this.ao = ao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#createProperty(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AoProperty createProperty(String key, String value) {
		final AoProperty p = ao.create(AoProperty.class, new DBParam("KEY", key), new DBParam("VALUE", value));
		p.save();
		return p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#readProperties(java.lang.String)
	 */
	@Override
	public AoProperty[] readProperties(String keyPrefix) {
		keyPrefix += "%";
		final Query query = Query.select().where("KEY like ?", keyPrefix);
		return ao.find(AoProperty.class, query);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.atlassian.jira.git.ao.PropertyDao#readProperty(java.lang.String)
	 */
	@Override
	public AoProperty readProperty(String key) {
		return ao.get(AoProperty.class, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.atlassian.jira.git.ao.PropertyDao#readProperty(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AoProperty readProperty(String key, String defaultValue) {
		AoProperty p = ao.get(AoProperty.class, key);
		if (p == null) {
			p = ao.create(AoProperty.class, new DBParam("KEY", key), new DBParam("VALUE", defaultValue));
			p.save();
		}
		return p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#updateProperty(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AoProperty updateProperty(String key, String value) {
		AoProperty p = ao.get(AoProperty.class, key);
		if (p == null) {
			return null;
		} else {
			p.setValue(value);
			p.save();
			return p;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#updateProperty(jpm.atlassian.jira.
	 * git.ao.AoProperty)
	 */
	@Override
	public AoProperty updateProperty(AoProperty p) {
		p.save();
		return p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#upsertProperty(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AoProperty upsertProperty(String key, String value) {
		AoProperty p = ao.get(AoProperty.class, key);
		if (p == null) {
			p = ao.create(AoProperty.class, new DBParam("KEY", key), new DBParam("VALUE", value));
			p.save();
			return p;
		} else {
			p.setValue(value);
			p.save();
			return p;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#deleteProperty(java.lang.String)
	 */
	@Override
	public AoProperty deleteProperty(String key) {
		final AoProperty p = ao.get(AoProperty.class, key);
		if (p != null) {
			ao.delete(p);
		}
		return p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jpm.atlassian.jira.git.ao.PropertyDao#deleteProperty(jpm.atlassian.jira.
	 * git.ao.AoProperty)
	 */
	@Override
	public void deleteProperty(AoProperty p) {
		ao.delete(p);
	}
}
