package jpm.atlassian.jira.git.ao;

import jpm.git.config.Property;
import net.java.ao.RawEntity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;
import net.java.ao.schema.StringLength;

public interface AoProperty extends RawEntity<String>, Property {

	@Override
	@NotNull
	@PrimaryKey("KEY")
	@StringLength(value = 450)
	public String getKey();

	@Override
	public void setKey(String key);

	@Override
	@NotNull
	@StringLength(value = StringLength.MAX_LENGTH)
	public String getValue();

	@Override
	public void setValue(String value);

}
