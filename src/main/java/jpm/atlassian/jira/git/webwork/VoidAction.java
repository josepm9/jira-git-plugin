package jpm.atlassian.jira.git.webwork;

import com.atlassian.jira.web.action.JiraWebActionSupport;

public class VoidAction extends JiraWebActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9138185544397420718L;

	/* (non-Javadoc)
	 * @see webwork.action.ActionSupport#doExecute()
	 */
	@Override
	protected String doExecute() throws Exception {
		return "view";
	}
	
}
