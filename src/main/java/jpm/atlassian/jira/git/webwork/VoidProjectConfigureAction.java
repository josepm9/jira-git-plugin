package jpm.atlassian.jira.git.webwork;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class VoidProjectConfigureAction extends JiraWebActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9138185544397420718L;

	String projectKey;
	Project project;

	public VoidProjectConfigureAction() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see webwork.action.ActionSupport#doExecute()
	 */
	@Override
	protected String doExecute() throws Exception {
		if (hasPermission()) {
			return "view";
		} else {
			return "no-permission";
		}
	}

	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
		this.project = getProjectManager().getProjectObjByKey(projectKey);
	}
	
	public String getProjectKey() {
		return projectKey;
	}

	public boolean hasPermission() {
		return project == null ? false
				: getPermissionManager().hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project,
						getLoggedInUser());
	}

}
