package jpm.atlassian.jira.git.bo.gadgets;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ValidationError {
	/**
	 * The field the error relates to
	 */
	@XmlElement
	private String field;

	/**
	 * The Error key...
	 */
	@XmlElement
	private String error;

	@XmlElement
	private Collection<String> params;

	public ValidationError() {
	}
	
	
	public ValidationError(String field, String error, Collection<String> params) {
		super();
		this.field = field;
		this.error = error;
		this.params = params;
	}



	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field
	 *            the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the params
	 */
	public Collection<String> getParams() {
		return params;
	}

	/**
	 * @param params
	 *            the params to set
	 */
	public void setParams(Collection<String> params) {
		this.params = params;
	}

}