package jpm.atlassian.jira.git.bo.gadgets;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorCollection
{
    /**
     * Generic error messages
     */
    @XmlElement
    private Collection<String> errorMessages = new ArrayList<String>();
 
    /**
     * Errors specific to a certain field.
     */
    @XmlElement
    private Collection<ValidationError> errors = new ArrayList<ValidationError>();

	/**
	 * @return the errorMessages
	 */
	public Collection<String> getErrorMessages() {
		return errorMessages;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(Collection<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * @return the errors
	 */
	public Collection<ValidationError> getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(Collection<ValidationError> errors) {
		this.errors = errors;
	}
 
    
}