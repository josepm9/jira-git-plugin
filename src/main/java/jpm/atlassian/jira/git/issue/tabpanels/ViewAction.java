package jpm.atlassian.jira.git.issue.tabpanels;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.user.util.UserManager;

import jpm.git.bo.GitCommit;
import jpm.git.bo.GitLogIssue;
import jpm.git.persistence.JsonMapper;
import jpm.git.svc.GitService;
import jpm.git.svc.ServiceException;

class ViewAction extends AbstractIssueAction {

	final static Logger LOG = LoggerFactory.getLogger(ViewAction.class);

	final Issue issue;
	final Project project;
	final ApplicationUser user;
	final AvatarService avatarService;
	final UserManager userManager;
	final GitService gitService;

	final String gitLogIssuesJson;
	String projectAvatarURL = null;
	String userAvatarsJson = null;

	public ViewAction(IssueTabPanelModuleDescriptor descriptor, Issue issue, ApplicationUser user,
			final AvatarService avatarService, final UserManager userManager, final GitService gitService) {
		super(descriptor);
		this.issue = issue;
		this.user = user;
		this.project = issue.getProjectObject();
		this.avatarService = avatarService;
		this.userManager = userManager;
		this.gitService = gitService;
		gitLogIssuesJson = doGetGitLogIssues();
	}

	@Override
	public Date getTimePerformed() {
		return issue.getCreated();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void populateVelocityParams(@SuppressWarnings("rawtypes") Map arg0) {
		arg0.put("action", this);
		arg0.put("issue", issue);
		arg0.put("project", project);
		arg0.put("user", user);
		arg0.put("gitLogIssuesJsonHtml", gitLogIssuesJson);
		arg0.put("projectAvatarURL", projectAvatarURL);
		arg0.put("userAvatarsJsonHtml", userAvatarsJson);
	}

	// ///////////////////////////////////////////
	// Commits, avatars...

	/**
	 * Obtener commits, avatar...
	 * 
	 * @return
	 */
	protected String doGetGitLogIssues() {
		GitLogIssue[] gitLogIssues = null;
		try {
			gitLogIssues = gitService.getIssueCommits(project.getKey(), issue.getKey());
		} catch (ServiceException e) {
		}
		if (gitLogIssues == null) {
			return "[]";
		}
		// Default user avatar
		final String defaultUserAvatarURL = avatarService.getAvatarURL(user, (ApplicationUser) null, Avatar.Size.MEDIUM).toString();
		// Cargar avatars de usuarios
		Map<String, String> userAvatarsURLs = new HashMap<>();
		for (GitLogIssue gitLogIssue : gitLogIssues) {
			for (GitCommit commit : gitLogIssue.getCommits()) {
				if (!userAvatarsURLs.containsKey(commit.getAuthorEmail())) {
					userAvatarsURLs.put(commit.getAuthorEmail(),
							doGetUserAvatar(commit.getAuthor(), commit.getAuthorEmail(), defaultUserAvatarURL));
				}
			}

		}
		try {
			userAvatarsJson = JsonMapper.getInstance().writeValueAsString(userAvatarsURLs);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			userAvatarsJson = "null";
		}
		// Project avatar
		URI url = avatarService.getProjectAvatarURL(project, Avatar.Size.MEDIUM);
		if (url == null) {
			url = avatarService.getProjectDefaultAvatarURL(Avatar.Size.MEDIUM);
		}
		projectAvatarURL = url.toString();
		// DOne
		try {
			return JsonMapper.getInstance().writeValueAsString(gitLogIssues);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return "[]";
		}
	}

	/**
	 * Obtener el avatar de un usuario
	 * 
	 * @param author
	 * @param authorEmail
	 * @return
	 */
	protected String doGetUserAvatar(String author, String authorEmail, String defaultUserAvatarURL) {
		// Buscar usuario
		ApplicationUser avatarUser = userManager.getUserByKey(author);
		if (avatarUser == null) {
			avatarUser = UserUtils.getUser(author);
		}
		if (avatarUser == null) {
			avatarUser = UserUtils.getUserByEmail(authorEmail);
		}
		// Retornar avatar
		return avatarUser == null ? defaultUserAvatarURL
				: avatarService.getAvatarURL(user, avatarUser, Avatar.Size.SMALL).toString();
	}

}
