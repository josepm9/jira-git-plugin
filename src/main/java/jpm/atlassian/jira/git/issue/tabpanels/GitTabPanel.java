package jpm.atlassian.jira.git.issue.tabpanels;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import jpm.atlassian.jira.git.PluginProperties;
import jpm.git.config.Configuration;
import jpm.git.svc.GitService;

public class GitTabPanel extends AbstractIssueTabPanel {

	final PermissionManager permissionManager;
	final Configuration cfg;
	final AvatarService avatarService;
	final UserManager userManager;
	final GitService gitService;

	public GitTabPanel(final PermissionManager permissionManager, final Configuration cfg,
			final AvatarService avatarService, final UserManager userManager, final GitService gitService) {
		this.permissionManager = permissionManager;
		this.cfg = cfg;
		this.avatarService = avatarService;
		this.userManager = userManager;
		this.gitService = gitService;
	}

	@Override
	public List<IssueAction> getActions(Issue issue, ApplicationUser remoteUser) {
		final List<IssueAction> list = new ArrayList<>();
		list.add(new ViewAction(descriptor, issue, remoteUser, avatarService, userManager, gitService));
		return list;
	}

	@Override
	public boolean showPanel(Issue issue, ApplicationUser remoteUser) {
		return !PluginProperties.getIssuePanelRequireRoleDev(cfg)
				|| this.permissionManager.hasPermission(ProjectPermissions.VIEW_DEV_TOOLS, issue, remoteUser);
	}

}
