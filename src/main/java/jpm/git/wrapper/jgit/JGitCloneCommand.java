package jpm.git.wrapper.jgit;

import java.io.File;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.wrapper.GitWrapperException;

/**
 * Clonar un repositorio
 * 
 * @author José
 *
 */
public class JGitCloneCommand extends JGitWrapper<Object> {

	final static Logger LOG = LoggerFactory.getLogger(JGitCloneCommand.class);

	public JGitCloneCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		super(gitPath, workingFolder, remoteRepoURL, authUser, authPassword,
				"JGit clone '" + remoteRepoURL + "' at '" + workingFolder.getAbsolutePath() + "'");
	}

	@Override
	public void execute() throws GitWrapperException {
		CredentialsProvider cp = null;
		if (authUser != null && authUser.length() > 0) {
			cp = new UsernamePasswordCredentialsProvider(authUser, authPassword == null ? "" : authPassword);
		}
		final CloneCommand cmd = Git.cloneRepository();
		final String repoDir = remoteRepoURL.substring(remoteRepoURL.lastIndexOf("/") + 1);
		cmd.setDirectory(new File(workingFolder.getAbsolutePath() + File.separator + repoDir)).setURI(remoteRepoURL)
				.setBare(true);
		if (cp != null) {
			cmd.setCredentialsProvider(cp);
		}
		cmd.setProgressMonitor(this);
		try {
			cmd.call().close();
			endExecution(0, null);
		} catch (GitAPIException e) {
			final String msg = "GitAPIException: " + e.getMessage();
			LOG.error(msg, e);
			endExecution(1, msg);
		}
	}

}
