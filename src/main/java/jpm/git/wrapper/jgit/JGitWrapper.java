package jpm.git.wrapper.jgit;

import java.io.File;

import org.eclipse.jgit.lib.ProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.wrapper.AbstractCommand;

/**
 * Clonar un repositorio
 * 
 * @author José
 *
 */
abstract class JGitWrapper<T> extends AbstractCommand<T> implements ProgressMonitor {

	final static Logger LOG = LoggerFactory.getLogger(JGitWrapper.class);

	public JGitWrapper(String gitPath, File workingFolder, String remoteRepoURL, String authUser, String authPassword,
			String description) {
		super(gitPath, workingFolder, remoteRepoURL, authUser, authPassword, description);
	}

	// ///////////////////////////////////
	// ProgressMonitor

	@Override
	public void start(int totalTasks) {
		output(totalTasks + " tasks to execute\n");
	}

	@Override
	public void beginTask(String title, int totalWork) {
		output(title);
	}

	@Override
	public void update(int completed) {
		output(".");
	}

	@Override
	public void endTask() {
		output(" [OK]\n");
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

}
