package jpm.git.wrapper.jgit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.util.io.DisabledOutputStream;

import jpm.git.bo.GitCommit;
import jpm.git.bo.GitCommitChangedFile;
import jpm.git.wrapper.GitWrapperException;

/**
 * Buscar entre todos los commits del repositorio un token.
 * 
 * @author José
 *
 */
public class JGitLogIssueCommand extends JGitWrapper<GitCommit[]> {

	private final static GitCommit[] EMPTY = new GitCommit[0];

	protected GitCommit[] result = EMPTY;
	protected final String issue;

	/**
	 * Buscar entre todos los commits del repositorio un token.
	 * 
	 * @param gitPath
	 * @param workingFolder
	 *            directorio de trabajo. OJO: NO es la carpeta en la que se ha
	 *            clonado el repositorio sino la que la contiene.
	 * @param issue
	 *            incidencia a buscar.
	 * @throws GitWrapperException
	 */
	public JGitLogIssueCommand(String gitPath, File workingFolder, String issue) {
		super(gitPath, workingFolder, null, null, null,
				"JGit log --issue=" + issue + " --workDir=" + workingFolder.getAbsolutePath());
		this.issue = issue;
	}

	@Override
	public GitCommit[] getResult() throws GitWrapperException {
		return result;
	}

	@Override
	public void execute() throws GitWrapperException {
		Git git = null;
		Repository repo = null;
		DiffFormatter diff = new DiffFormatter(DisabledOutputStream.INSTANCE);
		try {
			repo = new RepositoryBuilder().setGitDir(workingFolder).setMustExist(true).setBare().build();
		} catch (IOException e) {
			final String msg = "RepositoryBuilder: " + e.getMessage();
			LOG.error(msg, e);
			endExecution(1, msg);
			return;
		}
		try {
			git = new Git(repo);
			final Iterable<RevCommit> it = git.log().all().call();
			if (it != null) {
				// Lista
				final List<GitCommit> commitArray = new ArrayList<>();
				// Detector de cambios
				diff.setRepository(repo);
				diff.setDiffComparator(RawTextComparator.DEFAULT);
				diff.setDetectRenames(true);
				// Expresión regular para verificar si el commit guarda relación
				// con la incidencia
				final Pattern pattern = Pattern.compile("^(.*[^a-zA-Z])?" + issue + "([^\\d].*)?$", Pattern.DOTALL);
				it.forEach((RevCommit r) -> addIfMatches(diff, r, commitArray, pattern));
				result = commitArray.toArray(new GitCommit[commitArray.size()]);
			}
			endExecution(0, null);
		} catch (Exception e) {
			final String msg = e.getClass().getName() + ": " + e.getMessage();
			LOG.error(msg, e);
			endExecution(0, msg);
			throw new GitWrapperException(GitWrapperException.E_GENERIC, msg, e);
		} finally {
			diff.close();
			if (git != null) {
				git.close();
			}
			if (repo != null) {
				repo.close();
			}
		}
	}

	protected void addIfMatches(final DiffFormatter diff, final RevCommit revCommit, List<GitCommit> commitArray,
			final Pattern pattern) {
		final String fullMessage = revCommit.getFullMessage();
		if (pattern.matcher(fullMessage).matches()) {
			// Extract some data
			final PersonIdent author = revCommit.getAuthorIdent();
			final PersonIdent committer = revCommit.getCommitterIdent();
			final int idx = fullMessage.indexOf("\n\n");
			String body;
			String subject;
			if (idx == -1) {
				subject = fullMessage.trim();
				body = "";
			} else {
				subject = fullMessage.substring(0, idx).trim();
				body = fullMessage.substring(idx + 2).trim();
			}
			// Changeset
			final Map<String, GitCommitChangedFile> changeSet = new HashMap<>();
			final RevCommit[] parents = revCommit.getParents();
			if (parents==null || parents.length==0) {
				scanDiffEntries(diff, null, revCommit, changeSet);
			}
			else {
				for (RevCommit parent : parents) {
					scanDiffEntries(diff, parent, revCommit, changeSet);
				}
			}
			// Create GitCommit
			commitArray.add(new GitCommit(revCommit.getName(), author.getName(), author.getEmailAddress(),
					author.getWhen(), committer == null ? null : committer.getName(),
					committer == null ? null : committer.getEmailAddress(),
					committer == null ? null : committer.getWhen(), /* refNames */ "", subject, body, /* notes */"",
					changeSet.values().toArray(new GitCommitChangedFile[changeSet.values().size()])));
		}
		revCommit.disposeBody();
	}

	protected void scanDiffEntries(final DiffFormatter diff, RevCommit parent, RevCommit revCommit,
			final Map<String, GitCommitChangedFile> changeSet) {
		List<DiffEntry> diffs = null;
		try {
			diffs = diff.scan(parent == null ? null : parent.getTree(), revCommit.getTree());
		} catch (Exception e) {
			LOG.error(e.getClass().getName() + ": " + e.getMessage(), e);
		}
		if (diffs != null) {
			String type = null, name = null;
			for (DiffEntry diffEntry : diffs) {
				switch (diffEntry.getChangeType()) {
				case ADD:
				case COPY:
					type = "A";
					name = diffEntry.getNewPath();
					break;
				case DELETE:
					type = "D";
					name = diffEntry.getOldPath();
					break;
				case MODIFY:
					type = "M";
					name = diffEntry.getOldPath();
					break;
				case RENAME:
					type = "M";
					name = diffEntry.getOldPath();
					break;
				}
				changeSet.put(name, new GitCommitChangedFile(type, name));
			}
		}
	}

}
