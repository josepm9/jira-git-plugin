package jpm.git.wrapper.jgit;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.wrapper.GitWrapperException;

/**
 * Clonar un repositorio
 * 
 * @author José
 *
 */
public class JGitFetchCommand extends JGitWrapper<Object> {

	final static Logger LOG = LoggerFactory.getLogger(JGitFetchCommand.class);

	public JGitFetchCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		super(gitPath, workingFolder, remoteRepoURL, authUser, authPassword,
				"JGit fetch at '" + workingFolder.getAbsolutePath() + "'");
	}

	@Override
	public void execute() throws GitWrapperException {
		CredentialsProvider cp = null;
		if (authUser != null && authUser.length() > 0) {
			cp = new UsernamePasswordCredentialsProvider(authUser, authPassword == null ? "" : authPassword);
		}
		Repository repo;
		try {
			repo = new RepositoryBuilder().setGitDir(workingFolder).setMustExist(true).setBare().build();
		} catch (IOException e) {
			final String msg = "RepositoryBuilder: " + e.getMessage();
			LOG.error(msg, e);
			endExecution(1, msg);
			return;
		}
		final Git git = new Git(repo);
		try {
			final FetchCommand cmd = git.fetch();
			if (cp != null) {
				cmd.setCredentialsProvider(cp);
			}
			cmd.setProgressMonitor(this);
			cmd.call();
			endExecution(0, null);
		} catch (GitAPIException e) {
			final String msg = "GitAPIException: " + e.getMessage();
			LOG.error(msg, e);
			endExecution(1, msg);
			return;
		} finally {
			git.close();
		}
	}

}
