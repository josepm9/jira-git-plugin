package jpm.git.wrapper.git;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.wrapper.AbstractCommand;
import jpm.git.wrapper.GitWrapperException;

abstract class GitWrapper<T> extends AbstractCommand<T> {

	private final static Logger LOG = LoggerFactory.getLogger(GitWrapper.class);

	protected ProcessBuilder processBuilder;
	protected Process process;

	final String[] arguments;

	protected GitWrapper(File workingFolder, String[] arguments) {
		super(null, workingFolder, null, null, null, String.join(" ", arguments));
		this.arguments = arguments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.git.wrapper.Command#execute()
	 */
	@Override
	public void execute() throws GitWrapperException {
		processBuilder = new ProcessBuilder(arguments);
		if (workingFolder != null) {
			processBuilder.directory(workingFolder);
		}
		processBuilder.redirectErrorStream(true);
		try {
			process = processBuilder.start();
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw new GitWrapperException(GitWrapperException.E_GENERIC, e.getMessage(), e);
		}
		try {
			process.getOutputStream().close();
		} catch (IOException e1) {
		}

		while (true) {
			if (process.isAlive()) {
				readAvailable();
				try {
					Thread.sleep(50l);
				} catch (InterruptedException e) {
					LOG.error("Thread interrupted, destroying git process");
					process.destroy();
					end(-1);
					return;
				}
			} else {
				readAvailable();
				break;
			}
		}
		end(process.exitValue());
	}

	/**
	 * Utilidad para leer todos los datos disponibles en una tarea
	 * 
	 * @param asyncTask
	 */
	protected void readAvailable() {
		if (process == null || process.getInputStream() == null) {
			return;
		}
		// Error stream is redirected
		final InputStream is = process.getInputStream();
		int available;
		try {
			while ((available = is.available()) > 0) {
				final byte[] buffer = new byte[available];
				is.read(buffer, 0, available);
				output(new String(buffer, "UTF-8"));
			}
		} catch (IOException e) {
			LOG.error("IOException reading <is>: " + e.getMessage(), e);
		}
	}

	/**
	 * Sobreescribir este método si es necesario tomar alguna medida después de
	 * finalizado el comando
	 */
	protected void onEnd() {
	}

	@Override
	public T getResult() throws GitWrapperException {
		return null;
	}
}
