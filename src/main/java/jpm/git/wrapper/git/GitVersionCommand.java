package jpm.git.wrapper.git;

/**
 * Ejecutar el comando versión
 * @author José
 *
 */
public class GitVersionCommand extends GitWrapper<String> {

	public GitVersionCommand(String gitPath) {
		super(null, new String[] { gitPath, "--no-pager", "--version" });
	}

	@Override
	public String getResult() {
		return getFullOutput();
	}
}
