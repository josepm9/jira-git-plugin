package jpm.git.wrapper.git;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;

import jpm.git.bo.GitCommit;
import jpm.git.bo.GitCommitChangedFile;
import jpm.git.wrapper.GitWrapperException;

class GitWrapperCommitParser {

	private static final String SEP = "<?$%&!>";
	private static final int SEPLEN = SEP.length();
	/**
	 * Formato utilizado para mostrar un commit. Se utiliza, por ejemplo, en
	 * {@link GitWrapper#searchCommits(java.io.File, String, String)}.<br>
	 * Campos:
	 * <ul>
	 * <li>%H: Hash del commit</li>
	 * <li>%aN: nombre del autor del commit</li>
	 * <li>%aE: email del autor del commit</li>
	 * <li>%at: fecha de autor�a, formato UNIX_TIMESTAMP</li>
	 * <li>%cN: committer</li>
	 * <li>%cE: committer email</li>
	 * <li>%ct: committer date, formato UNIX_TIMESTAMP</li>
	 * <li>%d: ref names</li>
	 * <li>%s: t�tulo del commit (primera l�nea del comentario del commit)</li>
	 * <li>%b: cuerpo del commit (el resto del comentario del commit)</li>
	 * <li>%N: notas del commit</li>
	 * <li>Lista de modificaciones sencilla (argumento --name-status). Cada
	 * l�nea tiene el formato "(A|M|D)\b+(.*)" donde .* es el nombre del fichero
	 * afectado</li>
	 * </ul>
	 * 
	 * @see <a href="https://git-scm.com/docs/git-log">https://git-scm.com/docs/
	 *      git-log</a>
	 */
	public static final String FORMAT_PARAM = "--format=\"" + SEP + "%H" + SEP + "%aN" + SEP + "%aE" + SEP + "%at" + SEP
			+ "%cN" + SEP + "%cE" + SEP + "%ct" + SEP + "%d" + SEP + "%s" + SEP + "%b" + SEP + "%N" + SEP + "\"";
	/**
	 * Argumento que especifica c�mo mostrar las diferencias. Es requerido para
	 * que el formato (argumento anterior) sea correcto (@see
	 * {@link #FORMAT_PARAM})
	 */
	public static final String MODIFICATIONS_PARAM = "--name-status";

	/**
	 * Interpretar una cadena correspondiente a un conjunto de commits
	 * 
	 * @param s
	 * @return
	 * @throws GitWrapperException
	 *             Error de formato
	 */
	public static GitCommit[] parseGitCommits(String s) throws GitWrapperException {
		final ArrayList<GitCommit> commits = new ArrayList<GitCommit>();
		int i = 0, j;
		while ((i = s.indexOf(SEP, i)) > -1) {
			// Nuevo commit
			final GitCommit c = new GitCommit();
			i += SEPLEN;
			// hash
			j = next(s, i, true, "hash");
			c.setHash(s.substring(i, j));
			i = j + SEPLEN;
			// author
			j = next(s, i, true, "author");
			c.setAuthor(s.substring(i, j));
			i = j + SEPLEN;
			// public String authorEmail;
			j = next(s, i, true, "authorEmail");
			c.setAuthorEmail(s.substring(i, j));
			i = j + SEPLEN;
			// public Date authorDate;
			j = next(s, i, true, "authorDate");
			c.setAuthorDate(unixTS2Date(s.substring(i, j), "authorDate"));
			i = j + SEPLEN;
			// public String committer;
			j = next(s, i, true, "committer");
			c.setCommitter(s.substring(i, j));
			i = j + SEPLEN;
			// public String committerEmail;
			j = next(s, i, true, "committerEmail");
			c.setCommitterEmail(s.substring(i, j));
			i = j + SEPLEN;
			// public Date committerDate;
			j = next(s, i, true, "committerDate");
			c.setCommitterDate(unixTS2Date(s.substring(i, j), "committerDate"));
			i = j + SEPLEN;
			// public String refNames;
			j = next(s, i, true, "refNames");
			c.setRefNames(s.substring(i, j));
			i = j + SEPLEN;
			// public String subject;
			j = next(s, i, true, "subject");
			c.setSubject(s.substring(i, j));
			i = j + SEPLEN;
			// public String body;
			j = next(s, i, true, "body");
			c.setBody(s.substring(i, j));
			i = j + SEPLEN;
			// public String notes;
			j = next(s, i, true, "notes");
			c.setNotes(s.substring(i, j));
			i = j + SEPLEN;
			// public GitCommitChangedFile[] changedFiles;
			j = next(s, i, false, "changedFiles");
			c.setChangedFiles(parseChangedFiles(s.substring(i, j)));
			// Añadir y preparar siguiente iteración
			commits.add(c);
			i = j;
		}
		return commits.toArray(new GitCommit[commits.size()]);
	}

	/**
	 * Busca el siguiente delimitador
	 * 
	 * @param s
	 * @param i
	 * @param mandatory
	 *            en caso de ser cierto, si no se encuentra el delimitador se
	 *            genera una excepci�n. En otro caso, se devuelve el tama�o de
	 *            la cadena.
	 * @return
	 */
	private static int next(String s, int i, boolean mandatory, String token) throws GitWrapperException {
		int j = s.indexOf(SEP, i);
		if (j == -1) {
			if (mandatory) {
				throw new GitWrapperException(GitWrapperException.E_COMMIT_FORMAT, "Se esperaba el fin del " + token,
						null);

			} else {
				return s.length();
			}
		} else {
			return j;
		}
	}

	/**
	 * Convierte un sello de tiempo en formato UNIX a java.util.Date.
	 * 
	 * @param s
	 *            sello de tiempo en formato UNIX
	 * @return java.util.Date si s no es vac�o, null si s es vac�o
	 * @throws GitWrapperException
	 *             si s no se corresponde con un sello de tiempo formato UNIX
	 *             v�lido
	 */
	private static Date unixTS2Date(String s, String token) throws GitWrapperException {
		try {
			return s.length() == 0 ? null : new Date(Long.parseLong(s, 10) * 1000);
		} catch (NumberFormatException e) {
			throw new GitWrapperException(GitWrapperException.E_COMMIT_FORMAT,
					"Sello de tiempo (" + token + ") formato UNIX no v�lido: " + s, e);
		}
	}

	/**
	 * Interpreta la cadena s como una lista de modificaciones de un commit
	 * 
	 * @param s
	 * @return Una vector de ficheros modificados
	 * @throws GitWrapperException
	 */
	private static GitCommitChangedFile[] parseChangedFiles(String s) throws GitWrapperException {
		final ArrayList<GitCommitChangedFile> changesFiles = new ArrayList<GitCommitChangedFile>();
		final BufferedReader reader = new BufferedReader(new StringReader(s));
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.length() > 0) {
					final GitCommitChangedFile c = new GitCommitChangedFile();
					c.setType(line.substring(0, 1));
					c.setFilePath(line.substring(1).trim());
					// TODO: validar c
					changesFiles.add(c);
				}
			}
		} catch (IOException e) {
			throw new GitWrapperException(GitWrapperException.E_COMMIT_FORMAT,
					"Error entrada/salida inesperado: " + e.getMessage(), e);
		}
		return changesFiles.toArray(new GitCommitChangedFile[changesFiles.size()]);
	}
}
