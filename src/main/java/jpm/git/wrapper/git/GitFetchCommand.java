package jpm.git.wrapper.git;

import java.io.File;

/**
 * Sincronizar un repositorio. git pull -ff-only
 * 
 * @author José
 *
 */
public class GitFetchCommand extends GitWrapper<Object> {

	public GitFetchCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		super(workingFolder, new String[] { gitPath, "--no-pager", "fetch", "-p", "-t",
				GitCloneCommand.getRepoURL(remoteRepoURL, authUser, authPassword) });
	}

}
