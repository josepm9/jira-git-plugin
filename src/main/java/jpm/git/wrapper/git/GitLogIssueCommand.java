package jpm.git.wrapper.git;

import java.io.File;

import jpm.git.bo.GitCommit;
import jpm.git.wrapper.GitWrapperException;

/**
 * Buscar entre todos los commits del repositorio un token.
 * 
 * @author José
 *
 */
public class GitLogIssueCommand extends GitWrapper<GitCommit[]> {

	protected GitCommit[] result = null;

	/**
	 * Buscar entre todos los commits del repositorio un token.
	 * 
	 * @param gitPath
	 * @param workingFolder
	 *            directorio de trabajo. OJO: NO es la carpeta en la que se ha
	 *            clonado el repositorio sino la que la contiene.
	 * @param issue
	 *            incidencia a buscar.
	 * @throws GitWrapperException
	 */
	public GitLogIssueCommand(String gitPath, File workingFolder, String issue) {
		super(workingFolder,
				new String[] { gitPath, "--no-pager", "log", "--all",
						"\"--grep=(^|.*[^a-zA-Z])" + issue + "([^\\d.*]|$)\"", "--extended-regexp",
						GitWrapperCommitParser.FORMAT_PARAM, GitWrapperCommitParser.MODIFICATIONS_PARAM });
	}

	@Override
	protected void onEnd() {
	}

	@Override
	public GitCommit[] getResult() throws GitWrapperException {
		final String s = getFullOutput();
		if (result == null) {
			if (s.length() > 0) {
				result = GitWrapperCommitParser.parseGitCommits(s);
			}
			if (result == null) {
				result = new GitCommit[0];
			}
		}
		return result;
	}
}
