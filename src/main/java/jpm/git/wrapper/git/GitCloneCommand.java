package jpm.git.wrapper.git;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Clonar un repositorio
 * 
 * @author José
 *
 */
public class GitCloneCommand extends GitWrapper<Object> {

	public GitCloneCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		super(workingFolder, new String[] { gitPath, "--no-pager", "clone",
				getRepoURL(remoteRepoURL, authUser, authPassword), "--bare", "--config", "core.askPass", "--verbose" });
	}

	protected static String getRepoURL(String remoteRepoURL, String authUser, String authPassword) {
		if (authUser != null && authUser.length() > 0) {
			final String s = remoteRepoURL.toLowerCase();
			try {
				if (s.startsWith("http://")) {
					return remoteRepoURL.substring(0, 7) + URLEncoder.encode(authUser, "UTF-8")
							+ (authPassword == null ? "" : (":" + URLEncoder.encode(authPassword, "UTF-8"))) + "@"
							+ remoteRepoURL.substring(7);
				} else if (s.startsWith("https://")) {
					return remoteRepoURL.substring(0, 8) + URLEncoder.encode(authUser, "UTF-8")
							+ (authPassword == null ? "" : (":" + URLEncoder.encode(authPassword, "UTF-8"))) + "@"
							+ remoteRepoURL.substring(8);
				}
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException("UnsupportedEncodingException: " + e.getMessage());
			}
		}
		return remoteRepoURL;
	}

}
