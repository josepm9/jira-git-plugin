package jpm.git.wrapper;

import java.io.File;

import jpm.git.bo.GitCommit;
import jpm.git.wrapper.git.GitCloneCommand;
import jpm.git.wrapper.git.GitFetchCommand;
import jpm.git.wrapper.git.GitLogIssueCommand;
import jpm.git.wrapper.git.GitVersionCommand;

public class GitCommandFactory extends CommandFactory {

	@Override
	public Command<Object> createGitCloneCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		return new GitCloneCommand(gitPath, workingFolder, remoteRepoURL, authUser, authPassword);
	}

	@Override
	public Command<Object> createGitFetchCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		return new GitFetchCommand(gitPath, workingFolder, remoteRepoURL, authUser, authPassword);
	}

	@Override
	public Command<GitCommit[]> createGitLogIssueCommand(String gitPath, File workingFolder, String issue) {
		return new GitLogIssueCommand(gitPath, workingFolder, issue);
	}

	@Override
	public Command<String> createGitVersionCommand(String gitPath) {
		return new GitVersionCommand(gitPath);
	}

}
