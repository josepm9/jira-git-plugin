package jpm.git.wrapper;

import java.io.File;

import jpm.git.bo.GitCommit;

public abstract class CommandFactory {
	
	private static CommandFactory INSTANCE;

	public static CommandFactory getInstance() {
		if (INSTANCE==null) {
			INSTANCE = new JGitCommandFactory();
		}
		return INSTANCE;
	}
	
	public static void setInstance(CommandFactory instance) {
		INSTANCE = instance;
	}
	
	public abstract Command<Object> createGitCloneCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword);

	public abstract Command<Object> createGitFetchCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword);

	public abstract Command<GitCommit[]> createGitLogIssueCommand(String gitPath, File workingFolder, String issue);

	public abstract Command<String> createGitVersionCommand(String gitPath);
	

}
