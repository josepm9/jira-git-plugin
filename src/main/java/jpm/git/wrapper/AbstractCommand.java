package jpm.git.wrapper;

import java.io.File;

import org.eclipse.jgit.lib.ProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clonar un repositorio
 * 
 * @author José
 *
 */
public abstract class AbstractCommand<T> implements Command<T>, ProgressMonitor {

	final static Logger LOG = LoggerFactory.getLogger(AbstractCommand.class);

	protected final File workingFolder;
	protected final String remoteRepoURL;
	protected final String authUser;
	protected final String authPassword;
	protected final String description;

	private Integer exitStatus;
	private CommandListener listener;
	private StringBuilder outputBuilder = new StringBuilder();
	private String output;

	public AbstractCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword, final String description) {
		this.workingFolder = workingFolder;
		this.remoteRepoURL = remoteRepoURL;
		this.authUser = authUser;
		this.authPassword = authPassword;
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.git.wrapper.Command#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Integer getExitValue() {
		return exitStatus;
	}
	
	public File getWorkingFolder() {
		return workingFolder;
	}

	@Override
	public Command<T> listener(CommandListener listener) {
		this.listener = listener;
		return this;
	}

	protected void endExecution(int exitStatus, String msg) {
		if (msg != null) {
			output(msg);
		}
		end(exitStatus);
	}

	protected void output(String msg) {
		outputBuilder.append(msg);
		if (listener != null) {
			try {
				listener.onOutput(msg);
			} catch (Exception e) {
			}
		}
	}

	protected void end(int exitStatus) {
		this.exitStatus = exitStatus;
	}

	@Override
	public T getResult() throws GitWrapperException {
		return null;
	}

	@Override
	public String getFullOutput() {
		if (exitStatus == null) {
			LOG.error("Command already running");
			throw new IllegalStateException("Command already running");
		} else if (exitStatus != 0) {
			LOG.error("Command failed");
			throw new IllegalStateException("Command failed");
		}
		if (output == null) {
			output = outputBuilder.toString();
		}
		return output;
	}

	// ///////////////////////////////////
	// ProgressMonitor

	@Override
	public void start(int totalTasks) {
		if (listener != null) {
			try {
				listener.onOutput(totalTasks + " tasks to execute\n");
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void beginTask(String title, int totalWork) {
		if (listener != null) {
			try {
				listener.onOutput(title);
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void update(int completed) {
		if (listener != null) {
			try {
				listener.onOutput(".");
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void endTask() {
		if (listener != null) {
			try {
				listener.onOutput(" [OK]\n");
			} catch (Exception e) {
			}
		}
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

}
