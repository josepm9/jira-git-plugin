package jpm.git.wrapper;

/**
 * Interface for any git command. The execution of the command will be always
 * synchronous no matter the underline implementation
 * 
 * @author José
 *
 */
public interface Command<T> {
	
	/**
	 * Command exit value. <br/>
	 * <ul>
	 * <li>0 ==> success</li>
	 * <li>!=0 ==> failure</li>
	 * <li>null ==> still running</li>
	 * </ul>
	 * @return
	 */
	public Integer getExitValue();
	
	/**
	 * A string with a description of the command and arguments
	 * @return
	 */
	public String getDescription();
	
	/**
	 * Establecer un escuchador para los eventos del comando
	 * @param listener
	 * @return el comando actualizado con el escuchador
	 */
	public Command<T> listener(CommandListener listener);
	
	/**
	 * Ejecutar el comando
	 * @throws GitWrapperException
	 */
	public void execute() throws GitWrapperException;
	
	/**
	 * Obtener el resultado de la ejecución del comando.
	 */
	public T getResult() throws GitWrapperException;
	
	/**
	 * Obtener la salida completa de un comando (después de su finalización)
	 */
	public String getFullOutput();

}