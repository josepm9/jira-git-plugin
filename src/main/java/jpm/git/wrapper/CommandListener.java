package jpm.git.wrapper;

public interface CommandListener {

	public void onOutput(String output);

}
