package jpm.git.wrapper;

import java.io.File;

import jpm.git.bo.GitCommit;
import jpm.git.wrapper.git.GitVersionCommand;
import jpm.git.wrapper.jgit.JGitCloneCommand;
import jpm.git.wrapper.jgit.JGitFetchCommand;
import jpm.git.wrapper.jgit.JGitLogIssueCommand;

public class JGitCommandFactory extends CommandFactory {

	@Override
	public Command<Object> createGitCloneCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		return new JGitCloneCommand(gitPath, workingFolder, remoteRepoURL, authUser, authPassword);
	}

	@Override
	public Command<Object> createGitFetchCommand(String gitPath, File workingFolder, String remoteRepoURL, String authUser,
			String authPassword) {
		return new JGitFetchCommand(gitPath, workingFolder, remoteRepoURL, authUser, authPassword);
	}

	@Override
	public Command<GitCommit[]> createGitLogIssueCommand(String gitPath, File workingFolder, String issue) {
		return new JGitLogIssueCommand(gitPath, workingFolder, issue);
	}

	@Override
	public Command<String> createGitVersionCommand(String gitPath) {
		return new GitVersionCommand(gitPath);
	}

}
