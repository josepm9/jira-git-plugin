package jpm.git.wrapper;

public class GitWrapperException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 357428736610216787L;

	public static final String E_GENERIC = "GW-999999";
	public static final String E_COMMIT_FORMAT = "GW-000100";
	
	public GitWrapperException(String code, String message, Throwable cause) {
		super(message, cause);
	}

}
