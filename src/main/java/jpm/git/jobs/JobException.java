package jpm.git.jobs;

public class JobException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1079898491039695956L;

	public JobException(String message, Throwable cause) {
		super(message, cause);
	}

}
