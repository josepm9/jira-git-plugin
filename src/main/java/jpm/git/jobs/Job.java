package jpm.git.jobs;

public interface Job {
	
	public void doJob() throws JobException;
	
}
