package jpm.git.jobs;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.GitRepository;
import jpm.git.persistence.DaoException;
import jpm.git.persistence.GitRepositoryDAO;
import jpm.git.tasks.TaskManager;

public class GitFetchJobImpl implements GitFetchJob {

	final static Logger LOG = LoggerFactory.getLogger(GitFetchJobImpl.class);

	final GitRepositoryDAO gitRepositoryDAO;
	final TaskManager taskManager;

	public GitFetchJobImpl(final GitRepositoryDAO gitRepositoryDAO, final TaskManager taskManager) {
		this.gitRepositoryDAO = gitRepositoryDAO;
		this.taskManager = taskManager;
	}

	@Override
	public void doJob() throws JobException {
		GitRepository[] gitRepositories = null;
		try {
			gitRepositories = gitRepositoryDAO.getRepositories();
		} catch (DaoException e) {
			final String msg = "Could not get repository list (DaoException): " + e.getMessage();
			LOG.error(msg, e);
			throw new JobException(msg, e);
		}
		if (gitRepositories != null) {
			for (GitRepository repo : gitRepositories) {
				taskManager
						.start(taskManager.createGitFetchTask(repo.getProjectKey() + File.separator + repo.getRepoKey(),
								repo.getUrl(), repo.getAuthUser(), repo.getAuthPassword()));
			}
		}
	}

}
