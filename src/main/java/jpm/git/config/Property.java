package jpm.git.config;

public interface Property {
	
	public String getKey();
	public void setKey(String key);

	public String getValue();
	public void setValue(String value);

}
