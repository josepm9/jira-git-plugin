package jpm.git.config;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.persistence.PropertyDao;

/**
 * Componente que contiene la configuración del plugin
 * 
 * @author j.penalba
 *
 */
public class Configuration {

	final static Logger LOG = LoggerFactory.getLogger(Configuration.class);
	
	public static final String P_GIT_LOCATION = "_/git.location";

	/**
	 * Asistente para el acceso a las propiedades en BBDD
	 */
	final PropertyDao<?> propertyDAO;

	/**
	 * Carpeta de trabajo
	 */
	protected File workDir;

	/**
	 * Constructor de estado
	 * 
	 * @param propertyDAO
	 */
	public Configuration(final PropertyDao<?> propertyDAO) {
		this.propertyDAO = propertyDAO;
		this.workDir = null;
	}

	/**
	 * {@link #getGitExecutable}
	 * 
	 * @return
	 */
	public String getGitExecutable() {
		return propertyDAO.readProperty(P_GIT_LOCATION, "git").getValue();
	}

	/**
	 * {@link #setGitExecutable}
	 */
	public void setGitExecutable(String gitExecutable) {
		propertyDAO.upsertProperty(P_GIT_LOCATION, gitExecutable);
	}

	/**
	 * {@link #workDir}
	 */
	public File getWorkDir() {
		return workDir;
	}
	
	/**
	 * {@link #workDir}
	 */
	public void setWorkDir(File workDir) {
		this.workDir = workDir;
	}
	
	/**
	 * Recuperar el valor de una propiedad por su clave
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public String getPropertyValue(final String key, final String defaultValue) {
		return propertyDAO.readProperty(key, defaultValue).getValue();
	}

}
