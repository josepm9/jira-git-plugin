package jpm.git.tasks;

/**
 * Escuchador de eventos de una tarea asíncrona
 * @author j.penalba
 *
 */
public interface TaskListener {
	
	/**
	 * Evento generado cuando se detecta que ha finalizado la tarea
	 * @param task
	 */
	public void onEnd(final Task<?> task);

}
