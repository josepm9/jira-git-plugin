package jpm.git.tasks.impl;

import java.io.File;

import jpm.git.bo.GitCommit;
import jpm.git.tasks.Task;
import jpm.git.tasks.TaskListener;
import jpm.git.tasks.TaskManager;
import jpm.git.wrapper.CommandFactory;

public class GitLogIssueTask extends Task<GitCommit[]> {

	@Override
	public AsyncTaskType getTaskType() {
		return AsyncTaskType.GitLogIssue;
	}

	public GitLogIssueTask(String gitPath, File workingFolder, String issue, TaskManager taskManager,
			TaskListener taskListener) {
		super(CommandFactory.getInstance().createGitLogIssueCommand(gitPath, workingFolder, issue), taskManager,
				taskListener);
	}

}
