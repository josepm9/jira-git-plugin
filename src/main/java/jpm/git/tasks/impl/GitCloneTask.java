package jpm.git.tasks.impl;

import java.io.File;

import jpm.git.tasks.Task;
import jpm.git.tasks.TaskListener;
import jpm.git.tasks.TaskManager;
import jpm.git.wrapper.CommandFactory;

public class GitCloneTask extends Task<Object> {

	@Override
	public AsyncTaskType getTaskType() {
		return AsyncTaskType.GitClone;
	}

	public GitCloneTask(String gitPath, File workingFolder, String remoteRepoURL, String authUser, String authPassword,
			TaskManager taskManager, TaskListener taskListener) {
		super(CommandFactory.getInstance().createGitCloneCommand(gitPath, workingFolder, remoteRepoURL, authUser,
				authPassword), taskManager, taskListener);
	}

}
