package jpm.git.tasks.impl;

import java.io.File;

import jpm.git.tasks.Task;
import jpm.git.tasks.TaskListener;
import jpm.git.tasks.TaskManager;
import jpm.git.wrapper.CommandFactory;

public class GitFetchTask extends Task<Object> {

	@Override
	public AsyncTaskType getTaskType() {
		return AsyncTaskType.GitFetch;
	}

	public GitFetchTask(String gitPath, File workingFolder, String gitRepoURL, String authUser, String authPassword, TaskManager taskManager, TaskListener taskListener) {
		super(CommandFactory.getInstance().createGitFetchCommand(gitPath, workingFolder, gitRepoURL, authUser, authPassword), taskManager, taskListener);
	}

}
