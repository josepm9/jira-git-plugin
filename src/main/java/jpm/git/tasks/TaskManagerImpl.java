package jpm.git.tasks;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.config.Configuration;
import jpm.git.tasks.impl.GitCloneTask;
import jpm.git.tasks.impl.GitFetchTask;
import jpm.git.tasks.impl.GitLogIssueTask;

public class TaskManagerImpl implements TaskManager, TaskListener {

	final static Logger LOG = LoggerFactory.getLogger(TaskManagerImpl.class);

	/**
	 * Número máximo admitido de tareas concurrentes
	 */
	public final static int MAX_TASKS = 5;

	/**
	 * Gestor en estado iniciado
	 */
	private static final int ST_INIT = 0;

	/**
	 * Gestor en estado parada
	 */
	private static final int ST_STOP = 1;

	/**
	 * Estado en que se encuentra el Gestor.
	 */
	protected int status = ST_INIT;

	/**
	 * Registro de tareas conocidas
	 */
	final protected Map<String, Task<?>> allTasks = new HashMap<>();

	/**
	 * Conjunto de hilos que gestionarán la ejecución de tareas
	 */
	final protected ThreadPoolExecutor exeSvc = new ThreadPoolExecutor(MAX_TASKS, MAX_TASKS, 0L, TimeUnit.MILLISECONDS,
			new LinkedBlockingQueue<Runnable>());

	/**
	 * Identificador de la siguiente tarea
	 */
	protected long nextTaskId = 1l;

	/**
	 * Escuchadores de eventos de las tareas administradas
	 */
	protected List<TaskListener> taskListeners = new ArrayList<>();

	/**
	 * Configuración
	 */
	protected final Configuration config;

	/**
	 * Constructor
	 */
	public TaskManagerImpl(final Configuration config) {
		this.config = config;
	}

	// ///////////////////////////////
	// Accessors

	public Map<String, Task<?>> getTasks() {
		return allTasks;
	}

	@Override
	public int getRunningTasksCount() {
		return exeSvc.getActiveCount();
	}

	@Override
	public int getQueuedTasksCount() {
		return exeSvc.getQueue().size();
	}

	// ///////////////////////////////
	// Methods

	/**
	 * Registrar un nuevo escuchador de eventos de tareas administrativas
	 * 
	 * @param l
	 */
	public void registerTaskListener(final TaskListener l) {
		synchronized (taskListeners) {
			taskListeners.add(l);
		}
	}

	/**
	 * Eliminar un nuevo escuchador de eventos de tareas administrativas
	 * 
	 * @param l
	 */
	public void unregisterTaskListener(final TaskListener l) {
		synchronized (taskListeners) {
			taskListeners.remove(l);
		}
	}

	// see TaskManager#start(Task)
	public String start(Task<?> task) {
		if (status == ST_STOP) {
			throw new RuntimeException("TaskManagerImpl on shutdown");
		}
		synchronized (allTasks) {
			task.id = Long.toString(nextTaskId++, 10);
			allTasks.put(task.id, task);
		}
		LOG.debug("Queue task " + task.id);
		task.state = Task.ST_QUEUED;
		task.queueDate = System.currentTimeMillis();
		exeSvc.execute(task);
		return task.id;
	};

	// see TaskManager#run(Task)
	public void syncRun(Task<?> task) {
		LOG.debug("Run task " + task.id + " synchronously");
		if (status == ST_STOP) {
			throw new RuntimeException("TaskManagerImpl on shutdown");
		}
		task.run();
	};

	// see TaskManager#getAsyncTaskStatus(String)
	public TaskStatus getTaskStatus(String key) {
		final Task<?> t = allTasks.get(key);
		if (t == null) {
			return null;
		} else {
			final TaskStatus status = new TaskStatus();
			status.state = t.state;
			status.newOutput = t.purgeCommandOutput();
			return status;
		}
	}

	/**
	 * Detener el administrador de tareas
	 */
	@Override
	public void shutdown(final boolean now, final long millis) {
		status = ST_STOP;
		if (now) {
			exeSvc.shutdownNow();
		} else {
			exeSvc.shutdown();
		}
		try {
			if (!exeSvc.awaitTermination(millis, TimeUnit.MILLISECONDS)) {
				LOG.error("JGP-TaskManager: Not all tasks where terminated!");
			}
		} catch (InterruptedException e) {
			LOG.error("Interrupted while waiting for termination: " + e.getMessage());
		}
		synchronized (taskListeners) {
			taskListeners.clear();
		}
		synchronized (allTasks) {
			allTasks.clear();
		}
	}

	/**
	 * Si la historia se llena de tareas (+ de 20) se limpia eliminando las
	 * tareas terminadas y viejas (+ de 1 minutos)
	 */
	protected void cleanupTaskHistory() {
		if (allTasks.size() > 20) {
			final List<String> toDelete = new ArrayList<>();
			for (Task<?> task : allTasks.values()) {
				if (task.endDate != 0 && (System.currentTimeMillis() - task.endDate) > 60000l) {
					toDelete.add(task.id);
				}
			}
			for (String id : toDelete) {
				synchronized (allTasks) {
					allTasks.remove(id);
				}
			}
		}

	}

	// ////////////////////////////////////////
	// Construcción de tareas

	/**
	 * @see {@link TaskManager#createGitCloneTask(String, String)}
	 */
	public GitCloneTask createGitCloneTask(String rootDir, String remoteRepoURL, String authUser, String authPassword) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("createGitCloneTask '" + remoteRepoURL + "' at '" + rootDir + "'");
		}
		return new GitCloneTask(config.getGitExecutable(), getProjectWorkDir(rootDir), remoteRepoURL, authUser,
				authPassword, this, this);
	}

	/**
	 * @see {@link TaskManager#createGitFetchTask(String)}
	 */
	public GitFetchTask createGitFetchTask(String relativeDir, String gitRepoURL, String authUser,
			String authPassword) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("createGitFetchTask at '" + relativeDir + "'");
		}
		return new GitFetchTask(config.getGitExecutable(), getProjectWorkDir(relativeDir), gitRepoURL, authUser,
				authPassword, this, this);
	}

	/**
	 * @see {@link TaskManager#createGitLogIssueTask(String,String)}
	 */
	public GitLogIssueTask createGitLogIssueTask(String relativeDir, String issue) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("createGitLogIssueTask '" + issue + "' at '" + relativeDir + "'");
		}
		return new GitLogIssueTask(config.getGitExecutable(), getProjectWorkDir(relativeDir), issue, this, this);
	}

	//
	/**
	 * @see {@link TaskManager#getProjectWorkDir(String)}
	 */
	public File getProjectWorkDir(String projectKey) {
		return new File(config.getWorkDir().getAbsolutePath() + File.separator + projectKey);
	}

	// ////////////////////////////////////////
	// TaskListener implementation

	// see TaskListener#onEnd(Task)
	public void onEnd(final Task<?> task) {
		LOG.debug("Task " + task.id + " ended");
		for (TaskListener l : taskListeners) {
			try {
				l.onEnd(task);
			} catch (Exception e) {
				LOG.error("Error durante la ejecución de escuchador de las tarea finalizada: " + e.getMessage(), e);
			}
		}
		cleanupTaskHistory();
	}

}
