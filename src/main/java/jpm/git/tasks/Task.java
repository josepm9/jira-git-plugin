package jpm.git.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.wrapper.Command;
import jpm.git.wrapper.CommandListener;
import jpm.git.wrapper.GitWrapperException;

/**
 * Clase que encapsula la llamada de git para poder mantener un registro y
 * permitir el acceso a los resultados de la ejecución
 * 
 * @author José
 *
 */
public abstract class Task<T> implements Runnable, CommandListener {

	final static Logger LOG = LoggerFactory.getLogger(Task.class);

	/**
	 * Tipos de Tarea
	 */
	public enum AsyncTaskType {
		Generic, GitClone, GitFetch, GitLogIssue
	}

	/**
	 * Ejecutable git encapsulado. ver {@link #runTask()}
	 */
	protected final Command<T> gitWrapper;
	/**
	 * Gestor de tareas
	 */
	protected final TaskManager taskManager;
	/**
	 * Instancia que escucha eventos de ejecución de tarea
	 */
	protected final TaskListener taskListener;

	/**
	 * Estado tarea iniciada, sin programar su ejecución
	 */
	public final static int ST_INITIALIZED = 0;
	/**
	 * Tarea iniciada, en cola para ser ejecutada
	 */
	public final static int ST_QUEUED = 1;
	/**
	 * El arranque de la tarea falló
	 */
	public final static int ST_STARTUP_FAILED = 2;
	/**
	 * Tarea en ejecución
	 */
	public final static int ST_RUNNING = 3;
	/**
	 * Tarea finalizada con resultado satisfactorio
	 */
	public final static int ST_ENDED_OK = 4;
	/**
	 * Tarea finalizada con errores
	 */
	public final static int ST_ENDED_KO = 5;

	/**
	 * Estado de ejecución de la tarea. Nivel de acceso paquete: lo modifica el
	 * gesor de tareas
	 */
	int state = ST_INITIALIZED;

	/**
	 * Salida del comando. Sólo se escribe desde el gestor, la lectura implica
	 * la limpieza del buffer.
	 */
	private StringBuilder commandOutput = new StringBuilder();

	/**
	 * Fecha de encolado de la tarea
	 */
	long queueDate;
	/**
	 * Fecha de arranque de la tarea
	 */
	long startupDate;
	/**
	 * Fecha de fin de la tarea
	 */
	long endDate;

	/**
	 * Identificador de la tarea, utilizado en el gestor
	 */
	String id;

	/**
	 * Hilo que se encarga la ejecución de la tarea
	 */
	Thread executingThread;

	/**
	 * Objeto de datos. No se utiliza internamente, es una herramienta que se
	 * aporta para utilizar externamente. (Es útil para hacer correspondencia en
	 * la ejecución "asíncrona" de los eventos)
	 */
	protected Object data;

	/**
	 * Constructor
	 * 
	 * @param gitWrapper
	 * @param taskManager
	 */
	public Task(final Command<T> gitWrapper, final TaskManager taskManager, final TaskListener taskListener) {
		this.gitWrapper = gitWrapper;
		this.taskManager = taskManager;
		this.taskListener = taskListener;
		gitWrapper.listener(this);
	}

	/**
	 * Recuperar el tipo de tarea
	 */
	public abstract AsyncTaskType getTaskType();

	/**
	 * {@link #state}
	 * 
	 * @return
	 */
	public int getStatus() {
		return state;
	}

	/**
	 * {@link #state}
	 * 
	 * @param status
	 */
	public void setStatus(int status) {
		this.state = status;
	}

	/**
	 * {@link #commandOutput}
	 * 
	 * @return
	 */
	public String purgeCommandOutput() {
		String s;
		synchronized (commandOutput) {
			s = commandOutput.toString();
			commandOutput.setLength(0);
		}
		return s;
	}

	/**
	 * {@link #queueDate}
	 * 
	 * @return
	 */
	public long getQueueDate() {
		return queueDate;
	}

	/**
	 * {@link #startupDate}
	 * 
	 * @return
	 */
	public long getStartupDate() {
		return startupDate;
	}

	/**
	 * {@link #endDate}
	 * 
	 * @return
	 */
	public long getEndDate() {
		return endDate;
	}

	/**
	 * {@link #data}
	 * 
	 * @return
	 */
	public Object getData() {
		return data;
	}

	/**
	 * {@link #data}
	 * 
	 * @param data
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @see {@link Runnable#run()}
	 */
	@Override
	final public void run() {
		startupDate = System.currentTimeMillis();
		state = Task.ST_RUNNING;
		executingThread = Thread.currentThread();
		try {
			gitWrapper.execute();
		} catch (GitWrapperException e) {
			final String msg = e.getMessage();
			LOG.error(msg, e);
			onOutput(msg);
			state = Task.ST_STARTUP_FAILED;
		} catch (Exception e) {
			final String msg = "Unhanled exception (" + e.getClass().getName() + "): " + e.getMessage();
			LOG.error(msg, e);
			onOutput(msg);
			state = Task.ST_STARTUP_FAILED;
		}
		finally {
			if (state==Task.ST_RUNNING) { // Exception not thrown
				state = gitWrapper.getExitValue() == 0 ? Task.ST_ENDED_OK : Task.ST_ENDED_KO;
			}
			endDate = System.currentTimeMillis();
			if (taskListener != null) {
				try {
					taskListener.onEnd(this);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}
			executingThread = null;
		}
	}

	public T getResult() {
		try {
			return gitWrapper.getResult();
		} catch (GitWrapperException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	// /////////////////////////////////
	// CommandListener

	/*
	 * (non-Javadoc)
	 * 
	 * @see jpm.git.wrapper.CommandListener#onOutput(java.lang.String)
	 */
	@Override
	public void onOutput(String output) {
		synchronized (commandOutput) {
			commandOutput.append(output);
		}
	}

}
