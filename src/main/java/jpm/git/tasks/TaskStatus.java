package jpm.git.tasks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase que representa la foto del estado de una tarea asíncrona en un momento dado
 * @author j.penalba
 *
 */
@XmlType(name="TaskStatus")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="TaskStatus")
public class TaskStatus {
	
	@XmlElement
	protected int state;
	@XmlElement
	protected String newOutput;
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getNewOutput() {
		return newOutput;
	}
	public void setNewOutput(String newOutput) {
		this.newOutput = newOutput;
	}

}
