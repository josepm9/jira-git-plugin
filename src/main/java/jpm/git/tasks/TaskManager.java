package jpm.git.tasks;

import java.io.File;

import jpm.git.tasks.impl.GitCloneTask;
import jpm.git.tasks.impl.GitFetchTask;
import jpm.git.tasks.impl.GitLogIssueTask;

public interface TaskManager {

	/**
	 * Obtener el número (aproximado) de tareas en ejecución
	 * 
	 * @return
	 */
	public int getRunningTasksCount();

	/**
	 * Obtener el número (aproximado) de tareas en cola para ser ejecutadas
	 * 
	 * @return
	 */
	public int getQueuedTasksCount();

	/**
	 * Registrar un nuevo escuchador de eventos de tareas administrativas
	 * 
	 * @param l
	 */
	public void registerTaskListener(final TaskListener l);

	/**
	 * Eliminar un nuevo escuchador de eventos de tareas administrativas
	 * 
	 * @param l
	 */
	public void unregisterTaskListener(final TaskListener l);

	/**
	 * Iniciar una tarea asíncrona. La tarea puede quedar encolada. {@link Task}
	 * 
	 * @param task
	 * @return la clave de la tarea para futuras consultas
	 */
	public String start(Task<?> task);

	/**
	 * Ejecuta una tarea síncrona.
	 * 
	 * @param task
	 * @return la clave de la tarea para futuras consultas
	 */
	public void syncRun(Task<?> task);

	/**
	 * Recuperar el estado de una tarea asíncrona
	 * 
	 * @param key
	 * @return
	 */
	public TaskStatus getTaskStatus(String key);

	/**
	 * Crear una tarea "GitClone" que se podrá ejecutar en asíncrono.<br/>
	 * Para ejecutar en asíncrono utilizar: {@link #start(Task)}
	 * 
	 * @param rootDir
	 * @param remoteRepoURL
	 * @param authUser
	 * @param authPassword
	 * @return
	 */
	public GitCloneTask createGitCloneTask(String rootDir, String remoteRepoURL, String authUser, String authPassword);

	/**
	 * Crear una tarea "GitFetch" que se podrá ejecutar en asíncrono.<br/>
	 * Para ejecutar en asíncrono utilizar: {@link #start(Task)}
	 * 
	 * @param relativeDir
	 * @return
	 */
	public GitFetchTask createGitFetchTask(String relativeDir, String gitRepoURL, String authUser,
			String authPassword);

	/**
	 * Crear una tarea "GitLog" filtrados por incidencia que se podrá ejecutar
	 * en asíncrono.<br/>
	 * Para ejecutar en asíncrono utilizar: {@link #start(Task)}
	 * 
	 * @param relativeDir
	 * @param issue
	 * @return
	 */
	public GitLogIssueTask createGitLogIssueTask(String relativeDir, String issue);

	/**
	 * Obtener la carpeta de proyeto dentro del área de trabajo
	 * 
	 * @param projectKey
	 * @return
	 */
	public File getProjectWorkDir(String projectKey);

	/**
	 * Detener el administrador de tareas
	 * 
	 * @param now
	 * @param millis
	 */
	public void shutdown(final boolean now, final long millis);
}
