package jpm.git.svc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.GitCommit;
import jpm.git.bo.GitInfo;
import jpm.git.bo.GitLogIssue;
import jpm.git.bo.GitRepository;
import jpm.git.bo.PropertyBO;
import jpm.git.bo.TaskInfo;
import jpm.git.config.Configuration;
import jpm.git.config.Property;
import jpm.git.persistence.DaoException;
import jpm.git.persistence.GitRepositoryDAO;
import jpm.git.persistence.PropertyDao;
import jpm.git.svc.ServiceException.ErrorCode;
import jpm.git.tasks.Task;
import jpm.git.tasks.TaskListener;
import jpm.git.tasks.TaskManager;
import jpm.git.tasks.TaskStatus;
import jpm.git.tasks.impl.GitLogIssueTask;
import jpm.git.util.GitUtil;
import jpm.git.util.IoUtil;
import jpm.git.wrapper.Command;
import jpm.git.wrapper.CommandFactory;
import jpm.git.wrapper.GitWrapperException;

public class GitServiceImpl implements GitService, TaskListener {

	final static Logger LOG = LoggerFactory.getLogger(GitServiceImpl.class);

	/**
	 * Gestor de tareas
	 */
	final TaskManager taskManager;

	/**
	 * Configuración del plugin
	 */
	final Configuration cfg;

	/**
	 * Filtro (posiblemente de seguridad) a aplicar a los métodos
	 */
	protected GitApiMethodFilter filter = null;

	/**
	 * Persistencia de datos de repositorio
	 */
	final GitRepositoryDAO gitRepositoryDAO;

	/**
	 * Persistencia de propiedades
	 */
	final PropertyDao<? extends Property> propertyDao;

	public GitServiceImpl(final Configuration cfg, final TaskManager taskManager, GitApiMethodFilter filter,
			final GitRepositoryDAO gitRepositoryDAO, PropertyDao<? extends Property> propertyDao) {
		this.cfg = cfg;
		this.taskManager = taskManager;
		this.taskManager.registerTaskListener(this);
		this.filter = filter;
		this.gitRepositoryDAO = gitRepositoryDAO;
		this.propertyDao = propertyDao;
	}

	@Override
	public GitInfo findGitExecutable() throws ServiceException {
		LOG.debug("findGitExecutable(): SVC IN");
		if (filter != null)
			filter.findGitExecutable();
		final StringBuilder s = new StringBuilder();
		final String git = GitUtil.findGitExecutable(s);
		if (git != null) {
			cfg.setGitExecutable(git);
			return new GitInfo(cfg.getGitExecutable(), s.toString(), null);
		} else {
			throw new ServiceException(ErrorCode.E_NOT_FOUND, s.toString(), null);
		}
	}

	@Override
	public GitInfo getGitExecutable() throws ServiceException {
		LOG.debug("getGitExecutable(): SVC IN");
		if (filter != null)
			filter.getGitExecutable();
		try {
			final StringBuilder s = new StringBuilder();
			String error = null;
			final Command<String> git = CommandFactory.getInstance().createGitVersionCommand(cfg.getGitExecutable());
			try {
				git.execute();
			} catch (GitWrapperException e) {
				error = e.getMessage();
			}
			// Si el comando se lanzó con éxito, obtenemos la salida
			if (error == null) {
				GitUtil.logExecution(git, s);
				final int exitValue = git.getExitValue();
				if (exitValue != 0) {
					error = "Exit value == " + exitValue;
				}
			}
			return new GitInfo(cfg.getGitExecutable(), s.toString(), error);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_GENERIC, e.getMessage(), null);
		}
	}

	@Override
	public GitInfo setGitExecutable(@FormParam(value = "git") String git) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("setGitExecutable(" + git + "): SVC IN");
		}
		if (filter != null)
			filter.setGitExecutable(git);
		cfg.setGitExecutable(git);
		return getGitExecutable();
	}

	// ///////////////////////////////////////////////////
	// Administración de proyectos y repositorios

	/**
	 * Realizar las validaciones necesarias para certificar que es posible
	 * lanzar el comando git clone
	 * 
	 * @param projectKey
	 * @param repoURL
	 * @param authUser
	 * @param authPassword
	 * @param gitRepoManType
	 * @param createURLs
	 * @return la clave del nuevo repositorio a crear (nombre de la carpeta del
	 *         repositorio en la carpeta de proyecto)
	 * @throws ServiceException
	 */
	protected String validateCreateNewRepository(String projectKey, String repoURL, String authUser,
			String authPassword, String gitRepoManType, Boolean createURLs) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("createNewRepository(projectKey: " + projectKey + ", repoURL: " + repoURL + ", gitRepoManType: "
					+ gitRepoManType + ", createURLs: " + createURLs + "): SVC IN");
		}
		if (filter != null)
			filter.createNewRepository(projectKey, repoURL, authUser, authPassword, gitRepoManType, createURLs);
		LOG.debug("Clonar " + repoURL);
		// Comprobar si el repositorio tiene un nombre correcto y si ya existe
		final String repoKey = repoURL.substring(repoURL.lastIndexOf("/") + 1);
		// if (repoKey.toLowerCase().endsWith(".git")) {
		// repoKey = repoKey.substring(0, repoKey.length() - 4);
		// }
		// Comprobar si el repositorio existe
		LOG.debug("Comprobar si el repositorio " + projectKey + "/" + repoKey + " existe");
		GitRepository gitRepository = null;
		try {
			gitRepository = gitRepositoryDAO.getRepository(projectKey, repoKey);
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
		if (gitRepository != null) {
			LOG.error("El repositorio ya existe");
			throw new ServiceException(ErrorCode.E_ALREADY_EXISTS,
					"El repositorio " + projectKey + "/" + repoKey + " ya existe", null);
		}
		// Si no existe, pero existe la carpeta, la borramos
		final File repoFolder = new File(cfg.getWorkDir() + File.separator + projectKey + File.separator + repoKey);
		if (repoFolder.exists()) {
			LOG.warn("La carpeta para el repositorio " + projectKey + "/" + repoKey + " ya existe");
			if (!IoUtil.delete(repoFolder)) {
				LOG.error("El carpeta del repositorio ya existe y no fue posible eliminarla");
				throw new ServiceException(ErrorCode.E_ALREADY_EXISTS, "La carpeta para el repositorio " + projectKey
						+ "/" + repoKey + " ya existe y no fue posible eliminarla", null);
			}
		}
		// Crear el directorio padre
		final File projectFolder = new File(cfg.getWorkDir() + File.separator + projectKey);
		if (!projectFolder.exists() && !projectFolder.mkdirs()) {
			LOG.error("No fue posible crear la carpeta " + cfg.getWorkDir() + File.separator + projectKey);
			throw new ServiceException(ErrorCode.E_FS_ERROR,
					"No fue posible crear la carpeta " + cfg.getWorkDir() + File.separator + projectKey, null);
		}
		;
		// Devolver el comando
		return repoKey;
	}

	@Override
	public GitRepository createNewRepository(String projectKey, String repoURL, String authUser, String authPassword,
			String gitRepoManType, Boolean createURLs) throws ServiceException {
		final String repoKey = validateCreateNewRepository(projectKey, repoURL, authUser, authPassword, gitRepoManType,
				createURLs);
		LOG.debug("About to launch command...");
		final Task<Object> task = taskManager.createGitCloneTask(projectKey, repoURL, authUser, authPassword);
		// Preparar respuesta, además la necesitamos como datos
		final GitRepository repo = new GitRepository(projectKey, repoKey, repoURL, authUser, authPassword,
				gitRepoManType, createURLs == null ? false : createURLs);
		task.setData(repo);
		taskManager.syncRun(task);
		if (task.getStatus() != Task.ST_ENDED_OK) {
			throw new ServiceException(ErrorCode.E_TASK_FAILED, "JGit clone task failed: " + task.purgeCommandOutput(),
					null);
		}
		// Ok
		LOG.debug("Done: send response back");
		return repo;
	}

	@Override
	public TaskInfo async_createNewRepository(String projectKey, String repoURL, String authUser, String authPassword,
			String gitRepoManType, Boolean createURLs) throws ServiceException {
		final String repoKey = validateCreateNewRepository(projectKey, repoURL, authUser, authPassword, gitRepoManType,
				createURLs);
		LOG.debug("About to launch command...");
		final Task<Object> task = taskManager.createGitCloneTask(projectKey, repoURL, authUser, authPassword);
		// Para enlazar con el repositorio en el evento onEnd
		final GitRepository repo = new GitRepository(projectKey, repoKey, repoURL, authUser, authPassword,
				gitRepoManType, createURLs == null ? false : createURLs);
		task.setData(repo);
		// iniciar tarea
		final String taskKey = taskManager.start(task);
		// Ok
		LOG.debug("Done: send response back");
		return new TaskInfo(taskKey, taskManager.getTaskStatus(taskKey), repo);
	}

	/**
	 * Realizar las validaciones necesarias para certificar que es posible
	 * lanzar el comando git fetch
	 * 
	 * @param projectKey
	 * @param repoKey
	 *            return Objeto "{@link GitRepository}" correspondiente al
	 *            repositorio a sincronizar. Si no es posible realizar la
	 *            sincronización se lanzará una excepción de tipo
	 *            "{@link ServiceException}"
	 * @throws ServiceException
	 */
	protected GitRepository validateSynchronizeRepository(String projectKey, String repoKey) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("synchronizeRepository(projectKey: " + projectKey + ", repoKey: " + repoKey + "): SVC IN");
		}
		if (filter != null)
			filter.synchronizeRepository(projectKey, repoKey);
		// Comprobar si el repositorio existe
		LOG.debug("Comprobar si el repositorio existe");
		GitRepository gitRepository = null;
		try {
			gitRepository = gitRepositoryDAO.getRepository(projectKey, repoKey);
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
		if (gitRepository == null) {
			LOG.error("El repositorio no existe");
			throw new ServiceException(ErrorCode.E_NOT_FOUND,
					"El repositorio " + projectKey + "/" + repoKey + " no existe", null);
		}
		// Devolver el repositorio
		return gitRepository;
	}

	@Override
	public TaskInfo async_synchronizeRepository(String projectKey, String repoKey) throws ServiceException {
		final GitRepository repo = validateSynchronizeRepository(projectKey, repoKey);
		LOG.debug("About to launch command...");
		final Task<Object> task = taskManager.createGitFetchTask(projectKey + File.separator + repoKey, repo.getUrl(),
				repo.getAuthUser(), repo.getAuthPassword());
		// Para enlazar con el repositorio en el evento onEnd
		task.setData(repo);
		// Iniciar tarea
		final String taskKey = taskManager.start(task);
		// Ok
		LOG.debug("Done: send response back");
		return new TaskInfo(taskKey, taskManager.getTaskStatus(taskKey), repo);
	}

	@Override
	public void synchronizeRepository(String projectKey, String repoKey) throws ServiceException {
		final GitRepository repo = validateSynchronizeRepository(projectKey, repoKey);
		LOG.debug("About to launch command...");
		final String repoRelDir = projectKey + File.separator + repoKey;
		Task<Object> task;
		String taskLabel;
		if (taskManager.getProjectWorkDir(repoRelDir).exists()) {
			taskLabel = "fetch";
			task = taskManager.createGitFetchTask(repoRelDir, repo.getUrl(), repo.getAuthUser(),
					repo.getAuthPassword());
		} else {
			taskLabel = "clone";
			task = taskManager.createGitCloneTask(projectKey, repo.getUrl(), repo.getAuthUser(),
					repo.getAuthPassword());
		}
		taskManager.syncRun(task);
		if (task.getStatus() != Task.ST_ENDED_OK) {
			throw new ServiceException(ErrorCode.E_TASK_FAILED,
					"JGit " + taskLabel + " task failed: " + task.purgeCommandOutput(), null);
		}
		// Ok
		LOG.debug("Done");
	}

	@Override
	public GitRepository[] getRepositories() throws ServiceException {
		LOG.debug("getRepositories(): SVC IN");
		if (filter != null)
			filter.getRepositories();
		try {
			return gitRepositoryDAO.getRepositories();
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
	}

	@Override
	public GitRepository[] getRepositories(String projectKey) throws ServiceException {
		LOG.debug("getRepositories(" + projectKey + "): SVC IN");
		if (filter != null)
			filter.getRepositories(projectKey);
		try {
			return gitRepositoryDAO.getRepositories(projectKey);
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
	}

	@Override
	public GitRepository getRepository(String projectKey, String repoKey) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("getRepository(projectKey: " + projectKey + ",repoKey: " + repoKey + "): SVC IN");
		}
		if (filter != null)
			filter.getRepository(projectKey, repoKey);
		try {
			final GitRepository gitRepository = gitRepositoryDAO.getRepository(projectKey, repoKey);
			if (gitRepository == null) {
				LOG.error("Repositorio no encontrado projectKey: " + projectKey + ", repoKey: " + repoKey);
				throw new ServiceException(ErrorCode.E_NOT_FOUND, "Repositorio no encontrado", null);
			}
			return gitRepository;
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
	}

	@Override
	public GitRepository updateRepository(String projectKey, String repoKey, String repoURL, String authUser,
			String authPassword, String gitRepoManType, Boolean createURLs) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("updateRepository(projectKey: " + projectKey + ", repoURL: " + repoURL + ", gitRepoManType: "
					+ gitRepoManType + ", createURLs: " + createURLs + "): SVC IN");
		}
		if (filter != null)
			filter.updateRepository(projectKey, repoKey, repoURL, authUser, authPassword, gitRepoManType, createURLs);
		try {
			GitRepository gitRepository = gitRepositoryDAO.getRepository(projectKey, repoKey);
			if (gitRepository == null) {
				LOG.error("Repositorio no encontrado projectKey: " + projectKey + ", repoKey: " + repoKey);
				throw new ServiceException(ErrorCode.E_NOT_FOUND, "Repositorio no encontrado", null);
			}
			// projectKey, repoKey y repoURL no pueden cambiar...
			if (projectKey.equals(gitRepository.getProjectKey()) && repoKey.equals(gitRepository.getRepoKey())
					&& repoURL.equals(gitRepository.getUrl())) {
				gitRepository = new GitRepository(projectKey, repoKey, repoURL, authUser, authPassword, gitRepoManType,
						createURLs);
				gitRepositoryDAO.updateRepository(gitRepository);
				return gitRepository;
			} else {
				LOG.error("Project key, repository key and repository URL can not be modified");
				throw new ServiceException(ErrorCode.E_ARGUMENTS,
						"Project key, repository key and repository URL can not be modified", null);
			}
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
	}

	@Override
	public GitRepository deleteRepository(String projectKey, String repoKey) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("deleteRepository(projectKey: " + projectKey + ",repoKey: " + repoKey + "): SVC IN");
		}
		if (filter != null)
			filter.deleteRepository(projectKey, repoKey);
		try {
			final GitRepository gitRepository = getRepository(projectKey, repoKey);
			if (gitRepository == null) {
				return null;
			} else {
				IoUtil.tryDelete(taskManager.getProjectWorkDir(projectKey + File.separator + repoKey));
				gitRepositoryDAO.deleteRepository(gitRepository);
				return gitRepository;
			}
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		} catch (IOException e) {
			LOG.error(e.getClass().getName() + ": " + e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_GENERIC, e.getMessage(), e);
		}
	}

	@Override
	public TaskStatus getAsyncTaskStatus(String projectKey, String key) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("getAsyncTaskStatus(projectKey: " + projectKey + ",key: " + key + "): SVC IN");
		}
		if (filter != null)
			filter.getAsyncTaskStatus(projectKey, key);
		// TODO: relacionar projectKey y key
		final TaskStatus taskStatus = taskManager.getTaskStatus(key);
		if (taskStatus == null) {
			LOG.error("Tarea " + key + " no encontrada");
			throw new ServiceException(ErrorCode.E_NOT_FOUND, "Tarea " + key + " no encontrada", null);
		} else {
			LOG.debug("Tarea " + key + " estado " + taskStatus.getState());
			return taskStatus;
		}
	}

	// //////////////////////////////////////////
	// Consulta de cambios

	public GitLogIssue[] getIssueCommits(String projectKey, String issueKey) throws ServiceException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("getIssueCommits(projectKey: " + projectKey + ", issueKey: " + issueKey + "): SVC IN");
		}
		if (filter != null)
			filter.getIssueCommits(projectKey, issueKey);
		GitRepository[] gitRepositories;
		try {
			gitRepositories = gitRepositoryDAO.getRepositories(projectKey);
		} catch (DaoException e) {
			LOG.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
		}
		if (gitRepositories != null) {
			final List<GitLogIssue> gitLogIssues = new ArrayList<>();
			GitCommit[] commits = null;
			for (GitRepository gitRepository : gitRepositories) {
				final GitLogIssueTask task = taskManager
						.createGitLogIssueTask(projectKey + File.separator + gitRepository.getRepoKey(), issueKey);
				taskManager.syncRun(task);
				if (task.getStatus() == Task.ST_ENDED_OK) {
					try {
						commits = task.getResult();
						if (commits != null && commits.length > 0) {
							if (LOG.isDebugEnabled()) {
								LOG.debug(
										commits.length + " commits found on repository " + gitRepository.getRepoKey());
							}
							gitLogIssues.add(new GitLogIssue(gitRepository, issueKey, commits));
						} else {
							if (LOG.isDebugEnabled()) {
								LOG.debug("No commits found on repository " + gitRepository.getRepoKey());
							}
						}
					} catch (Exception e) {
						LOG.error(
								"Git log failed, projectKey: " + projectKey + ", repository: "
										+ gitRepository.getRepoKey() + " issueKey: " + issueKey + ": " + e.getMessage(),
								e);
					}
				}
			}
			return gitLogIssues.toArray(new GitLogIssue[gitLogIssues.size()]);
		} else {
			return null;
		}
	}

	// //////////////////////////////////////
	// Implementar TaskListener

	@Override
	public void onEnd(Task<?> task) {
		switch (task.getTaskType()) {
		case GitClone:
			// Git clone finalizado
			final GitRepository info = (GitRepository) task.getData();
			LOG.debug("GitClone onEnd, project " + info.getProjectKey() + " repository " + info.getRepoKey()
					+ ", status " + task.getStatus());
			if (task.getStatus() == Task.ST_ENDED_OK) {
				try {
					gitRepositoryDAO.insertRepository(info);
				} catch (DaoException e) {
					LOG.error(e.getMessage(), e);
				}
			}
			break;
		case GitFetch:
			LOG.debug("GitFetch onEnd");
			break;
		case GitLogIssue:
			LOG.debug("GitLogIssue onEnd");
			break;
		case Generic:
			LOG.debug("Generic onEnd");
			break;
		default:
			LOG.error("onEnd default, unhandled task type!");
			break;
		}
	}

	// ////////////////////////////////////
	// Properties

	public PropertyBO[] getProperties() throws ServiceException {
		final Property[] ps = propertyDao.readProperties("");
		if (ps == null) {
			return null;
		} else {
			final PropertyBO[] pBos = new PropertyBO[ps.length];
			for (int i = 0; i < ps.length; ++i) {
				pBos[i] = PropertyBO.fromProperty(ps[i]);
				pBos[i].setKey(ps[i].getKey().replace("/", ":"));
			}
			return pBos;
		}
	}

	public PropertyBO getProperty(String key) throws ServiceException {
		return PropertyBO.fromProperty(propertyDao.readProperty(key.replace(":", "/")));
	}

	public PropertyBO updateProperty(final String key, final PropertyBO p) throws ServiceException {
		return PropertyBO.fromProperty(propertyDao.updateProperty(key.replace(":", "/"), p.getValue()));
	}

	public PropertyBO createProperty(final PropertyBO p) throws ServiceException {
		return PropertyBO.fromProperty(propertyDao.createProperty(p.getKey().replace(":", "/"), p.getValue()));
	}

	public PropertyBO deleteProperty(final String key) throws ServiceException {
		return PropertyBO.fromProperty(propertyDao.deleteProperty(key.replace(":", "/")));
	}
}
