package jpm.git.svc;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.GadgetDataTable;
import jpm.git.bo.GitCommit;
import jpm.git.bo.GitRepository;
import jpm.git.config.Configuration;
import jpm.git.config.Property;
import jpm.git.persistence.DaoException;
import jpm.git.persistence.GitRepositoryDAO;
import jpm.git.persistence.PropertyDao;
import jpm.git.statistics.CommitsStatistic;
import jpm.git.svc.ServiceException.ErrorCode;
import jpm.git.tasks.TaskManager;
import jpm.git.tasks.impl.GitLogIssueTask;

public class GadgetsServiceImpl implements GadgetsService {

	final static Logger LOG = LoggerFactory.getLogger(GadgetsServiceImpl.class);

	/**
	 * Gestor de tareas
	 */
	final TaskManager taskManager;

	/**
	 * Configuración del plugin
	 */
	final Configuration cfg;

	/**
	 * Persistencia de datos de repositorio
	 */
	final GitRepositoryDAO gitRepositoryDAO;

	/**
	 * Persistencia de propiedades
	 */
	final PropertyDao<? extends Property> propertyDao;

	public GadgetsServiceImpl(final Configuration cfg, final TaskManager taskManager,
			final GitRepositoryDAO gitRepositoryDAO, PropertyDao<? extends Property> propertyDao) {
		this.cfg = cfg;
		this.taskManager = taskManager;
		this.gitRepositoryDAO = gitRepositoryDAO;
		this.propertyDao = propertyDao;
	}

	@Override
	public GadgetDataTable commitsStatistics(String projectKey, int repository, int entity, int group, int type)
			throws ServiceException {
		try {
			final CommitsStatistic stats = new CommitsStatistic(repository == 1, entity, group, type);
			// Obtener los repositorios del proyecto
			GitRepository[] repositories;
			try {
				repositories = gitRepositoryDAO.getRepositories(projectKey);
			} catch (DaoException e) {
				LOG.error(e.getMessage(), e);
				throw new ServiceException(ErrorCode.E_PERSISTENCE, e.getMessage(), e);
			}
			if (repositories != null && repositories.length > 0) {
				// Buscar TODOS los commits de cada repositorio
				GitLogIssueTask task;
				GitCommit[] commits;
				String repoKey;
				for (GitRepository repo : repositories) {
					repoKey = repo.getRepoKey();
					task = taskManager.createGitLogIssueTask(projectKey + File.separator + repoKey, "");
					taskManager.syncRun(task);
					commits = task.getResult();
					if (commits != null && commits.length > 0) {
						for (GitCommit commit : commits) {
							stats.compute(repoKey, commit);
						}
					}
				}
			}
			return stats.getDataTable();
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			LOG.error(e.getClass().getName() + ": " + e.getMessage(), e);
			throw new ServiceException(ErrorCode.E_GENERIC, e.getMessage(), e);
		}
	}

}
