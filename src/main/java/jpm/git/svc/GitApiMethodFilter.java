package jpm.git.svc;

import jpm.git.bo.PropertyBO;

public interface GitApiMethodFilter {

	public void findGitExecutable() throws ServiceException;

	public void getGitExecutable() throws ServiceException;

	public void createNewRepository(String projectKey, String repoURL, String authUser, String authPassword,
			String gitRepoManType, Boolean createURLs) throws ServiceException;
	
	public void synchronizeRepository(String projectKey, String repoKey) throws ServiceException;

	public void setGitExecutable(String git) throws ServiceException;

	public void getRepositories() throws ServiceException;

	public void getRepositories(String projectKey) throws ServiceException;

	public void getRepository(String projectKey, String repoKey) throws ServiceException;

	public void updateRepository(String projectKey, String repoKey, String repoURL, String authUser,
			String authPassword, String gitRepoManType, Boolean createURLs) throws ServiceException;

	public void deleteRepository(String projectKey, String repoKey) throws ServiceException;

	public void getAsyncTaskStatus(String projectKey, String key) throws ServiceException;

	public void getIssueCommits(String projectKey, String issueKey) throws ServiceException;

	public void getProperties() throws ServiceException;

	public void getProperty(String key) throws ServiceException;

	public void updateProperty(final String key, final PropertyBO p) throws ServiceException;

	public void createProperty(final PropertyBO bean) throws ServiceException;

	public void deleteProperty(final String key) throws ServiceException;
}
