package jpm.git.svc;

import jpm.git.bo.GadgetDataTable;

public interface GadgetsService {

	/**
	 * F
	 * 
	 * @param projectKey
	 *            clave del proyecto
	 * @param repository
	 *            al generar la tabla de datos. True indica agrupar
	 *            repositorios, (los resultados incluirán una columna
	 *            repositorio
	 * @param entity
	 * @param group
	 * @param type
	 * @return
	 * @throws ServiceException
	 */
	public GadgetDataTable commitsStatistics(String projectKey, int repository, int entity, int group, int type)
			throws ServiceException;

}
