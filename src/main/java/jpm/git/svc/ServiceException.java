package jpm.git.svc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilizada para encapsular el contenido (entidad) de una respuesta HTTP
 * de error de uno de los servicios serializable a JSON/XML.
 * 
 * @author José
 *
 */
@XmlType(name = "ServiceException", propOrder = { "code", "description" })
@XmlRootElement(name = "ServiceException")
@XmlAccessorType(XmlAccessType.NONE)
public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7326192681064271333L;

	public static enum ErrorCode {
		E_NOT_IMPLEMENTED("JGP00001", 501), E_ALREADY_EXISTS("JGP00002", 409), E_TASK_FAILED("JGP00003",
				500), E_NOT_FOUND("JGP00004", 404), E_FS_ERROR("JGP00005", 500), E_FORBIDDEN("JGP00006",
				403), E_PERSISTENCE("JGP00007", 500), E_ARGUMENTS("JGP99998", 400), E_GENERIC("JGP99999", 500);
		public final String key;
		public final int httpStatus;

		private ErrorCode(String key, int httpStatus) {
			this.key = key;
			this.httpStatus = httpStatus;
		}
	}

	/**
	 * Clave de error
	 */
	@XmlElement(name = "key")
	protected String key;

	/**
	 * Código de error
	 */
	public final ErrorCode errorCode;

	/**
	 * Constructor de estado
	 * 
	 * @param code
	 * @param description
	 */
	public ServiceException(final ErrorCode errorCode, String description, Exception cause) {
		super(description, cause);
		this.errorCode = errorCode;
		this.key = errorCode.key;
	}

	/**
	 * {@link #code}
	 * 
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * {@link #code}
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * {@link #description}
	 * 
	 * @return
	 */
	/**
	 * Descripción del error
	 */
	@XmlElement(name = "description")
	@Override
	public String getMessage() {
		return super.getMessage();
	}
}
