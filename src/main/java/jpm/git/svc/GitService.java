package jpm.git.svc;

import jpm.git.bo.GitInfo;
import jpm.git.bo.GitLogIssue;
import jpm.git.bo.GitRepository;
import jpm.git.bo.PropertyBO;
import jpm.git.bo.TaskInfo;
import jpm.git.tasks.TaskStatus;

/**
 * Servicio que contiene las operaciones administrativas del plugin
 * 
 * @author j.penalba
 *
 */
public interface GitService {

	// ///////////////////////////////////////////
	// Ejecutable

	/**
	 * Intentar buscar la localización del ejecutable de git de manera
	 * automática
	 * 
	 * @return
	 */
	public GitInfo findGitExecutable() throws ServiceException;

	/**
	 * Obtener la ruta del ejecutable git configurada actualmente
	 * 
	 * @return
	 */
	public GitInfo getGitExecutable() throws ServiceException;

	/**
	 * Establecer manualmente la ruta del ejecutable git
	 * 
	 * @param git
	 * @return
	 */
	public GitInfo setGitExecutable(String git) throws ServiceException;

	// ///////////////////////////////////////////
	// Administración de proyetos y repositorios

	/**
	 * Crear un nuevo repositorio de manera asíncrona. Utilizar {@link #getAsyncTaskStatus(String, String)} para consultar el estado de la operación.
	 * 
	 * @param projectKey
	 * @param repoURL
	 * @param authUser
	 * @param authPassword
	 * @param gitRepoManType
	 * @param createURLs
	 * @return {@link TaskInfo}
	 */
	public TaskInfo async_createNewRepository(String projectKey, String repoURL, String authUser, String authPassword,
			String gitRepoManType, Boolean createURLs) throws ServiceException;
	
	/**
	 * Crear un nuevo repositorio
	 * 
	 * @param projectKey
	 * @param repoURL
	 * @param authUser
	 * @param authPassword
	 * @param gitRepoManType
	 * @param createURLs
	 * @return {@link GitRepository}
	 */
	public GitRepository createNewRepository(String projectKey, String repoURL, String authUser, String authPassword,
			String gitRepoManType, Boolean createURLs) throws ServiceException;
	
	/**
	 * Sincronizar un repositorio existente. Utilizar {@link #getAsyncTaskStatus(String, String)} para consultar el estado de la operación.
	 * 
	 * @param projectKey
	 * @param repoKey
	 * @return {@link TaskInfo}
	 */
	public TaskInfo async_synchronizeRepository(String projectKey, String repoKey) throws ServiceException;
	
	/**
	 * Sincronizar un repositorio existente
	 * 
	 * @param projectKey
	 * @param repoKey
	 */
	public void synchronizeRepository(String projectKey, String repoKey) throws ServiceException;

	public GitRepository[] getRepositories() throws ServiceException;

	public GitRepository[] getRepositories(String projectKey) throws ServiceException;

	public GitRepository getRepository(String projectKey, String repoKey) throws ServiceException;

	public GitRepository updateRepository(String projectKey, String repoKey, String repoURL, String authUser,
			String authPassword, String gitRepoManType, Boolean createURLs) throws ServiceException;

	public GitRepository deleteRepository(String projectKey, String repoKey) throws ServiceException;

	public TaskStatus getAsyncTaskStatus(String projectKey, String key) throws ServiceException;

	// //////////////////////////////////////////
	// Consulta de cambios

	/**
	 * Obtener todos los commits asociados a una incidencia
	 * 
	 * @param projectKey
	 * @param issueKey
	 * @return {@link GitLogIssue}[] cada uno con el conjunto de cambios
	 *         detectados para la incidencia.
	 */
	public GitLogIssue[] getIssueCommits(String projectKey, String issueKey) throws ServiceException;

	// //////////////////////////////////////////
	// Propiedades

	public PropertyBO[] getProperties() throws ServiceException;

	public PropertyBO getProperty(String key) throws ServiceException;

	public PropertyBO updateProperty(final String key, final PropertyBO p) throws ServiceException;

	public PropertyBO createProperty(final PropertyBO p) throws ServiceException;

	public PropertyBO deleteProperty(final String key) throws ServiceException;
}
