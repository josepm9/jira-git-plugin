package jpm.git.svc.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.bo.PropertyBO;
import jpm.git.svc.GitService;
import jpm.git.svc.ServiceException;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GitRestApi {

	final static Logger LOG = LoggerFactory.getLogger(GitRestApi.class);

	final GitService svc;

	public GitRestApi(final GitService svc) {
		this.svc = svc;
	}

	@GET
	@Path("/git")
	public Response getGitExecutable(@QueryParam(value = "find") Boolean find) {
		try {
			if (Boolean.TRUE.equals(find)) {
				return Response.ok(svc.findGitExecutable()).build();
			} else {
				return Response.ok(svc.getGitExecutable()).build();
			}
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@PUT
	@Path("/git")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response setGitExecutable(@FormParam(value = "git") String git) {
		try {
			return Response.ok(svc.setGitExecutable(git)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@GET
	@Path("/repositories")
	public Response getRepositories() {
		try {
			return Response.ok(svc.getRepositories()).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@POST
	@Path("/projects/{projectKey}/repositories")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createNewRepository(@PathParam(value = "projectKey") String projectKey,
			@FormParam(value = "repoURL") String repoURL, @FormParam(value = "authUser") String authUser,
			@FormParam(value = "authPassword") String authPassword,
			@FormParam(value = "gitRepoManType") String gitRepoManType,
			@FormParam(value = "createURLs") Boolean createURLs, @FormParam(value = "async") Boolean async) {
		try {
			if (Boolean.TRUE.equals(async)) {
				return Response.status(202).entity(svc.async_createNewRepository(projectKey, repoURL, authUser,
						authPassword, gitRepoManType, createURLs)).build();
			} else {
				return Response.status(201).entity(svc.createNewRepository(projectKey, repoURL, authUser, authPassword,
						gitRepoManType, createURLs)).build();
			}
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@GET
	@Path("/projects/{projectKey}/repositories")
	public Response getRepositories(@PathParam("projectKey") String projectKey) {
		try {
			return Response.ok(svc.getRepositories(projectKey)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@GET
	@Path("/projects/{projectKey}/repositories/{repoKey}")
	public Response getRepository(@PathParam("projectKey") String projectKey, @PathParam("repoKey") String repoKey) {
		try {
			return Response.ok(svc.getRepository(projectKey, repoKey)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@PUT
	@Path("/projects/{projectKey}/repositories/{repoKey}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateRepository(@PathParam("projectKey") String projectKey, @PathParam("repoKey") String repoKey,
			@FormParam(value = "repoURL") String repoURL, @FormParam(value = "authUser") String authUser,
			@FormParam(value = "authPassword") String authPassword,
			@FormParam(value = "gitRepoManType") String gitRepoManType,
			@FormParam(value = "createURLs") Boolean createURLs) {
		try {
			return Response.ok(svc.updateRepository(projectKey, repoKey, repoURL, authUser, authPassword,
					gitRepoManType, createURLs)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@DELETE
	@Path("/projects/{projectKey}/repositories/{repoKey}")
	public Response deleteRepository(@PathParam("projectKey") String projectKey, @PathParam("repoKey") String repoKey) {
		try {
			return Response.ok(svc.deleteRepository(projectKey, repoKey)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@GET
	@Path("/projects/{projectKey}/task/{key}")
	public Response getAsyncTaskStatus(@PathParam("projectKey") String projectKey, @PathParam("key") String key) {
		try {
			return Response.ok(svc.getAsyncTaskStatus(projectKey, key)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@GET
	@Path("/projects/{projectKey}/commits")
	public Response getIssueCommits(@PathParam("projectKey") String projectKey,
			@QueryParam("issueKey") String issueKey) {
		try {
			return Response.ok(svc.getIssueCommits(projectKey, issueKey)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	// //////////////////////////////
	// Properties

	@GET
	@Path("/properties")
	public Response getProperties() {
		try {
			return Response.ok(svc.getProperties()).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@GET
	@Path("/properties/{key}")
	public Response getProperty(@PathParam("key") String key) {
		try {
			return Response.ok(svc.getProperty(key)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@PUT
	@Path("/properties/{key}")
	public Response updateProperty(@PathParam("key") final String key, final PropertyBO p) {
		try {
			return Response.ok(svc.updateProperty(key, p)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@POST
	@Path("/properties/{key}")
	public Response createProperty(final PropertyBO p) {
		try {
			return Response.ok(svc.createProperty(p)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

	@DELETE
	@Path("/properties/{key}")
	public Response deleteProperty(@PathParam("key") final String key) {
		try {
			return Response.ok(svc.deleteProperty(key)).build();
		} catch (ServiceException e) {
			return Response.status(e.errorCode.httpStatus).entity(e).build();
		}
	}

}
