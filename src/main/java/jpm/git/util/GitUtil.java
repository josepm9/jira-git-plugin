package jpm.git.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.wrapper.Command;
import jpm.git.wrapper.CommandFactory;
import jpm.git.wrapper.GitWrapperException;

public class GitUtil {

	final static Logger LOG = LoggerFactory.getLogger(GitUtil.class);

	private static String[] GIT_COMMON_PATHS = { "git.exe", System.getenv("ProgramFiles") + "\\Git\\cmd\\git.exe",
			System.getProperty("user.home") + "\\AppData\\Local\\Programs\\Git\\cmd\\git.exe", "git", "/usr/bin/git",
			"/usr/local/bin/git" };

	/**
	 * Log de una ejecución
	 * 
	 * @param g
	 * @param call
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static int logExecution(final Command<?> g, StringBuilder output) throws InterruptedException, IOException {
		final StringBuilder sb = new StringBuilder("******************************\n");
		sb.append(g.getFullOutput());
		LOG.info(sb.toString() + (output==null?"":output.toString())
				+ "************************************************************************************");
		return g.getExitValue();
	}

	/**
	 * Utilidad para encontrar la ruta de "git" entre distintas rutas frecuentes
	 * 
	 * @param output
	 * @return
	 */
	public static String findGitExecutable(final StringBuilder output) {
		String executable = null;
		for (String s : GIT_COMMON_PATHS) {
			try {
				final Command<String> g = CommandFactory.getInstance().createGitVersionCommand(s);
				g.execute();
				final int result = logExecution(g, output);
				if (result == 0) {
					executable = s;
					break;
				} else {
					LOG.warn("Invocado 'git' en la " + s + " con resultado: " + result);
				}
			} catch (GitWrapperException e) {
				LOG.warn("No se encontró el ejecutable 'git' en la ruta " + s + ": " + e.getMessage());
			} catch (InterruptedException e) {
				LOG.warn("No se encontró el ejecutable 'git' en la ruta " + s + ": " + e.getMessage());
			} catch (IOException e) {
				LOG.warn("No se encontró el ejecutable 'git' en la ruta " + s + ": " + e.getMessage());
			}
		}
		LOG.info("Ejecutable encontrado: " + executable);
		return executable;
	}

}
