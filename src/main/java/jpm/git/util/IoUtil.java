package jpm.git.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IoUtil {

	final static Logger LOG = LoggerFactory.getLogger(IoUtil.class);

	/**
	 * Utilidad para enviar a un StringBuilder los datos de un stream
	 * 
	 * @param is
	 * @param sb
	 * @throws IOException
	 */
	public static void is2string(final InputStream is, StringBuilder sb) throws IOException {
		final byte[] buffer = new byte[2048];
		int n;
		while ((n = is.read(buffer, 0, 2048)) > 0) {
			sb.append(new String(buffer, 0, n));
		}
	}

	/**
	 * Utilidad para eliminar una carpeta junto con todo su contenido
	 * 
	 * @param f
	 */
	public static void nioDelete(File file) throws IOException {
		if (file.exists()) {
			final File[] contents = file.listFiles();
			if (contents != null && contents.length > 0) {
				for (File f : contents) {
					nioDelete(f);
				}
			}
			java.nio.file.Files.delete(file.toPath());
		}
	}

	/**
	 * Utilidad para eliminar una carpeta junto con todo su contenido
	 * 
	 * @param f
	 */
	public static boolean delete(File file) {
		if (file.exists()) {
			final File[] contents = file.listFiles();
			if (contents != null && contents.length > 0) {
				for (File f : contents) {
					delete(f);
				}
			}
			return file.delete();
		}
		return true;
	}

	/**
	 * Intentar borrar una carpeta. La existencia de este metodo se debe a los
	 * problemas para borrar en windows.
	 * 
	 * @param f
	 * @throws IOException
	 */
	public static void tryDelete(File f) throws IOException {
		for (int i = 0; i < 20; ++i) {
			if (IoUtil.delete(f)) {
				return;
			}
		}
		LOG.warn("IoUtil.delete failed 20 times, trying with IoUtil.nioDelete");
		try {
			IoUtil.nioDelete(f);
		} catch (IOException e) {
			LOG.error(e.getClass().getName() + ": " + e.getMessage());
		}
	}

}
