package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "GadgetDataRow")
@XmlType(name = "GadgetDataRow")
@XmlAccessorType(XmlAccessType.FIELD)
public class GadgetDataRow {

	protected Object[] cells;

	/**
	 * @return the cells
	 */
	public Object[] getCells() {
		return cells;
	}

	/**
	 * @param cells
	 *            the cells to set
	 */
	public void setCells(Object[] cells) {
		this.cells = cells;
	}

	// Util methods

	/**
	 * 
	 * @param c
	 */
	public void inc(int c) {
		final Integer i = (Integer) cells[cells.length - 1];
		cells[cells.length - 1] = i == null ? 1 : (i + c);
	}
}
