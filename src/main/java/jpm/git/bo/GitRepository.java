package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Clase utilizada para encapsular la información de un repositorio en una
 * respuesta HTTP de error de uno de los servicios serializable en JSON/XML.
 * 
 * @author José
 *
 */
@XmlType(name = "GitRepository", propOrder = { "projectKey", "id", "url", "authUser", "authPassword", "type",
		"createURLs" })
@XmlRootElement(name = "GitRepository")
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
public class GitRepository {

	/**
	 * Clave del proyecto que contiene el repositorio.
	 */
	@XmlElement
	protected String projectKey;

	/**
	 * Clave del repositorio dentro del proyecto que lo contiene. Será única
	 * dentro de un proyecto. la pareja {@link #projectKey}/repoKey será única
	 * en Jira.
	 */
	@XmlElement(name = "id")
	@JsonProperty("id")
	protected String repoKey;

	/**
	 * URL de acceso al repositorio
	 */
	@XmlElement
	protected String url;

	/**
	 * Usuario para autenticar la conexión al repositorio
	 */
	@XmlElement
	protected String authUser;

	/**
	 * Contraseña para autenticar la conexión al repositorio
	 */
	@XmlElement
	protected String authPassword;

	/**
	 * Tipo de administrador de repositorio git remoto (gitlab, bitbucket,
	 * github, ...)
	 */
	@XmlElement
	protected String type;

	/**
	 * Propiedad que indica si se desea crear URLs de acceso a los commits del
	 * administrador de repositorio git remoto
	 */
	@XmlElement
	protected boolean createURLs;

	/**
	 * Constructor por defecto
	 */
	public GitRepository() {
	}

	/**
	 * Constructor de estado
	 * 
	 * @param projectKey
	 * @param repoKey
	 * @param url
	 * @param type
	 * @param createURLs
	 */
	public GitRepository(String projectKey, String repoKey, String url, String authUser, String authPassword,
			String type, boolean createURLs) {
		this.projectKey = projectKey;
		this.repoKey = repoKey;
		this.url = url;
		this.type = type;
		this.createURLs = createURLs;
		this.authUser = authUser;
		this.authPassword = authPassword;
	}

	/**
	 * {@link #projectKey}
	 * 
	 * @return
	 */
	public String getProjectKey() {
		return projectKey;
	}

	/**
	 * {@link #projectKey}
	 */
	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}

	/**
	 * {@link #repoKey}
	 * 
	 * @return
	 */
	public String getRepoKey() {
		return repoKey;
	}

	/**
	 * {@link #repoKey}
	 */
	public void setRepoKey(String repoKey) {
		this.repoKey = repoKey;
	}

	/**
	 * {@link #url}
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * {@link #url}
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the authUser
	 */
	public String getAuthUser() {
		return authUser;
	}

	/**
	 * @param authUser
	 *            the authUser to set
	 */
	public void setAuthUser(String authUser) {
		this.authUser = authUser;
	}

	/**
	 * @return the authPassword
	 */
	public String getAuthPassword() {
		return authPassword;
	}

	/**
	 * @param authPassword
	 *            the authPassword to set
	 */
	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}

	/**
	 * {@link #type}
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * {@link #type}
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * {@link #createURLs}
	 * 
	 * @return
	 */
	public boolean isCreateURLs() {
		return createURLs;
	}

	/**
	 * {@link #createURLs}
	 */
	public void setCreateURLs(boolean createURLs) {
		this.createURLs = createURLs;
	}

}
