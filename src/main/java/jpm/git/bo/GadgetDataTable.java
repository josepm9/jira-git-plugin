package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "GadgetDataTable")
@XmlType(name = "GadgetDataTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class GadgetDataTable {
	
	protected String[] columns;
	protected GadgetDataRow[] rows;

	/**
	 * @return the columns
	 */
	public String[] getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(String[] columns) {
		this.columns = columns;
	}

	/**
	 * @return the rows
	 */
	public GadgetDataRow[] getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(GadgetDataRow[] rows) {
		this.rows = rows;
	}

	
}
