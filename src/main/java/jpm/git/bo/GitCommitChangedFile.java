package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Cambio en un fichero dentro de un commit
 * @author José
 *
 */
@XmlRootElement(name="GitCommitChangedFile")
@XmlType(name="GitCommitChangedFile")
@XmlAccessorType(XmlAccessType.FIELD)
public class GitCommitChangedFile {
	/**
	 * Tipo de cambio:
	 * <ul>
	 * <li>A: añadido</li>
	 * <li>D: eliminado</li>
	 * <li>M: modificado</li>
	 * </ul>
	 */
	protected String type;
	/**
	 * Ruta relativa (en el repositorio) del fichero modificado
	 */
	protected String filePath;
	
	public GitCommitChangedFile() {
	}
	
	public GitCommitChangedFile(String type, String filePath) {
		super();
		this.type = type;
		this.filePath = filePath;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}



	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}



	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}



	public String toString() {
		return "{ 'GitCommitChangedFile' : { 'type' : '" + type + "', 'filePath' : '" + filePath + "' } }";
	}

}
