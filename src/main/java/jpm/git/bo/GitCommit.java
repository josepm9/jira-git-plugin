package jpm.git.bo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Datos de un commit.
 * 
 * @see {@link GitWrapperCommitParser#FORMAT_PARAM)
 * @author José
 *
 */
@XmlRootElement(name = "GitCommit")
@XmlType(name = "GitCommit")
@XmlAccessorType(XmlAccessType.FIELD)
public class GitCommit {

	protected String hash;
	protected String author;
	protected String authorEmail;
	protected Date authorDate;
	protected String committer;
	protected String committerEmail;
	protected Date committerDate;
	protected String refNames;
	protected String subject;
	protected String body;
	protected String notes;
	protected GitCommitChangedFile[] changedFiles;

	public GitCommit() {
	}

	public GitCommit(String hash, String author, String authorEmail, Date authorDate, String committer,
			String committerEmail, Date committerDate, String refNames, String subject, String body, String notes,
			GitCommitChangedFile[] changedFiles) {
		this.hash = hash;
		this.author = author;
		this.authorEmail = authorEmail;
		this.authorDate = authorDate;
		this.committer = committer;
		this.committerEmail = committerEmail;
		this.committerDate = committerDate;
		this.refNames = refNames;
		this.subject = subject;
		this.body = body;
		this.notes = notes;
		this.changedFiles = changedFiles;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash
	 *            the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the authorEmail
	 */
	public String getAuthorEmail() {
		return authorEmail;
	}

	/**
	 * @param authorEmail
	 *            the authorEmail to set
	 */
	public void setAuthorEmail(String authorEmail) {
		this.authorEmail = authorEmail;
	}

	/**
	 * @return the authorDate
	 */
	public Date getAuthorDate() {
		return authorDate;
	}

	/**
	 * @param authorDate
	 *            the authorDate to set
	 */
	public void setAuthorDate(Date authorDate) {
		this.authorDate = authorDate;
	}

	/**
	 * @return the committer
	 */
	public String getCommitter() {
		return committer;
	}

	/**
	 * @param committer
	 *            the committer to set
	 */
	public void setCommitter(String committer) {
		this.committer = committer;
	}

	/**
	 * @return the committerEmail
	 */
	public String getCommitterEmail() {
		return committerEmail;
	}

	/**
	 * @param committerEmail
	 *            the committerEmail to set
	 */
	public void setCommitterEmail(String committerEmail) {
		this.committerEmail = committerEmail;
	}

	/**
	 * @return the committerDate
	 */
	public Date getCommitterDate() {
		return committerDate;
	}

	/**
	 * @param committerDate
	 *            the committerDate to set
	 */
	public void setCommitterDate(Date committerDate) {
		this.committerDate = committerDate;
	}

	/**
	 * @return the refNames
	 */
	public String getRefNames() {
		return refNames;
	}

	/**
	 * @param refNames
	 *            the refNames to set
	 */
	public void setRefNames(String refNames) {
		this.refNames = refNames;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the changedFiles
	 */
	public GitCommitChangedFile[] getChangedFiles() {
		return changedFiles;
	}

	/**
	 * @param changedFiles
	 *            the changedFiles to set
	 */
	public void setChangedFiles(GitCommitChangedFile[] changedFiles) {
		this.changedFiles = changedFiles;
	}

	public String toString() {
		return "{ 'GitCommit' : '" + hash + "' }";
	}

}
