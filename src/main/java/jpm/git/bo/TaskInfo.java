package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import jpm.git.tasks.TaskStatus;

@XmlType(name = "TaskInfo", propOrder = { "key", "status", "gitRepository" })
@XmlRootElement(name = "TaskInfo")
@XmlAccessorType(XmlAccessType.NONE)
public class TaskInfo {
	
	@XmlElement
	protected GitRepository gitRepository;
	
	@XmlElement
	protected String key;
	
	@XmlElement
	protected TaskStatus status;
	
	/**
	 * Constructor por defecto
	 */
	public TaskInfo() {
	}
	
	/**
	 * Constructor por estado
	 */
	public TaskInfo(String key, TaskStatus status, GitRepository gitRepository) {
		this.key = key;
		this.status = status;
		this.gitRepository = gitRepository;
	}
	
	/**
	 * @return the gitRepository
	 */
	public GitRepository getGitRepository() {
		return gitRepository;
	}

	/**
	 * @param gitRepository the gitRepository to set
	 */
	public void setGitRepository(GitRepository gitRepository) {
		this.gitRepository = gitRepository;
	}

	/**
	 * {@link #key}
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * {@link #key}
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * {@link #status}
	 * @return
	 */
	public TaskStatus getAsyncTaskStatus() {
		return status;
	}

	/**
	 * {@link #status}
	 */
	public void setAsyncTaskStatus(TaskStatus status) {
		this.status = status;
	}
	
	

}
