package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase para albergar información del ejecutable git del sistema
 * 
 * @author j.penalba
 *
 */
@XmlType(name = "GitInfo")
@XmlRootElement(name = "GitInfo")
@XmlAccessorType(XmlAccessType.NONE)
public class GitInfo {

	/**
	 * Default constructor
	 */
	public GitInfo() {
	}
	
	/**
	 * State constructor
	 * @param gitLocation
	 * @param versionOutput
	 */
	public GitInfo(final String gitLocation, final String versionOutput, final String error) {
		this.gitLocation = gitLocation;
		this.versionOutput = versionOutput;
		this.error = error;
	}

	/**
	 * Localización del ejecutable git
	 */
	@XmlElement
	protected String gitLocation;
	/**
	 * Salida del comando "git --version"
	 */
	@XmlElement
	protected String versionOutput;
	/**
	 * Descripción del error en caso de que el ejecutable no esté disponible
	 */
	@XmlElement
	protected String error;

	/**
	 * @return the gitLocation
	 */
	public String getGitLocation() {
		return gitLocation;
	}

	/**
	 * @param gitLocation
	 *            the gitLocation to set
	 */
	public void setGitLocation(String gitLocation) {
		this.gitLocation = gitLocation;
	}

	/**
	 * @return the versionOutput
	 */
	public String getVersionOutput() {
		return versionOutput;
	}

	/**
	 * @param versionOutput
	 *            the versionOutput to set
	 */
	public void setVersionOutput(String versionOutput) {
		this.versionOutput = versionOutput;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

}
