package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import jpm.git.config.Property;

@XmlType(name = "PropertyBO")
@XmlRootElement(name = "PropertyBO")
@XmlAccessorType(XmlAccessType.NONE)
public class PropertyBO {

	@XmlElement(name = "id")
	protected String key;
	@XmlElement(name = "value")
	protected String value;

	/**
	 * Default constructor
	 */
	public PropertyBO() {
	}

	/**
	 * State constructor
	 * 
	 * @param key
	 * @param value
	 */
	public PropertyBO(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public static PropertyBO fromProperty(Property p) {
		return p == null ? null : new PropertyBO(p.getKey(), p.getValue());
	}

}
