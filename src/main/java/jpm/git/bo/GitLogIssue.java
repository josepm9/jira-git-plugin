package jpm.git.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilizada para encapsular ua lista de "commits" obtenida de filtrar en un repositorio
 * todos los commits relacionados con una incidencia en Jira {@link #issueKey}
 * @author José
 *
 */
@XmlType(name = "GitLogIssue", propOrder = { "issueKey", "commits", "gitRepository" })
@XmlRootElement(name = "GitLogIssue")
@XmlAccessorType(XmlAccessType.NONE)
public class GitLogIssue {
	
	@XmlElement
	protected GitRepository gitRepository;
	
	/**
	 * Clave de la incidencia utilizada para filtrar commits
	 */
	@XmlElement
	protected String issueKey;
	
	/**
	 * Lista de commits obtenidos relacionados con la incidencia indicada {@link #issueKey}
	 */
	@XmlElement(name="commit")
	@XmlElementWrapper(name="commits")
	protected GitCommit[] commits;
	
	/**
	 * Default constructor
	 */
	public GitLogIssue() {
	}
	
	/**
	 * State constructor
	 * @param gitRepository
	 * @param issueKey
	 * @param commits
	 */
	public GitLogIssue(GitRepository gitRepository, String issueKey, GitCommit[] commits) {
		super();
		this.gitRepository = gitRepository;
		this.issueKey = issueKey;
		this.commits = commits;
	}



	/**
	 * @return the gitRepository
	 */
	public GitRepository getGitRepository() {
		return gitRepository;
	}

	/**
	 * @param gitRepository the gitRepository to set
	 */
	public void setGitRepository(GitRepository gitRepository) {
		this.gitRepository = gitRepository;
	}

	/**
	 * {@link #issueKey}
	 * @return
	 */
	public String getIssueKey() {
		return issueKey;
	}

	/**
	 * {@link #issueKey}
	 */
	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}

	/**
	 * {@link #commits}
	 * @return
	 */
	public GitCommit[] getCommits() {
		return commits;
	}

	/**
	 * {@link #commits}
	 */
	public void setCommits(GitCommit[] commits) {
		this.commits = commits;
	}
	
	

}
