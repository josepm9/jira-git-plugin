package jpm.git.statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jpm.git.bo.GadgetDataRow;
import jpm.git.bo.GadgetDataTable;
import jpm.git.bo.GitCommit;

/**
 * Clase de apoyo para generar una estadística de commits (o ficheros
 * modificados)
 * 
 * @author José
 *
 */
public class CommitsStatistic {

	/**
	 * Agrupar los resultados de todos los repositorios del proyecto. En caso
	 * negativo, se generará una columna "repository" que será la utilizada en
	 * primer lugar para ordenar.
	 */
	protected final boolean groupRepositories;
	/**
	 * Entidad que se contará para generar la estadística. Admite los valores:
	 * <br/>
	 * <ol type="1">
	 * <li>Commits</li>
	 * <li>Ficheros modificados (suma de todos los ficheros modificados en todos
	 * los commits)</li>
	 * </ol>
	 */
	protected final int entity;
	/**
	 * Dato utilizado para agrupar resultados. Admite los valores: <br/>
	 * <ol type="1">
	 * <li>Autor</li>
	 * <li>Repositorio (sólo tiene sentido si no se agrupa los resultados de los
	 * distintos repositorios: {@link #groupRepositories} == false</li>
	 * </ol>
	 */
	protected final int groupBy;
	/**
	 * Tipo de gráfica a generar. Admite los valores: <br/>
	 * <ol type="1">
	 * <li>Table</li>
	 * <li>Tarta</li>
	 * <li>Barras</li>
	 * </ol>
	 */
	protected final int type;

	protected Map<String, Map<String, GadgetDataRow>> data = new HashMap<>();
	protected Map<String, GadgetDataRow> groupedData = new HashMap<>();

	public CommitsStatistic(boolean groupRepositories, int entity, int groupBy, int type) {
		this.groupRepositories = groupRepositories;
		this.entity = entity;
		this.groupBy = groupBy;
		this.type = type;
	}

	protected String[] getColumns() {
		final List<String> columns = new ArrayList<>();
		if (!groupRepositories) {
			columns.add("repository");
		}
		if (groupBy == 1) {
			columns.add("author-email");
		}
		columns.add(entity == 1 ? "total-commits" : "total-files-modified");
		return columns.toArray(new String[columns.size()]);
	}

	/**
	 * Acumula el commit pasado como argumento a la tabla de resultados
	 * 
	 * @param commit
	 */
	public void compute(final String repositoryKey, final GitCommit commit) {
		// Get repository
		Map<String, GadgetDataRow> rowsData;
		if (groupRepositories) {
			rowsData = groupedData;
		} else {
			rowsData = data.get(repositoryKey);
			if (rowsData == null) {
				rowsData = new HashMap<>();
				data.put(repositoryKey, rowsData);
			}
		}
		final String rowKey = groupBy == 1 ? commit.getAuthorEmail() : repositoryKey;
		GadgetDataRow rowData = rowsData.get(rowKey);
		if (rowData == null) {
			rowData = new GadgetDataRow();
			final List<Object> cells = new ArrayList<>();
			if (!groupRepositories) {
				cells.add(repositoryKey);
			}
			if (groupBy == 1) {
				cells.add(rowKey);
			}
			cells.add(0);
			rowData.setCells(cells.toArray(new Object[cells.size()]));
			rowsData.put(rowKey, rowData);
		}
		rowData.inc(entity == 1 ? 1 : commit.getChangedFiles().length);
	}

	/**
	 * Finaliza el cálculo y devuelve la tabla de datos
	 * 
	 * @return
	 */
	public GadgetDataTable getDataTable() {
		final GadgetDataTable dataTable = new GadgetDataTable();
		dataTable.setColumns(getColumns());
		List<GadgetDataRow> rows = new ArrayList<>();
		if (groupRepositories) {
			rows.addAll(groupedData.values());
		} else {
			for (Map<String, GadgetDataRow> rowsData : data.values()) {
				rows.addAll(rowsData.values());
			}
		}
		dataTable.setRows(rows.toArray(new GadgetDataRow[rows.size()]));
		return dataTable;
	}
}
