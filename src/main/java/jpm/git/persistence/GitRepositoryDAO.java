package jpm.git.persistence;

import jpm.git.bo.GitRepository;

public interface GitRepositoryDAO {

	/**
	 * Insertar un nuevo repositorio en el registro de repositorios
	 * @param repo
	 */
	public void insertRepository(GitRepository repo) throws DaoException;

	/**
	 * Obtener la lista de repositorios del registro de repositorios
	 * @param repo
	 */
	public GitRepository[] getRepositories() throws DaoException;

	/**
	 * Obtener la lista de repositorios de un proyecto en el registro de repositorios
	 * @param repo
	 */
	public GitRepository[] getRepositories(String projectKey) throws DaoException;

	/**
	 * Obtener los datos del repositorio del registro de repositorios
	 * @param projectKey
	 * @param repositoryKey
	 * @return
	 */
	public GitRepository getRepository(String projectKey, String repositoryKey) throws DaoException;

	/**
	 * Actualizar los datos de un repositorio del registro de repositorios
	 * @param repo
	 */
	public void updateRepository(GitRepository repo) throws DaoException;

	/**
	 * Borrar un repositorio del registro de repositorios
	 * @param repo
	 * @return
	 */
	public GitRepository deleteRepository(GitRepository repo) throws DaoException;

}
