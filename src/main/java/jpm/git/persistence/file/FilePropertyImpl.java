package jpm.git.persistence.file;

import jpm.git.config.Property;

public class FilePropertyImpl implements Property {
	
	protected String key;
	protected String value;
	
	public FilePropertyImpl(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

}
