package jpm.git.persistence.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpm.git.persistence.PropertyDao;

public class FilePropertyDaoImpl implements PropertyDao<FilePropertyImpl> {

	final static Logger LOG = LoggerFactory.getLogger(FilePropertyDaoImpl.class);

	final File propertiesFile;
	long fileTS = 0;

	public FilePropertyDaoImpl(final File propertiesFile) {
		this.propertiesFile = propertiesFile;
	}

	final Properties props = new Properties();

	@Override
	public FilePropertyImpl createProperty(String key, String value) {
		load();
		return doCreateProperty(key, value);
	}

	protected FilePropertyImpl doCreateProperty(String key, String value) {
		final FilePropertyImpl p = new FilePropertyImpl(key, value);
		if (value == null) {
			LOG.debug("Removing property '" + key + "' (received null value)");
			props.remove(key);
		} else {
			LOG.debug("Storing property '" + key + "' with value: " + value);
			props.put(key, value);
		}
		store();
		return p;
	}

	@Override
	public FilePropertyImpl[] readProperties(String keyPrefix) {
		load();
		return doReadProperties(keyPrefix);
	}

	protected FilePropertyImpl[] doReadProperties(String keyPrefix) {
		final List<FilePropertyImpl> l = new ArrayList<>();
		for (Entry<Object, Object> entry : props.entrySet()) {
			if (((String) entry.getKey()).startsWith(keyPrefix)) {
				l.add(new FilePropertyImpl((String) entry.getValue(), (String) entry.getValue()));
			}
		}
		LOG.debug(l.size() + " properties read with key starting with '" + keyPrefix + "'");
		return l.toArray(new FilePropertyImpl[l.size()]);
	}

	@Override
	public FilePropertyImpl readProperty(String key) {
		load();
		return doReadProperty(key);
	}

	protected FilePropertyImpl doReadProperty(String key) {
		final String value = props.getProperty(key);
		LOG.debug("Property '" + key + "' read with value: " + value);
		return value == null ? null : new FilePropertyImpl(key, value);
	}

	@Override
	public FilePropertyImpl readProperty(String key, String defaultValue) {
		load();
		return doReadProperty(key, defaultValue);
	}

	protected FilePropertyImpl doReadProperty(String key, String defaultValue) {
		final FilePropertyImpl p = doReadProperty(key);
		if (p == null) {
			LOG.debug("Property '" + key + "' read with null value, create with default: " + defaultValue);
			return doCreateProperty(key, defaultValue);
		} else {
			return p;
		}
	}

	@Override
	public FilePropertyImpl updateProperty(String key, String value) {
		load();
		return doUpdateProperty(key, value);
	}

	protected FilePropertyImpl doUpdateProperty(String key, String value) {
		final FilePropertyImpl p = doReadProperty(key);
		if (p != null) {
			if (value == null) {
				LOG.debug("Property '" + key + "' updated to null: removing");
				props.remove(key);
			} else {
				LOG.debug("Property '" + key + "' updated to: " + value);
				props.setProperty(key, value);
			}
			store();
			p.setValue(value);
		}
		return p;
	}

	@Override
	public FilePropertyImpl updateProperty(FilePropertyImpl p) {
		load();
		return doUpdateProperty(p);
	}

	protected FilePropertyImpl doUpdateProperty(FilePropertyImpl p) {
		if (p.value == null) {
			LOG.debug("Property '" + p.key + "' updated to null: removing");
			props.remove(p.key);
		} else {
			LOG.debug("Property '" + p.key + "' updated to: " + p.value);
			props.setProperty(p.key, p.value);
		}
		store();
		return p;
	}

	@Override
	public FilePropertyImpl upsertProperty(String key, String value) {
		load();
		return doUpsertProperty(key, value);
	}

	protected FilePropertyImpl doUpsertProperty(String key, String value) {
		final FilePropertyImpl p = doReadProperty(key);
		if (p == null) {
			return doCreateProperty(key, value);
		} else {
			p.value = value;
			return doUpdateProperty(p);
		}
	}

	@Override
	public FilePropertyImpl deleteProperty(String key) {
		load();
		return doDeleteProperty(key);
	}

	protected FilePropertyImpl doDeleteProperty(String key) {
		final FilePropertyImpl p = doReadProperty(key);
		if (p != null) {
			LOG.debug("Deleting property '" + p.key + "'");
			props.remove(p.getKey());
			store();
		}
		return p;
	}

	@Override
	public void deleteProperty(FilePropertyImpl p) {
		load();
		doDeleteProperty(p);
	}

	protected void doDeleteProperty(FilePropertyImpl p) {
		props.remove(p.getKey());
		store();
	}

	// //////////////////////////////////
	// PERSISTENCE

	protected void load() {
		synchronized (propertiesFile) {
			if (propertiesFile.exists()) {
				final long ts = propertiesFile.lastModified();
				if (ts != fileTS) {
					LOG.info(fileTS == 0 ? "Loading properties first time" : "Reading updated properties");
					props.clear();
					InputStream is = null;
					try {
						is = new FileInputStream(propertiesFile);
						props.load(is);
					} catch (Exception e) {
						LOG.error("Could not load properties: " + e.getMessage());
						throw new RuntimeException("Could not load properties: " + e.getMessage(), e);
					} finally {
						if (is != null) {
							try {
								is.close();
							} catch (Exception e) {
							}
						}
					}
					fileTS = ts;
				}
			}
		}
	}

	protected void store() {
		synchronized (propertiesFile) {
			RandomAccessFile raf = null;
			OutputStream os = null;
			try {
				LOG.info("Storing properties");
				raf = new RandomAccessFile(propertiesFile, "rw");
				raf.setLength(0l);
				raf.close();
				raf = null;
				os = new FileOutputStream(propertiesFile);
				props.store(os, "");
				os.close();
				os = null;
				fileTS = propertiesFile.lastModified();
			} catch (Exception e) {
				LOG.error("Could not store properties: " + e.getMessage());
				throw new RuntimeException("Could not store properties: " + e.getMessage(), e);
			} finally {
				if (raf != null) {
					try {
						raf.close();
					} catch (Exception e) {
					}
				}
				if (os != null) {
					try {
						os.close();
					} catch (Exception e) {
					}
				}
			}

		}
	}
}
