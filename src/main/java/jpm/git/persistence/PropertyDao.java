package jpm.git.persistence;

import java.sql.SQLException;

import jpm.git.config.Property;

public interface PropertyDao<T extends Property> {

	/**
	 * Crear una nueva propiedad
	 * 
	 * @param key
	 * @param value
	 * @return
	 * @throws SQLException
	 */
	T createProperty(String key, String value);

	/**
	 * Obtener una lista de propiedades que tienen un determinado prefijo en su clave
	 * @param keyPrefix
	 * @return
	 * @throws SQLException
	 */
	T[] readProperties(String keyPrefix);

	/**
	 * Obtener una propiedad
	 * @param key
	 * @return
	 * @throws SQLException
	 */
	T readProperty(String key);

	/**
	 * Obtener una propiedad. Si no existe, se crea con un valor por defecto.
	 * @param key
	 * @param defaultValue
	 * @return
	 * @throws SQLException
	 */
	T readProperty(String key, String defaultValue);

	/**
	 * Actualizar una propiedad.
	 * @param key
	 * @param value
	 * @return la propiedad actualizada o null si no existe
	 * @throws SQLException
	 */
	T updateProperty(String key, String value);

	/**
	 * Actualiza el valor de una propiedad
	 * @param p
	 * @return
	 * @throws SQLException
	 */
	T updateProperty(T p);

	/**
	 * Actualiza el valor de una propiedad, se inserta si no existe
	 * @param key
	 * @param value
	 * @return
	 * @throws SQLException
	 */
	T upsertProperty(String key, String value);

	/**
	 * Eliminar una propiedad
	 * @param key
	 * @return
	 * @throws SQLException
	 */
	T deleteProperty(String key);

	/**
	 * Eliminar una propiedad
	 * @param p
	 * @throws SQLException
	 */
	void deleteProperty(T p);

}