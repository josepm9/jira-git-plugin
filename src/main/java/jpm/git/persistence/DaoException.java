package jpm.git.persistence;

public class DaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3549011000483678442L;

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

}
