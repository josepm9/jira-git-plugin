package jpm.git.persistence;

import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;

public class JsonMapper extends ObjectMapper {
	
	protected static JsonMapper INSTANCE = new JsonMapper();
	
	public static JsonMapper getInstance() {
		return INSTANCE;
	}

	protected JsonMapper() {
		final AnnotationIntrospector introspector = new JacksonAnnotationIntrospector();
		getDeserializationConfig().withAnnotationIntrospector(introspector);
		getSerializationConfig().withAnnotationIntrospector(introspector);
	}
	
}
