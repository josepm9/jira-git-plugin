package jpm.git.persistence.properties;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import jpm.git.bo.GitRepository;
import jpm.git.config.Property;
import jpm.git.persistence.DaoException;
import jpm.git.persistence.GitRepositoryDAO;
import jpm.git.persistence.JsonMapper;
import jpm.git.persistence.PropertyDao;

public class PropGitRepositoryDAOImpl implements GitRepositoryDAO {

	final JsonMapper mapper = JsonMapper.getInstance();

	final PropertyDao<? extends Property> propertyDao;

	private static final String PROJECT_PREFIX = "P/";
	private static final String REPO_SEPARATOR = "/";

	public PropGitRepositoryDAOImpl(final PropertyDao<? extends Property> propertyDao) {
		this.propertyDao = propertyDao;
	}

	/**
	 * Obtener la clave de la propiedad en la que se almacena los datos del
	 * repositorio
	 * 
	 * @param repo
	 * @return
	 */
	private String getRepoPropKey(GitRepository repo) {
		return PROJECT_PREFIX + repo.getProjectKey() + REPO_SEPARATOR + repo.getRepoKey();
	}

	/**
	 * Obtener la clave de la propiedad en la que se almacena los datos del
	 * repositorio
	 * 
	 * @param repo
	 * @param repoKey
	 * @return
	 */
	private String getRepoPropKey(String prjKey, String repoKey) {
		return PROJECT_PREFIX + prjKey + REPO_SEPARATOR + repoKey;
	}

	/**
	 * Crear una cadena JSON que represente los datos de un repositorio
	 * 
	 * @param repo
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private String writeRepoAsString(GitRepository repo)
			throws JsonGenerationException, JsonMappingException, IOException {
		final byte[] bytes = mapper.writeValueAsBytes(repo);
		return new String(bytes, "UTF-8");
	}

	/**
	 * Interpretar una cadena JSON que contiene la representación de los datos
	 * de un repositorio
	 * 
	 * @param value
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private GitRepository readRepoFromString(final String value)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(value, GitRepository.class);
	}

	@Override
	public void insertRepository(GitRepository repo) throws DaoException {
		try {
			propertyDao.createProperty(getRepoPropKey(repo), writeRepoAsString(repo));
		} catch (Exception e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public GitRepository[] getRepositories() throws DaoException {
		try {
			final Property[] props = propertyDao.readProperties(PROJECT_PREFIX);
			if (props != null) {
				final GitRepository[] repos = new GitRepository[props.length];
				for (int i = 0; i < repos.length; ++i) {
					repos[i] = readRepoFromString(props[i].getValue());
				}
				return repos;
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public GitRepository[] getRepositories(String projectKey) throws DaoException {
		try {
			final Property[] props = propertyDao.readProperties(PROJECT_PREFIX + projectKey);
			if (props != null) {
				final GitRepository[] repos = new GitRepository[props.length];
				for (int i = 0; i < repos.length; ++i) {
					repos[i] = readRepoFromString(props[i].getValue());
				}
				return repos;
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public GitRepository getRepository(String projectKey, String repositoryKey) throws DaoException {
		try {
			final Property prop = propertyDao.readProperty(getRepoPropKey(projectKey, repositoryKey));
			return prop == null ? null : readRepoFromString(prop.getValue());
		} catch (Exception e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public void updateRepository(GitRepository repo) throws DaoException {
		try {
			propertyDao.updateProperty(getRepoPropKey(repo), writeRepoAsString(repo));
		} catch (Exception e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public GitRepository deleteRepository(GitRepository repo) throws DaoException {
		try {
			final Property p = propertyDao.deleteProperty(getRepoPropKey(repo));
			return p == null ? null : readRepoFromString(p.getValue());
		} catch (Exception e) {
			throw new DaoException(e.getMessage(), e);
		}
	}

}
