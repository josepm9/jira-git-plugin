var JpgChart = {};

JpgChart.throwAbstractException = function() {
	throw "This method can not be invoked directly (abstract)";
};

// Base graph

JpgChart.AbstractGraphRenderer = function(t_) {
	t_.renderChart_ = function(chartData, jqContainer, repo) {
		if (repo) {
			var h1 = AJS.$('<h1></h1>');
			h1.text(repo);
			jqContainer.append(h1)
		}
		var px = jqContainer.css('width');
		var myChart = AJS.$('<canvas width="' + px + '" height="' + px + '"></canvas>');
		jqContainer.append(myChart);
		// Base object call
		t_.doRenderOne_(chartData, myChart, {});
	};
	
	t_.render = function(data, jqContainer) { JpgChart.throwAbstractException(); };

	t_.doRenderOne_ = function (chartData, myChart, options) { JpgChart.throwAbstractException(); };
	
	return t_;
}

// Circular based graphs

JpgChart.AbstractCircularGraphRenderer = function(t_) {
	t_ = JpgChart.AbstractGraphRenderer(t_);

	t_.render = function(data, jqContainer) {
		jqContainer.empty();
		if (data.columns.length==3) {
			// Multiple repositories
			var repo = null;
			var chartData = [];
			for (var i = 0; i < data.rows.length; ++i) {
				var rowRepo = data.rows[i].cells[0];
				if (repo && repo!=rowRepo) {
					t_.renderChart_(chartData, jqContainer, repo);
					chartData = [];
				}
				repo = rowRepo;
				chartData.push({
					label : data.rows[i].cells[1],
					value : data.rows[i].cells[2]
				});
			}
			if (chartData.length>0) {
				t_.renderChart_(chartData, jqContainer, repo);
			}
		}
		else {
			// One repository
			var chartData = [];
			for (var i = 0; i < data.rows.length; ++i) {
				chartData.push({
					label : data.rows[i].cells[0],
					value : data.rows[i].cells[1]
				});
			}
			t_.renderChart_(chartData, jqContainer, null);
		};	
	};
	
	return t_;
};

JpgChart.PolarGraphRenderer = function() {
	var t_ = JpgChart.AbstractCircularGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart, options) {
		var ctx = myChart[0].getContext("2d");
		return new Chart(ctx).PolarArea(chartData,options);
	}
	return t_;
};

JpgChart.PieGraphRenderer = function() {
	var t_ = JpgChart.AbstractCircularGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart,options) {
		var ctx = myChart[0].getContext("2d");
		return new Chart(ctx).Pie(chartData,options);
	}
	return t_;
};

JpgChart.DoughnutGraphRenderer = function() {
	var t_ = JpgChart.AbstractCircularGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart,options) {
		var ctx = myChart[0].getContext("2d");
		return new Chart(ctx).Doughnut(chartData,options);
	}
	return t_;
};

// Series based graphs

JpgChart.AbstractSeriesGraphRenderer = function(t_) {
	t_ = JpgChart.AbstractGraphRenderer(t_);
	
	var objectSize_ = function(obj) {
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};

	var objectKeys_ = function(obj) {
	    var arr = [], key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) arr.push(key);
	    }
	    return arr;
	};

	t_.render = function(data, jqContainer) {
		jqContainer.empty();
		if (data.columns.length==3) {
			// Multiple repositories
			var labels = {};
			var seriesMap = {};
			var serie = {}; // link data to label in each repo
			var repo = null;
			for (var i = 0; i < data.rows.length; ++i) {
				var row = data.rows[i];
				var rowRepo = row.cells[0];
				if (repo && repo!=rowRepo) {
					seriesMap[repo] = serie;
					serie = {};
				}
				repo = rowRepo;
				
				var label = row.cells[1];
				if (!(label in labels)) {
					labels[label] = 1;
				}
				serie[label] = row.cells[2];
			}
			if (objectSize_(serie)>0) {
				seriesMap[repo] = serie;
			}
			// Transform labels & seriesMap
			var repoKeys = objectKeys_(seriesMap);
			labels = objectKeys_(labels);
			var chartData = {
				labels : labels,
				datasets : []
			};
			for (var i = 0;i<repoKeys.length;++i) {
				var repo = repoKeys[i];
				var serie = seriesMap[repo];
				var ds = {
    				label: repo,
		            fillColor: "rgba(220,220,220,0.5)",
		            strokeColor: "rgba(220,220,220,0.8)",
		            highlightFill: "rgba(220,220,220,0.75)",
		            highlightStroke: "rgba(220,220,220,1)",
    				data : []
    			};
    			for (var j=0;j<labels.length;++j) {
    				var label = labels[j];
    				ds.data.push( (label in serie) ? serie[label]  : 0 );
    			}
    			chartData.datasets.push(ds);
			}
			t_.renderChart_(chartData, jqContainer, null);
		}
		else {
			// One repository
			var labels = [];
			var dsData = [];
			for (var i = 0; i < data.rows.length; ++i) {
				labels.push(data.rows[i].cells[0]);
				dsData.push(data.rows[i].cells[1]);
			}
			var chartData = {
				labels : labels,
				datasets: [
        			{
        				label: "",
			            fillColor: "rgba(220,220,220,0.5)",
			            strokeColor: "rgba(220,220,220,0.8)",
			            highlightFill: "rgba(220,220,220,0.75)",
			            highlightStroke: "rgba(220,220,220,1)",
        				data : dsData
        			}
        		]
			};
			t_.renderChart_(chartData, jqContainer, null);
		};	
	};
	
	return t_;
};

JpgChart.LineGraphRenderer = function() {
	var t_ = JpgChart.AbstractSeriesGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart,options) {
		var ctx = myChart[0].getContext("2d");
		return new Chart(ctx).Line(chartData, options);
	}
	return t_;
};

JpgChart.BarGraphRenderer = function() {
	var t_ = JpgChart.AbstractSeriesGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart,options) {
		var ctx = myChart[0].getContext("2d");
		return new Chart(ctx).Bar(chartData, options);
	}
	return t_;
};

JpgChart.RadarGraphRenderer = function() {
	var t_ = JpgChart.AbstractSeriesGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart,options) {
		var ctx = myChart[0].getContext("2d");
		return new Chart(ctx).Radar(chartData, options);
	}
	return t_;
};

JpgChart.TableGraphRenderer = function() {
	var t_ = JpgChart.AbstractSeriesGraphRenderer(this);
	t_.doRenderOne_ = function (chartData, myChart, options) {
		myChart = myChart.parent();
		myChart.empty();
		if (chartData.datasets.length>0) {
			// Create table
			var tbl = AJS.$('<table class="aui"><thead></thead><tbody></tbody></table>');
			// Cabeceras
			var headerRow = AJS.$('<tr><th id="repo"></th></tr>');
			for (var j = 0; j < chartData.labels.length; ++j) {
				var headerCell = AJS.$('<th></th>');
				headerCell.attr("id", chartData.labels[j]).text(chartData.labels[j]);
				headerRow.append(headerCell);
			}
			tbl.find('thead').append(headerRow);
			// Filas de datos
			for (var i = 0; i < chartData.datasets.length; ++i) {
				var dataRow = AJS.$('<tr></tr>');
				var serie = chartData.datasets[i];
				var cell = AJS.$('<td headers="repo"></td>');
				cell.text(serie.label);
				dataRow.append(cell);
				for (var j = 0; j < serie.data.length; ++j) {
					cell = AJS.$('<td></td>');
					cell.attr("headers", chartData.labels[j]).text(serie.data[j]);
					dataRow.append(cell);
				}
				tbl.find('tbody').append(dataRow);
			}
			// Done
			myChart.append(tbl);
		}
	}
	return t_;
};

// Factory

JpgChart.GraphRendererFactory = {
	create : function(type) {
		console.log("GraphRendererFactory.create " + type);
		if (type==1) {
			return new JpgChart.TableGraphRenderer();
		}
		else if (type==2) {
			return new JpgChart.PieGraphRenderer();
		}
		else if (type==3) {
			return new JpgChart.DoughnutGraphRenderer();
		}
		else if (type==4) {
			return new JpgChart.PolarGraphRenderer();
		}
		else if (type==5) {
			return new JpgChart.LineGraphRenderer();
		}
		else if (type==6) {
			return new JpgChart.BarGraphRenderer();
		}
		else if (type==7) {
			return new JpgChart.RadarGraphRenderer();
		}
		else {
			throw "GraphRendererFactory: unknown graph type " + type;
		}
	}
};

// Helper

var jgp_commitsStatisticsGadgetHelper = new function() {
	var t_ = this;
	var gadget_ = null;
	var gadgetData_ = null;
	
	var showGraph_ = function(data, strTextStatus, jqXHR) {
		// Crear mensaje
		var dlgContentMsg = AJS.$('<div class="aui-message shadowed info"></div>');
		dlgContentMsg.append(AJS.$('<span class="aui-icon aui-icon-small icon-info"></span>'));
		AJS.$('<div style="white-space: pre-wrap"></div>').appendTo(dlgContentMsg).text(JSON.stringify(gadgetData_) + 
			'\n-\n' + JSON.stringify(data));
		// Mostrar
		try{
			JpgChart.GraphRendererFactory.create(gadgetData_.type).render(data, gadget_.getView());
		}
		catch(err) {
			showErrorMessage_(err.message?err.message:err);
		}
	}

	var showError_ = function(jqXHR, strTextStatus, strErrorThrown) {
		// Mensaje a mostrar
		var msg;
		try{
			var data = JSON.parse(jqXHR.responseText);
			msg = "Error código " + data.code + '\n' + data.description;
		}catch(e){
			msg = 'Error interno (HTTP Status ' + jqXHR.status + ': ' + strErrorThrown + ')';
		}
		gadget_.getView().empty();
		showErrorMessage_(msg);
	};

	var showErrorMessage_ = function(msg) {
		var dlgContentMsg = AJS.$('<div class="aui-message shadowed error"></div>');
		dlgContentMsg.append(AJS.$('<span class="aui-icon aui-icon-small icon-error"></span>'));
		AJS.$('<div style="white-space: pre-wrap"></div>').appendTo(dlgContentMsg).text(msg);
		// Mostrar
		gadget_.getView().append(dlgContentMsg);
	};
	
	t_.create = function(gadget, gadgetData) {
		gadget_ = gadget;
		gadgetData_ = gadgetData;
		// gadget.getView()
		// gatget.baseUrl
		// gadgetData: project, repository, entity, group, type
		var spinner = AJS.$('<span class="aui-icon aui-icon-wait" style="display:block;margin:auto;">Loading...</span>');
		gadget.getView().empty().append(spinner);
		
		gadgets.window.setTitle( "[" + gadgetData.project + "]: " + (gadgetData.entity==1?"Commits":"Files modified") + 
			" by " + (gadgetData.group==1?"Author":"Repository") );
		
	    AJS.$.ajax(
	    	{
	    		"url" : gadget.getBaseUrl() + "/rest/gitplugin/1.0/gadgets/commits-statistics",
	    		"method" : "GET",
	    		"type" : "GET",
	    		"data" : {
	    			"project" : gadgetData.project,
	    			"repository" : gadgetData.repository,
	    			"entity" : gadgetData.entity,
	    			"group" : gadgetData.group,
	    			"type" : gadgetData.type
	    		},
	    		"success" : showGraph_,
	    		"error" : showError_
	    	}
	    );
		
	};
	
	return t_;
}();