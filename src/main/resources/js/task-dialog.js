var TaskDialog = function(container, pluginPrjAdminSvcURL, onClose) {
	var t$ = this;
	t$.pluginPrjAdminSvcURL = pluginPrjAdminSvcURL;
	t$.onClose = onClose;
	
	// /////////////////////////////////////////////////////////
	// DIALOGO DE PROGRESO
	// Global data
	t$._taskInfo = null;
	t$._container = container;
	// Se inician en _createDialog
	t$._ajsDialog = null;
	t$._dlgSection = null;
	t$._outputDiv = null;
	t$._headerElem = null;
	t$._spinElem = null;
	// Functions
	t$.showTask = function(operation, taskInfo) {
		t$._taskInfo = taskInfo;
		// Crear diálogo
		t$._createDialog("xlarge");
		t$._headerElem.text(operation + " " + taskInfo.gitRepository.url);
		t$._ajsDialog.show();
		t$._handleQueryProgressSuccess(taskInfo.status);
	};
	t$._scheduleQueryProgress = function() {
		setTimeout(t$._queryProgress(), 100);
	};
	t$._queryProgress = function() {
	    AJS.$.ajax(
	    	t$.pluginPrjAdminSvcURL+"/projects/" + t$._taskInfo.gitRepository.projectKey + 
	    		"/task/" + t$._taskInfo.key,
	    	{
	    		"method" : "GET",
	    		"type" : "GET",
	    		"success" : function(data, strTextStatus, jqXHR) {
					t$._handleQueryProgressSuccess(data);
	    		},
	    		"error" : function(jqXHR, strTextStatus, strErrorThrown) {
	    			t$._ajsDialog.hide();
	    			t$.showAjaxError(jqXHR, strTextStatus, strErrorThrown);
	    		}
	    	}
	    );
	};
	t$._updateOutputDivStyle = function(style) {
		t$._outputDiv.prev().removeClass("icon-info").addClass("icon-" + style);
		t$._outputDiv.parent().removeClass("info").addClass(style);
//		if (style=="error") {
//			t$._dlgSection.addClass("aui-dialog2-warning");
//		}
		
	};
	t$._handleQueryProgressSuccess = function(status) {
		// Tenemos un proceso en marcha
		t$._taskInfo.status = status;
		var taskState = status.state;
		var newOutput = status.newOutput;
		// Actualizar mensaje
		if (newOutput) {
			t$._outputDiv.append(document.createTextNode(newOutput));
		};
		// En función del estado
		if (taskState==1) {
			// Tarea encolada
			t$._scheduleQueryProgress();
		}
		else if (taskState==2) {
			// El arranque de la tarea falló
			t$._spinElem.spinStop();
			t$._updateOutputDivStyle( "error");
		}
		else if (taskState==3) {
			// Tarea en ejecución
			t$._scheduleQueryProgress();
		}
		else if (taskState==4) {
			// Ejecución finalizada OK
			t$._spinElem.spinStop();
			t$._updateOutputDivStyle( "success");
		}
		else if (taskState==5) {
			// Ejecución finalizada KO
			t$._spinElem.spinStop();
			t$._updateOutputDivStyle( "error");
		}
		else { // ERROR, ESTADO NO CONTROLADO
			t$._spinElem.spinStop();
			t$._updateOutputDivStyle( "error");
		}
	};
	
	// /////////////////////////////////////////////////////////
	// DIALOGO DE ERROR
	t$.showAjaxError = function(jqXHR, strTextStatus, strErrorThrown) {
		// Mensaje a mostrar
		var msg;
		try{
			var data = JSON.parse(jqXHR.responseText);
			msg = "Error código " + data.code + '\n' + data.description;
		}catch(e){
			msg = 'Error interno (HTTP Status ' + jqXHR.status + ': ' + strErrorThrown + ')';
		}
		// Crear y mostrar diálogo
		t$._createDialog("medium");
		t$._outputDiv.text(msg);
		t$._headerElem.text("Error");
		t$._updateOutputDivStyle( "error");
		t$._ajsDialog.show();
		t$._spinElem.spinStop();
	};

	// /////////////////////////////////////////////////////////
	// CREACIÓN DEL DIÁLOGO
	t$._createDialog = function(size) {
		// @see https://docs.atlassian.com/aui/latest/docs/dialog2.html
		var dlgSection = AJS.$('<section role="dialog" class="aui-layer aui-dialog2 aui-dialog2-' 
				+ size + '" aria-hidden="true" data-aui-modal="true" data-aui-remove-on-hide="true"></section>');
		t$._dlgSection = dlgSection;
		// Header
		var dlgHeader = AJS.$('<header class="aui-dialog2-header"></header>');
		t$._headerElem = AJS.$('<h2 class="aui-dialog2-header-main"></h2>');
		dlgHeader.append(t$._headerElem);
		dlgSection.append(dlgHeader);
		t$._spinElem = AJS.$('<div class="button-spinner"></div>');
		dlgHeader.append(AJS.$('<div class="aui-dialog2-header-secondary"></div>').append(t$._spinElem));
		// Content
		var dlgContentDiv = AJS.$('<div class="aui-dialog2-content"></div>');
		var dlgContentMsg = AJS.$('<div class="aui-message shadowed info"></div>'); 
		var dlgContentMsgSpan = AJS.$('<span class="aui-icon aui-icon-small icon-info"></span>');
		t$._outputDiv = AJS.$('<div style="white-space: pre-wrap"></div>');
		dlgContentMsg.append(dlgContentMsgSpan);
		dlgContentMsg.append(t$._outputDiv);
		dlgSection.append(dlgContentDiv.append(dlgContentMsg));
		// Footer
		var dlgFooter = AJS.$('<footer class="aui-dialog2-footer"></footer>');
		var dlgFooterDiv = AJS.$('<div class="aui-dialog2-footer-actions"></div>');
		var dlgHideBtn = AJS.$('<button id="dialog-submit-button" class="aui-button aui-button-primary">Ocultar este diálogo</button>');
		dlgHideBtn.click(function(e) {
		    e.preventDefault();
		    t$._ajsDialog.hide();
		    if (onClose) {
		    	t$.onClose();
		    }
		});
		dlgSection.append(dlgFooter.append(dlgFooterDiv.append(dlgHideBtn)));
		// initialize
		t$._container.append(dlgSection);
		t$._ajsDialog = AJS.dialog2(dlgSection);
		t$._ajsDialog.on("show", function() {
			t$._spinElem.spin();
		});
		return t$._ajsDialog; 
	};
};